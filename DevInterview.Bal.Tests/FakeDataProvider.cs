﻿using System;
using System.Collections.Generic;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.Identity;

namespace DevInterview.Bal.Tests
{
    public static class FakeDataProvider
    {
        #region Public Methods and Operators

        public static List<Category> PopulateCategoryStore()
        {
            var catStore = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Description = "cat cat",
                    Title = "ASP.NET",
                    UrlSlug = "aspnet"
                },
                new Category
                {
                    Id = 2,
                    Description = "cat 2 cat",
                    Title = "ASP",
                    UrlSlug = "asp"
                },
                new Category
                {
                    Id = 3,
                    Description = "cat",
                    Title = ".NET",
                    UrlSlug = "net"
                }
            };

            return catStore;
        }

        public static List<Post> PopulatePostStore()
        {
            var postStore = new List<Post>();

            var post = new Post
            {
                Id = 1,
                BodyEnd = "end",
                BodyStart = "start",
                Created = DateTime.Now,
                CreatedBy = new AppUser(),
                IsDeleted = false,
                IsVisible = true,
                LastUpdated = DateTime.Now,
                LastUpdatedBy = new AppUser(),
                Rating = new Rating(),
                Tags = new List<Tag>(),
                Title = "title",
                UrlSlug = "url",
                Views = 100,
                Category = new Category
                {
                    Id = 1
                }
            };
            postStore.Add(post);

            post = new Post
            {
                Id = 2,
                BodyEnd = "end",
                BodyStart = "start",
                Created = DateTime.Now,
                CreatedBy = new AppUser(),
                IsDeleted = false,
                IsVisible = true,
                LastUpdated = DateTime.Now,
                LastUpdatedBy = new AppUser(),
                Rating = new Rating(),
                Tags = new List<Tag>(),
                Title = "title",
                UrlSlug = "url",
                Views = 100,
                Category = new Category
                {
                    Id = 2
                }
            };
            postStore.Add(post);

            post = new Post
            {
                Id = 3,
                BodyEnd = "end",
                BodyStart = "start",
                Created = DateTime.Now,
                CreatedBy = new AppUser(),
                IsDeleted = false,
                IsVisible = true,
                LastUpdated = DateTime.Now,
                LastUpdatedBy = new AppUser(),
                Rating = new Rating(),
                Tags = new List<Tag>(),
                Title = "title",
                UrlSlug = "url",
                Views = 100,
                Category = new Category
                {
                    Id = 3
                }
            };
            postStore.Add(post);

            return postStore;
        }

        public static List<AppRole> PopulateRoleStore()
        {
            var roleStore = new List<AppRole>
            {
                new AppRole
                {
                    Id = 1,
                    Name = "Administrator"
                },
                new AppRole
                {
                    Id = 2,
                    Name = "Publisher"
                },
                new AppRole
                {
                    Id = 3,
                    Name = "Common"
                }
            };

            return roleStore;
        }

        public static List<Tag> PopulateTagStore()
        {
            var tags = new List<Tag>();

            var tag = new Tag
            {
                Id = 1,
                Title = "title",
                UrlSlug = "url"
            };
            tags.Add(tag);

            tag = new Tag
            {
                Id = 2,
                Title = "title",
                UrlSlug = "url"
            };
            tags.Add(tag);

            tag = new Tag
            {
                Id = 3,
                Title = "title",
                UrlSlug = "url"
            };
            tags.Add(tag);

            return tags;
        }

        public static List<AppUser> PopulateUserStore()
        {
            var userStore = new List<AppUser>
            {
                new AppUser
                {
                    Id = 1,
                    UserName = "George",
                    Email = "george@hi.com",
                    Created = DateTime.Now,
                    IsActive = false,
                    IsDeleted = true
                },
                new AppUser
                {
                    Id = 2,
                    UserName = "NOONE",
                    Email = "noone@hi.com",
                    Created = DateTime.Now,
                    IsActive = false,
                    IsDeleted = true
                },
                new AppUser
                {
                    Id = 3,
                    UserName = "Axe",
                    Email = "nope@moo.com",
                    Created = DateTime.Now,
                    IsActive = false,
                    IsDeleted = true
                }
            };

            return userStore;
        }

        #endregion
    }
}