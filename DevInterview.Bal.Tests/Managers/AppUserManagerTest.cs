﻿using System.Linq;
using System.Threading.Tasks;
using DevInterview.Bal.Managers.Identity;
using DevInterview.Bal.Tests.Mocks;
using DevInterview.Dal.Entities.Identity;
using DevInterview.Dal.Entities.NotBusiness;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace DevInterview.Bal.Tests.Managers
{
    [TestFixture]
    public class AppUserManagerTest
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            userRepositoryMock = new UserRepositoryMock(FakeDataProvider.PopulateUserStore());

            target = new AppUserManager(userRepositoryMock.Instance.Object);
        }

        #endregion


        #region All other members

        private AppUserManager target;

        private UserRepositoryMock userRepositoryMock;

        #endregion


        #region Test Methods

        [Test]
        public async Task CreateUser_WithValidUser_AddsUserToRepo()
        {
            var user = new AppUser
            {
                Email = "newuser@new.ru",
                UserName = "Customer"
            };
            int entriesBefore = userRepositoryMock.Store.Count;

            await target.CreateAsync(user);

            userRepositoryMock.Instance.Verify(u => u.CreateAsync(user), Times.Once());
            int entriesAfter = userRepositoryMock.Store.Count;
            entriesAfter.Should().Be(entriesBefore + 1);
        }

        [Test]
        public async Task CreateUser_WithValidUser_AssignsIdAfterCreation()
        {
            var user = new AppUser
            {
                Email = "newuser@new.ru",
                UserName = "Customer"
            };

            int futureId = userRepositoryMock.Store.Max(a => a.Id + 1);

            await target.CreateAsync(user);

            userRepositoryMock.Instance.Verify(u => u.CreateAsync(user));
            user.Id.Should().Be(futureId);
        }

        [Test]
        public async Task CreateUserWithPassword_WithValidUser_AddUserToRepo()
        {
            var user = new AppUser
            {
                Email = "newuser@new.ru",
                UserName = "Customer"
            };
            int entriesBefore = userRepositoryMock.Store.Count;

            await target.CreateAsync(user, "123456");

            userRepositoryMock.Instance.Verify(u => u.CreateAsync(user));
            int entriesAfter = userRepositoryMock.Store.Count;
            entriesAfter.Should().Be(entriesBefore + 1);
        }

        [Test]
        public async Task FindUserById_WithExistingUser_ReturnsUser()
        {
            AppUser user = await target.FindByIdAsync(2);
            AppUser user2 = await target.FindByIdAsync(-1);

            userRepositoryMock.Instance.Verify(u => u.FindByIdAsync(2));
            userRepositoryMock.Instance.Verify(u => u.FindByIdAsync(-1));
            user.Should().NotBeNull();
            user.Id.Should().Be(2);
            user2.Should().BeNull();
        }

        [Test]
        public async Task FindUserByName_WithExistingUser_ReturnsUser()
        {
            AppUser user = await target.FindByNameAsync("George");
            AppUser user2 = await target.FindByNameAsync("Noname");

            userRepositoryMock.Instance.Verify(u => u.FindByNameAsync("George"));
            userRepositoryMock.Instance.Verify(u => u.FindByNameAsync("Noname"));
            user.Should().NotBeNull();
            user.UserName.Should().Be("George");
            user2.Should().BeNull();
        }

        [Test]
        public async Task FindUserByNameAndPassword_WithExistingUser_ReturnsUser()
        {
            var newUser = new AppUser
            {
                UserName = "Customer"
            };
            await target.CreateAsync(newUser, "123456");

            AppUser user = await target.FindAsync("Customer", "123456");
            AppUser user2 = await target.FindAsync("Nono", "2345");

            userRepositoryMock.Instance.Verify(u => u.CreateAsync(newUser));
            user.Should().NotBeNull();
            user.UserName.Should().Be("Customer");
            user2.Should().BeNull();
        }

        [Test]
        public async Task GetAll_WithUsersInStorage_ShouldReturnUsers()
        {
            userRepositoryMock.Instance.Setup(u => u.GetAllAsync()).Returns(Task.FromResult(new SearchResult<AppUser>()));

            SearchResult<AppUser> result = await target.GetAllAsync();

            result.Should().NotBeNull();
            userRepositoryMock.Instance.Verify(u => u.GetAllAsync());
        }

        [Test]
        public async Task UpdateUser_WithValidUser_UpdatesUserInRepo()
        {
            const string email = "new@mail.com";
            AppUser user = await target.FindByIdAsync(2);
            user.Email = email;

            await target.UpdateAsync(user);

            userRepositoryMock.Instance.Verify(u => u.UpdateAsync(user));
            user = await target.FindByIdAsync(2);
            user.Should().NotBeNull();
            user.Email.Should().Be(email);
        }

        #endregion
    }
}