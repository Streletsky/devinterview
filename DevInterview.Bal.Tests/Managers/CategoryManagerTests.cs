﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using DevInterview.Bal.Managers.Business;
using DevInterview.Bal.Managers.Business.Interfaces;
using DevInterview.Bal.Tests.Mocks;
using DevInterview.Common.Configuration;
using DevInterview.Common.Enums;
using DevInterview.Common.Exceptions.Bal;
using DevInterview.Common.Exceptions.Dal;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace DevInterview.Bal.Tests.Managers
{
    [TestFixture]
    public class CategoryManagerTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            categoryRepositoryMock = new CategoryRepositoryMock(FakeDataProvider.PopulateCategoryStore());

            cacheMock = new CacheManagerMock(new Dictionary<string, object>());

            target = new CategoryManager(categoryRepositoryMock.Instance.Object, cacheMock.Instance.Object);

            testEntity = new Category
            {
                Id = 1,
                Title = "title",
                UrlSlug = "url"
            };
        }

        #endregion


        #region All other members

        private CacheManagerMock cacheMock;

        private CategoryRepositoryMock categoryRepositoryMock;

        private ICategoryManager target;

        private Category testEntity;

        #endregion


        #region Test Methods

        [Test]
        public void Create_WithCorrectCategory_ShouldAddToStorage()
        {
            Category result = target.Create(testEntity);

            result.Should().NotBeNull();
            result.Should().BeSameAs(testEntity);
            categoryRepositoryMock.Instance.Verify(m => m.Insert(testEntity), Times.Once);
        }

        [Test]
        public void Create_WithCorrectCategory_ShouldResetCache()
        {
            Category result = target.Create(testEntity);

            result.Should().NotBeNull();
            result.Should().BeSameAs(testEntity);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.Categories.ToString(), CacheRegions.Categories.ToString()), Times.Once);
        }

        [Test]
        public void Create_WithExceptionThrownFromRepository_ShouldThrowException()
        {
            categoryRepositoryMock.Instance.Setup(m => m.Insert(testEntity)).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.Create(testEntity));
        }

        [Test]
        [TestCase(null)]
        public void Create_WithNullCategory_ShouldThrowException(Category entity)
        {
            Assert.Throws<BusinessLogicException>(() => target.Create(entity));
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Create_WithNullTitle_ShouldThrowException(string title)
        {
            testEntity.Title = title;

            Assert.Throws<BusinessLogicException>(() => target.Create(testEntity));
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Create_WithNullUrlSlug_ShouldThrowException(string urlslug)
        {
            testEntity.UrlSlug = urlslug;

            Assert.Throws<BusinessLogicException>(() => target.Create(testEntity));
        }

        [Test]
        [TestCase(1)]
        public void Delete_WithExceptionThrownFromRepository_ShouldThrowException(int id)
        {
            categoryRepositoryMock.Instance.Setup(m => m.Delete(id)).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.Delete(id));
        }

        [Test]
        [TestCase(1)]
        public void Delete_WithValidId_ShouldDeleteFromDb(int id)
        {
            categoryRepositoryMock.Instance.Setup(m => m.Delete(id));

            target.Delete(id);

            categoryRepositoryMock.Instance.Verify(m => m.Delete(id), Times.Once);
        }

        [Test]
        [TestCase(1)]
        public void Delete_WithValidId_ShouldResetCache(int id)
        {
            categoryRepositoryMock.Instance.Setup(m => m.Delete(id));

            target.Delete(id);

            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.Categories.ToString(), CacheRegions.Categories.ToString()), Times.Once);
        }

        [Test]
        [TestCase(0)]
        [TestCase(-100)]
        public void Delete_WithWrongId_ShouldThrowException(int id)
        {
            Assert.Throws<BusinessLogicException>(() => target.Delete(id));
        }

        [Test]
        [TestCase(1)]
        public void DeleteEntity_WithExceptionThrownFromRepository_ShouldThrowException(int id)
        {
            categoryRepositoryMock.Instance.Setup(m => m.Delete(It.Is<Category>(c => c.Id == testEntity.Id))).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.Delete(testEntity));
        }

        [Test]
        [TestCase(null)]
        public void DeleteEntity_WithNullEntity_ShouldThrowException(Category cat)
        {
            Assert.Throws<BusinessLogicException>(() => target.Delete(cat));
        }

        [Test]
        [TestCase(1)]
        public void DeleteEntity_WithValidId_ShouldDeleteFromDb(int id)
        {
            categoryRepositoryMock.Instance.Setup(m => m.Delete(It.Is<Category>(c => c.Id == testEntity.Id)));

            target.Delete(testEntity);

            categoryRepositoryMock.Instance.Verify(m => m.Delete(It.Is<Category>(c => c.Id == testEntity.Id)), Times.Once);
        }

        [Test]
        [TestCase(1)]
        public void DeleteEntity_WithValidId_ShouldResetCache(int id)
        {
            categoryRepositoryMock.Instance.Setup(m => m.Delete(It.Is<Category>(c => c.Id == testEntity.Id)));

            target.Delete(testEntity);

            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.Categories.ToString(), CacheRegions.Categories.ToString()), Times.Once);
        }

        [Test]
        [TestCase(0)]
        [TestCase(-100)]
        public void DeleteEntity_WithWrongId_ShouldThrowException(int id)
        {
            var cat = new Category
            {
                Id = id
            };

            Assert.Throws<BusinessLogicException>(() => target.Delete(cat));
        }

        [Test]
        [TestCase(1)]
        public void Get_WithCategoryNotFoundInDb_ShouldReturnNull(int id)
        {
            categoryRepositoryMock.Instance.Setup(m => m.Get(id)).Returns((Category)null);

            Category result = target.Get(id);

            result.Should().BeNull();
        }

        [Test]
        [TestCase(1)]
        public void Get_WithExceptionThrownFromRepository_ShouldThrowException(int id)
        {
            categoryRepositoryMock.Instance.Setup(m => m.Get(id)).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.Get(id));
        }

        [Test]
        [TestCase(0)]
        [TestCase(-100)]
        public void Get_WithInvalidId_ShouldThrowException(int id)
        {
            Assert.Throws<BusinessLogicException>(() => target.Get(id));
        }

        [Test]
        [TestCase(1)]
        public void Get_WithValidId_ShouldReturnFromDb(int id)
        {
            Category expected = categoryRepositoryMock.Store.FirstOrDefault(c => c.Id == id);

            Category result = target.Get(id);

            categoryRepositoryMock.Instance.Verify(m => m.Get(It.IsAny<int>()), Times.Once);
            result.Should().NotBeNull();
            result.Should().BeSameAs(expected);
        }

        [Test]
        public void GetAll_WithCategoriesInCache_ShouldReturnCategoriesFromCache()
        {
            string cacheKey = CacheKeys.Categories.ToString();
            string cacheRegion = CacheRegions.Categories.ToString();
            var expected = new SearchResult<Category>
            {
                Data = categoryRepositoryMock.Store,
                Records = categoryRepositoryMock.Store.Count,
                TotalRecords = categoryRepositoryMock.Store.Count
            };

            cacheMock.Instance.Object.Add(cacheKey, expected, AppCfgReader.CategoriesListCacheTime, cacheRegion);

            SearchResult<Category> result = target.GetAll();

            result.Should().NotBeNull();
            result.Should().BeSameAs(expected);

            cacheMock.Instance.Verify(m => m.Get(cacheKey, cacheRegion), Times.Once);
            categoryRepositoryMock.Instance.Verify(m => m.GetAll(), Times.Never);
        }

        [Test]
        public void GetAll_WithCategoriesNotFoundInDb_ShouldReturnNull()
        {
            categoryRepositoryMock.Instance.Setup(m => m.GetAll()).Returns((SearchResult<Category>)null);

            SearchResult<Category> result = target.GetAll();

            result.Should().BeNull();
        }

        [Test]
        public void GetAll_WithCategoriesNotInCache_ShouldReturnCategoriesFromDbAndAddToCache()
        {
            string cacheKey = CacheKeys.Categories.ToString();
            string cacheRegion = CacheRegions.Categories.ToString();
            var expected = new SearchResult<Category>
            {
                Data = categoryRepositoryMock.Store,
                Records = categoryRepositoryMock.Store.Count,
                TotalRecords = categoryRepositoryMock.Store.Count
            };

            SearchResult<Category> result = target.GetAll();

            result.Should().NotBeNull();
            result.Data.Should().BeEquivalentTo(expected.Data);
            result.Records.Should().Be(expected.Records);
            result.TotalRecords.Should().Be(expected.TotalRecords);
            cacheMock.Instance.Verify(m => m.Get(cacheKey, cacheRegion), Times.Once);
            cacheMock.Instance.Verify(
                                      m =>
                                          m.Add(cacheKey,
                                                It.IsAny<SearchResult<Category>>(),
                                                AppCfgReader.CategoriesListCacheTime,
                                                cacheRegion),
                                      Times.Once);
            categoryRepositoryMock.Instance.Verify(m => m.GetAll(), Times.Once);
        }

        [Test]
        public void GetAll_WithExceptionThrownFromRepository_ShouldThrowException()
        {
            categoryRepositoryMock.Instance.Setup(m => m.GetAll()).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.GetAll());
        }

        [Test]
        public void Update_CategoryWithoutId_ShouldThrowException()
        {
            var cat = new Category
            {
                Id = 0,
                Title = "title",
                UrlSlug = "url"
            };

            Assert.Throws<BusinessLogicException>(() => target.Update(cat));
        }

        [Test]
        public void Update_WithCorrectCategory_ShouldAddToDb()
        {
            Category result = target.Update(testEntity);

            result.Should().NotBeNull();
            result.Should().BeSameAs(testEntity);
            categoryRepositoryMock.Instance.Verify(m => m.Update(testEntity), Times.Once);
        }

        [Test]
        public void Update_WithCorrectCategory_ShouldResetCache()
        {
            Category result = target.Update(testEntity);

            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.Categories.ToString(), CacheRegions.Categories.ToString()), Times.Once);
        }

        [Test]
        public void Update_WithExceptionThrownFromRepository_ShouldThrowException()
        {
            categoryRepositoryMock.Instance.Setup(m => m.Update(testEntity)).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.Update(testEntity));
        }

        [Test]
        [TestCase(null)]
        public void Update_WithNullCategory_ShouldThrowException(Category cat)
        {
            Assert.Throws<BusinessLogicException>(() => target.Update(cat));
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Update_WithNullTitle_ShouldThrowException(string title)
        {
            testEntity.Title = title;

            Assert.Throws<BusinessLogicException>(() => target.Update(testEntity));
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Update_WithNullUrlSlug_ShouldThrowException(string urlslug)
        {
            testEntity.UrlSlug = urlslug;

            Assert.Throws<BusinessLogicException>(() => target.Update(testEntity));
        }

        #endregion
    }
}