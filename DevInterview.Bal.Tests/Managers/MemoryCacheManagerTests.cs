﻿using DevInterview.Bal.Managers.Caching;
using DevInterview.Common.Exceptions.Bal;
using FluentAssertions;
using NUnit.Framework;

namespace DevInterview.Bal.Tests.Managers
{
    [TestFixture]
    public class MemoryCacheManagerTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            target = new MemoryCacheManager();

            PopulateCache();
        }

        [TearDown]
        public void TearDown()
        {
            target.Clear();
        }

        #endregion


        #region All other members

        private const string TestKey = "upvotes_count";

        private const int TestValue = 100;

        private MemoryCacheManager target;

        private void PopulateCache()
        {
            target.Add(TestKey, TestValue, int.MaxValue);
        }

        #endregion


        #region Test Methods

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Add_WithInvalidKey_ShouldThrowException(string key)
        {
            Assert.Throws<BusinessLogicException>(() => target.Add(key, 100, int.MaxValue));
        }

        [Test]
        public void Add_WithValidKeyAndAlreadyExistingData_ShouldAddItem()
        {
            const string key = "key";
            const string data = "data";
            target.Add(key, data, int.MaxValue);
            target.Add(key, data, int.MaxValue);

            var result = (string)target.Get(key);

            result.Should().NotBeNull();
            result.Should().Be(data);
        }

        [Test]
        public void Add_WithValidKeyAndData_ShouldAddItem()
        {
            const string key = "key";
            const string data = "data";
            target.Add(key, data, int.MaxValue);

            var result = (string)target.Get(key);

            result.Should().NotBeNull();
            result.Should().Be(data);
        }

        [Test]
        public void Add_WithValidKeyAndRegionAndAlreadyExistingData_ShouldAddItem()
        {
            const string key = "key";
            const string region = "region";
            const string data = "data";
            target.Add(key, data, int.MaxValue, region);
            target.Add(key, data, int.MaxValue, region);

            var result = (string)target.Get(key, region);

            result.Should().NotBeNull();
            result.Should().Be(data);
        }

        [Test]
        public void Add_WithValidKeyRegionAndData_ShouldAddItem()
        {
            const string key = "key";
            const string region = "region";
            const string data = "data";
            target.Add(key, data, int.MaxValue, region);

            var result = (string)target.Get(key, region);

            result.Should().NotBeNull();
            result.Should().Be(data);
        }

        [Test]
        public void Clear_WithoutRegion_ClearsWholeCache()
        {
            const string key = "key";
            const string region = "region";
            const string data = "data";
            target.Add(key, data, int.MaxValue, region);

            target.Clear();
            object result1 = target.Get(TestKey);
            object result2 = target.Get(key, region);

            result1.Should().BeNull();
            result2.Should().BeNull();
        }

        [Test]
        public void Clear_WithRegion_ClearsOnlyRegion()
        {
            const string key = "key";
            const string region = "region";
            const string data = "data";
            target.Add(key, data, int.MaxValue, region);

            target.Clear(region);
            object result1 = target.Get(TestKey);
            object result2 = target.Get(key, region);

            result1.Should().NotBeNull();
            result2.Should().BeNull();
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Contains_WithInvalidKey_ShouldThrowException(string key)
        {
            Assert.Throws<BusinessLogicException>(() => target.Contains(key));
        }

        [Test]
        public void Contains_WithValidKeyAndObjectInCache_ShouldReturnTrue()
        {
            bool result = target.Contains(TestKey);

            result.Should().BeTrue();
        }

        [Test]
        public void Contains_WithValidKeyAndObjectNotInCache_ShouldReturnFalse()
        {
            const string key = "notexists";

            bool result = target.Contains(key);

            result.Should().BeFalse();
        }

        [Test]
        public void Contains_WithValidKeyAndRegionAndObjectInCache_ShouldReturnTrue()
        {
            const string region = "region";
            target.Add(TestKey, TestValue, 60, region);

            bool result = target.Contains(TestKey, region);

            result.Should().BeTrue();
        }

        [Test]
        public void Contains_WithValidKeyAndRegionAndObjectNotInCache_ShouldReturnFalse()
        {
            const string key = "notexists";
            const string region = "region";

            bool result = target.Contains(key, region);

            result.Should().BeFalse();
        }

        [Test]
        public void Get_WithExistingItemInCache_ShouldReturnItem()
        {
            var result = (int)target.Get(TestKey);

            result.Should().NotBe(0);
            result.Should().Be(TestValue);
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Get_WithInvalidKey_ShouldThrowException(string key)
        {
            Assert.Throws<BusinessLogicException>(() => target.Get(key));
        }

        [Test]
        public void Get_WithNotExistingItemInCache_ShouldReturnNull()
        {
            object result = target.Get("notexisting");

            result.Should().BeNull();
        }

        [Test]
        public void Get_WithoutRegionAndValidKeyAndExistingItemInRegion_ShouldReturnNull()
        {
            const string region = "region";
            const string key = "key";
            const string data = "data";
            target.Add(key, data, int.MaxValue, region);

            object result = target.Get(key);

            result.Should().BeNull();
        }

        [Test]
        public void Get_WithRegionAndValidKeyAndExistingItemInThisRegion_ShouldReturnItem()
        {
            const string region = "region";
            const string key = "key";
            const string data = "data";
            target.Add(key, data, int.MaxValue, region);

            var result = (string)target.Get(key, region);

            result.Should().NotBeNull();
            result.Should().Be(data);
        }

        [Test]
        public void Get_WithRegionAndValidKeyAndNotExistingItemInThisRegion_ShouldReturnNull()
        {
            const string region = "region";

            object result = target.Get(TestKey, region);

            result.Should().BeNull();
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Remove_WithInvalidKey_ShouldThrowException(string key)
        {
            Assert.Throws<BusinessLogicException>(() => target.Remove(key));
        }

        [Test]
        public void Remove_WithValidKey_ShouldRemoveItem()
        {
            target.Remove(TestKey);

            object result = target.Get(TestKey);

            result.Should().BeNull();
        }

        [Test]
        public void Remove_WithValidKeyAndRegion_ShouldNotRemoveItemWithSameKeyInRoot()
        {
            const string key = "key";
            const string region = "region";
            const string data = "data";
            target.Add(key, data, int.MaxValue, region);

            target.Remove(key);

            object result = target.Get(key, region);

            result.Should().NotBeNull();
            result.Should().Be(data);
        }

        [Test]
        public void Remove_WithValidKeyAndRegion_ShouldRemoveItem()
        {
            const string key = "key";
            const string region = "region";
            const string data = "data";
            target.Add(key, data, int.MaxValue, region);

            target.Remove(key, region);

            object result = target.Get(key, region);

            result.Should().BeNull();
        }

        #endregion
    }
}