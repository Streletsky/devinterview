﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using DevInterview.Bal.Managers.Business;
using DevInterview.Bal.Managers.Business.Interfaces;
using DevInterview.Bal.SearchEngine.Base;
using DevInterview.Bal.Tests.Mocks;
using DevInterview.Common.Configuration;
using DevInterview.Common.Enums;
using DevInterview.Common.Exceptions.Bal;
using DevInterview.Common.Exceptions.Dal;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using FluentAssertions;
using Lucene.Net.Documents;
using Moq;
using NUnit.Framework;

namespace DevInterview.Bal.Tests.Managers
{
    [TestFixture]
    public class PostManagerTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            postRepositoryMock = new PostRepositoryMock(FakeDataProvider.PopulatePostStore());
            postRepositoryMock.Instance.Setup(m => m.GetPostsByCategory(It.IsAny<SearchParameters>())).Returns(new SearchResult<Post>());

            cacheMock = new CacheManagerMock(new Dictionary<string, object>());

            searchEngineMock = new Mock<ILuceneSearchEngine>();
            searchEngineMock.Setup(m => m.Search(It.IsAny<string>(), It.IsAny<int>())).Returns(new List<Document>());
            searchEngineMock.Setup(m => m.Search(It.IsAny<Post>(), It.IsAny<int>())).Returns(new List<Document>());

            target = new PostManager(postRepositoryMock.Instance.Object, cacheMock.Instance.Object, searchEngineMock.Object);

            testPost = new Post
            {
                Id = 1,
                Category = new Category
                {
                    Id = 1
                },
                BodyStart = "test",
                Title = "title",
                UrlSlug = "url"
            };
        }

        [TearDown]
        public void TearDown()
        {
            cacheMock.Store.Clear();
        }

        #endregion


        #region All other members

        private CacheManagerMock cacheMock;

        private PostRepositoryMock postRepositoryMock;

        private Mock<ILuceneSearchEngine> searchEngineMock;

        private IPostManager target;

        private Post testPost;

        #endregion


        #region Test Methods

        [Test]
        public void Constructor_WithNullRepositoryParam_ShouldThrowException()
        {
            Assert.Throws<BusinessLogicException>(() => new PostManager(null, cacheMock.Instance.Object, searchEngineMock.Object));
        }

        [Test]
        public void Create_WithCorrectPost_ShouldAddToStorage()
        {
            Post entity = testPost;

            Post result = target.Create(entity);

            result.Should().NotBeNull();
            result.Should().Be(entity);
            postRepositoryMock.Instance.Verify(m => m.Insert(entity), Times.Once);
        }

        [Test]
        public void Create_WithCorrectPost_ShouldResetCache()
        {
            Post entity = testPost;

            Post result = target.Create(entity);

            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.LatestPosts.ToString(), CacheRegions.Posts.ToString()), Times.Once);
        }

        [Test]
        public void Create_WithCorrectPostAndNewTags_ShouldResetCache()
        {
            testPost.Tags = new List<Tag>
            {
                new Tag
                {
                    Id = 0
                }
            };

            Post result = target.Create(testPost);

            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.Tags.ToString(), CacheRegions.Tags.ToString()), Times.Once);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.TaggedPostsCounter.ToString(), CacheRegions.Tags.ToString()), Times.Once);
        }

        [Test]
        public void Create_WithExceptionThrownFromRepository_ShouldThrowException()
        {
            Post entity = testPost;

            postRepositoryMock.Instance.Setup(m => m.Insert(entity)).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.Create(entity));
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Create_WithNullBodyStart_ShouldThrowException(string body)
        {
            testPost.BodyStart = body;

            Assert.Throws<BusinessLogicException>(() => target.Create(testPost));
        }

        [Test]
        public void Create_WithNullCategory_ShouldThrowException()
        {
            testPost.Category = null;

            Assert.Throws<BusinessLogicException>(() => target.Create(testPost));
        }

        [Test]
        [TestCase(null)]
        public void Create_WithNullPost_ShouldThrowException(Post entity)
        {
            Assert.Throws<BusinessLogicException>(() => target.Create(entity));
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Create_WithNullTitle_ShouldThrowException(string title)
        {
            testPost.Title = title;

            Assert.Throws<BusinessLogicException>(() => target.Create(testPost));
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Create_WithNullUrlSlug_ShouldThrowException(string urlslug)
        {
            testPost.UrlSlug = urlslug;

            Assert.Throws<BusinessLogicException>(() => target.Create(testPost));
        }

        [Test]
        public void Create_WithZeroCategoryId_ShouldThrowException()
        {
            testPost.Category.Id = 0;

            Assert.Throws<BusinessLogicException>(() => target.Create(testPost));
        }

        [Test]
        [TestCase(1)]
        public void Delete_WithCorrectPost_ShouldNotResetCacheIfPostNotInCache(int id)
        {
            target.Delete(id);

            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.LatestPosts.ToString(), CacheRegions.Posts.ToString()), Times.Never);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.TopRatedPosts.ToString(), CacheRegions.Posts.ToString()), Times.Never);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.MostViewedPosts.ToString(), CacheRegions.Posts.ToString()), Times.Never);
        }

        [Test]
        [TestCase(1)]
        public void Delete_WithCorrectPost_ShouldRemoveFromStorage(int id)
        {
            target.Delete(id);

            postRepositoryMock.Instance.Verify(m => m.Delete(id), Times.Once);
        }

        [Test]
        [TestCase(1)]
        public void Delete_WithCorrectPost_ShouldResetCacheIfPostInCacheAndShouldResetCounters(int id)
        {
            Post post = postRepositoryMock.Store.FirstOrDefault(p => p.Id == id);
            cacheMock.Instance.Object.Add(CacheKeys.LatestPosts.ToString(),
                                          new SearchResult<Post>
                                          {
                                              Data = postRepositoryMock.Store
                                          },
                                          AppCfgReader.LatestPostsCacheTime,
                                          CacheRegions.Posts.ToString());
            cacheMock.Instance.Object.Add(CacheKeys.MostViewedPosts.ToString(),
                                          new SearchResult<Post>
                                          {
                                              Data = postRepositoryMock.Store
                                          },
                                          AppCfgReader.MostViewedPostsCacheTime,
                                          CacheRegions.Posts.ToString());
            cacheMock.Instance.Object.Add(CacheKeys.TopRatedPosts.ToString(),
                                          new SearchResult<Post>
                                          {
                                              Data = postRepositoryMock.Store
                                          },
                                          AppCfgReader.TopRatedPostsCacheTime,
                                          CacheRegions.Posts.ToString());

            target.Delete(id);

            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.LatestPosts.ToString(), CacheRegions.Posts.ToString()), Times.Once);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.TopRatedPosts.ToString(), CacheRegions.Posts.ToString()), Times.Once);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.MostViewedPosts.ToString(), CacheRegions.Posts.ToString()), Times.Once);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.Tags.ToString(), CacheRegions.Tags.ToString()), Times.Once);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.TaggedPostsCounter.ToString(), CacheRegions.Tags.ToString()), Times.Once);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.CategoryCounter + post.Category.Id.ToString(), CacheRegions.Posts.ToString()), Times.Once);
        }

        [Test]
        [TestCase(1)]
        public void Delete_WithExceptionThrownFromRepository_ShouldThrowException(int id)
        {
            postRepositoryMock.Instance.Setup(m => m.Delete(id)).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.Delete(id));
        }

        [Test]
        [TestCase(0)]
        [TestCase(-100)]
        public void Delete_WithWrongId_ShouldThrowException(int id)
        {
            Assert.Throws<BusinessLogicException>(() => target.Delete(id));
        }

        [Test]
        [TestCase(1)]
        public void DeleteEntity_WithCorrectPost_ShouldNotResetCacheIfPostNotInCacheAndShouldResetCounters(int id)
        {
            target.Delete(testPost);

            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.LatestPosts.ToString(), CacheRegions.Posts.ToString()), Times.Never);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.TopRatedPosts.ToString(), CacheRegions.Posts.ToString()), Times.Never);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.MostViewedPosts.ToString(), CacheRegions.Posts.ToString()), Times.Never);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.Tags.ToString(), CacheRegions.Tags.ToString()), Times.Once);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.TaggedPostsCounter.ToString(), CacheRegions.Tags.ToString()), Times.Once);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.CategoryCounter + testPost.Category.Id.ToString(), CacheRegions.Posts.ToString()), Times.Once);
        }

        [Test]
        [TestCase(1)]
        public void DeleteEntity_WithCorrectPost_ShouldRemoveFromStorage(int id)
        {
            target.Delete(testPost);

            postRepositoryMock.Instance.Verify(m => m.Delete(It.Is<Post>(p => p.Id == testPost.Id)), Times.Once);
        }

        [Test]
        [TestCase(1)]
        public void DeleteEntity_WithCorrectPost_ShouldResetCacheIfPostInCacheAndShouldResetCounters(int id)
        {
            cacheMock.Instance.Object.Add(CacheKeys.LatestPosts.ToString(),
                                          new SearchResult<Post>
                                          {
                                              Data = postRepositoryMock.Store
                                          },
                                          AppCfgReader.LatestPostsCacheTime,
                                          CacheRegions.Posts.ToString());
            cacheMock.Instance.Object.Add(CacheKeys.MostViewedPosts.ToString(),
                                          new SearchResult<Post>
                                          {
                                              Data = postRepositoryMock.Store
                                          },
                                          AppCfgReader.MostViewedPostsCacheTime,
                                          CacheRegions.Posts.ToString());
            cacheMock.Instance.Object.Add(CacheKeys.TopRatedPosts.ToString(),
                                          new SearchResult<Post>
                                          {
                                              Data = postRepositoryMock.Store
                                          },
                                          AppCfgReader.TopRatedPostsCacheTime,
                                          CacheRegions.Posts.ToString());

            target.Delete(testPost);

            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.LatestPosts.ToString(), CacheRegions.Posts.ToString()), Times.Once);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.TopRatedPosts.ToString(), CacheRegions.Posts.ToString()), Times.Once);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.MostViewedPosts.ToString(), CacheRegions.Posts.ToString()), Times.Once);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.Tags.ToString(), CacheRegions.Tags.ToString()), Times.Once);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.TaggedPostsCounter.ToString(), CacheRegions.Tags.ToString()), Times.Once);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.CategoryCounter + testPost.Category.Id.ToString(), CacheRegions.Posts.ToString()), Times.Once);
        }

        [Test]
        [TestCase(1)]
        public void DeleteEntity_WithExceptionThrownFromRepository_ShouldThrowException(int id)
        {
            postRepositoryMock.Instance.Setup(m => m.Delete(It.IsAny<Post>())).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.Delete(testPost));
        }

        [Test]
        [TestCase(null)]
        public void DeleteEntity_WithNullEntity_ShouldThrowException(Post post)
        {
            Assert.Throws<BusinessLogicException>(() => target.Delete(post));
        }

        [Test]
        [TestCase(0)]
        [TestCase(-100)]
        public void DeleteEntity_WithWrongId_ShouldThrowException(int id)
        {
            var post = new Post
            {
                Id = id
            };

            Assert.Throws<BusinessLogicException>(() => target.Delete(post));
        }

        [Test]
        [TestCase(1)]
        public void Get_WithExceptionThrownFromRepository_ShouldThrowException(int id)
        {
            postRepositoryMock.Instance.Setup(m => m.Get(id)).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.Get(id));
        }

        [Test]
        [TestCase(0)]
        [TestCase(-100)]
        public void Get_WithInvalidId_ShouldThrowException(int id)
        {
            Assert.Throws<BusinessLogicException>(() => target.Get(id));
        }

        [Test]
        [TestCase(1)]
        public void Get_WithPostNotFoundInDb_ShouldReturnNull(int id)
        {
            postRepositoryMock.Instance.Setup(m => m.Get(id)).Returns((Post)null);

            Post result = target.Get(id);

            result.Should().BeNull();
        }

        [Test]
        [TestCase(1)]
        public void Get_WithValidIdAndPost_ShouldReturnPostFromStorage(int id)
        {
            Post expected = postRepositoryMock.Store.FirstOrDefault(c => c.Id == id);

            Post result = target.Get(id);

            result.Should().NotBeNull();
            result.Should().Be(expected);
            postRepositoryMock.Instance.Verify(m => m.Get(It.IsAny<int>()), Times.Once);
        }

        [Test]
        public void GetAll_WithCategoriesInStorage_ShouldReturnCategoriesFromDb()
        {
            var expected = new SearchResult<Post>
            {
                Data = postRepositoryMock.Store,
                Records = postRepositoryMock.Store.Count,
                TotalRecords = postRepositoryMock.Store.Count
            };

            SearchResult<Post> result = target.GetAll();

            result.Should().NotBeNull();
            result.Data.Should().BeEquivalentTo(expected.Data);
            result.Records.Should().Be(expected.Records);
            result.TotalRecords.Should().Be(expected.TotalRecords);
            postRepositoryMock.Instance.Verify(m => m.GetAll(), Times.Once);
        }

        [Test]
        public void GetAll_WithCategoriesNotFoundInDb_ShouldReturnNull()
        {
            postRepositoryMock.Instance.Setup(m => m.GetAll()).Returns((SearchResult<Post>)null);

            SearchResult<Post> result = target.GetAll();

            result.Should().BeNull();
        }

        [Test]
        public void GetAll_WithExceptionThrownFromRepository_ShouldThrowException()
        {
            postRepositoryMock.Instance.Setup(m => m.GetAll()).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.GetAll());
        }

        [Test]
        public void GetLatestPosts_WithExceptionThrownFromRepository_ShouldThrowException()
        {
            postRepositoryMock.Instance.Setup(m => m.Search(It.IsAny<SearchParameters>())).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.GetLatestPosts());
        }

        [Test]
        public void GetLatestPosts_WithResultsInCache_ShouldReturnPostsFromCache()
        {
            var expected = new SearchResult<Post>
            {
                Data = postRepositoryMock.Store
            };
            string cacheKey = CacheKeys.LatestPosts.ToString();
            string cacheRegion = CacheRegions.Posts.ToString();
            cacheMock.Store.Add(cacheKey + "`" + cacheRegion, expected);

            SearchResult<Post> result = target.GetLatestPosts();

            result.Should().NotBeNull();
            postRepositoryMock.Instance.Verify(m => m.Search(It.IsAny<SearchParameters>()), Times.Never);
            cacheMock.Instance.Verify(m => m.Get(cacheKey, cacheRegion), Times.Once);
            cacheMock.Instance.Verify(m => m.Add(cacheKey, result, AppCfgReader.LatestPostsCacheTime, cacheRegion), Times.Never);
        }

        [Test]
        public void GetLatestPosts_WithResultsNotInCache_ShouldSearchPostsAndPutInCache()
        {
            string cacheKey = CacheKeys.LatestPosts.ToString();
            string cacheRegion = CacheRegions.Posts.ToString();

            SearchResult<Post> result = target.GetLatestPosts();

            result.Should().NotBeNull();
            postRepositoryMock.Instance.Verify(m => m.Search(It.IsAny<SearchParameters>()), Times.Once);
            cacheMock.Instance.Verify(m => m.Get(cacheKey, cacheRegion), Times.Once);
            cacheMock.Instance.Verify(m => m.Add(cacheKey, result, AppCfgReader.LatestPostsCacheTime, cacheRegion), Times.Once);
        }

        [Test]
        public void GetLatestPosts_WithResultsNotInCache_ShouldSearchWithCorrectParams()
        {
            SearchResult<Post> result = target.GetLatestPosts();

            result.Should().NotBeNull();
            postRepositoryMock.Instance.Verify(
                                               m => m.Search(It.Is<SearchParameters>(s => s.Count == AppCfgReader.PageSize &&
                                                                                          s.Page == 1 &&
                                                                                          s.OrderBy == "Created" &&
                                                                                          s.OrderDirection == "desc")),
                                               Times.Once);
        }

        [Test]
        public void GetMostViewedPosts_WithExceptionThrownFromRepository_ShouldThrowException()
        {
            postRepositoryMock.Instance.Setup(m => m.Search(It.IsAny<SearchParameters>())).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.GetMostViewedPosts());
        }

        [Test]
        public void GetMostViewedPosts_WithResultsInCache_ShouldReturnPostsFromCache()
        {
            var expected = new SearchResult<Post>
            {
                Data = postRepositoryMock.Store
            };
            string cacheKey = CacheKeys.MostViewedPosts.ToString();
            string cacheRegion = CacheRegions.Posts.ToString();
            cacheMock.Store.Add(cacheKey + "`" + cacheRegion, expected);

            SearchResult<Post> result = target.GetMostViewedPosts();

            result.Should().NotBeNull();
            postRepositoryMock.Instance.Verify(m => m.Search(It.IsAny<SearchParameters>()), Times.Never);
            cacheMock.Instance.Verify(m => m.Get(cacheKey, cacheRegion), Times.Once);
            cacheMock.Instance.Verify(m => m.Add(cacheKey, result, AppCfgReader.MostViewedPostsCacheTime, cacheRegion), Times.Never);
        }

        [Test]
        public void GetMostViewedPosts_WithResultsNotInCache_ShouldSearchPostsAndPutInCache()
        {
            string cacheKey = CacheKeys.MostViewedPosts.ToString();
            string cacheRegion = CacheRegions.Posts.ToString();

            SearchResult<Post> result = target.GetMostViewedPosts();

            result.Should().NotBeNull();
            postRepositoryMock.Instance.Verify(m => m.Search(It.IsAny<SearchParameters>()), Times.Once);
            cacheMock.Instance.Verify(m => m.Get(cacheKey, cacheRegion), Times.Once);
            cacheMock.Instance.Verify(m => m.Add(cacheKey, result, AppCfgReader.MostViewedPostsCacheTime, cacheRegion), Times.Once);
        }

        [Test]
        public void GetMostViewedPosts_WithResultsNotInCache_ShouldSearchWithCorrectParams()
        {
            SearchResult<Post> result = target.GetMostViewedPosts();

            result.Should().NotBeNull();
            postRepositoryMock.Instance.Verify(m => m.Search(It.Is<SearchParameters>(s => s.Count == AppCfgReader.PageSize &&
                                                                                          s.Page == 1 &&
                                                                                          s.OrderBy == "Views" &&
                                                                                          s.OrderDirection == "desc")),
                                               Times.Once);
        }

        [Test]
        [TestCase(0)]
        [TestCase(-100)]
        public void GetPostsByCategoryId_WithInvalidId_ShouldThrowException(int id)
        {
            Assert.Throws<BusinessLogicException>(() => target.GetPostsByCategory(id, 1));
        }

        [Test]
        [TestCase(1, 1)]
        public void GetPostsByCategoryId_WithValidId_ShouldReturnPostsFromStorage(int id, int page)
        {
            SearchResult<Post> result = target.GetPostsByCategory(id, page);

            postRepositoryMock.Instance.Verify(
                                               m => m.GetPostsByCategory(It.Is<SearchParameters>(s => s.Count == AppCfgReader.PageSize &&
                                                                                                      s.Page == page &&
                                                                                                      s.OrderBy == "Created" &&
                                                                                                      s.OrderDirection == "desc" &&
                                                                                                      s.CustomSearchQuery == "Category.Id = " + id.ToString())),
                                               Times.Once);
            result.Should().NotBeNull();
        }

        [Test]
        [TestCase(0)]
        [TestCase(-100)]
        public void GetPostsByTagId_WithInvalidId_ShouldThrowException(int id)
        {
            Assert.Throws<BusinessLogicException>(() => target.GetPostsByTag(id, 1));
        }

        [Test]
        [TestCase(1, 1)]
        public void GetPostsByTagId_WithValidId_ShouldReturnPostsFromStorage(int id, int page)
        {
            SearchResult<Post> result = target.GetPostsByTag(id, page);

            postRepositoryMock.Instance.Verify(
                                               m => m.GetPostsByTag(It.Is<SearchParameters>(s => s.Count == AppCfgReader.PageSize &&
                                                                                                 s.Page == page &&
                                                                                                 s.OrderBy == "Created" &&
                                                                                                 s.OrderDirection == "desc" &&
                                                                                                 s.SearchQuery == id.ToString())),
                                               Times.Once);
            result.Should().NotBeNull();
        }

        [Test]
        [TestCase(0)]
        [TestCase(-100)]
        public void GetPostsCountByCategory_WithInvalidId_ShouldThrowException(int id)
        {
            Assert.Throws<BusinessLogicException>(() => target.GetPostsCountByCategory(id));
        }

        [Test]
        [TestCase(1)]
        public void GetPostsCountByCategory_WithValidId_ShouldReturnCount(int id)
        {
            const int expected = 3;
            postRepositoryMock.Instance.Setup(m => m.GetPostsCountByCategory(It.IsAny<int>())).Returns(expected);

            int result = target.GetPostsCountByCategory(id);

            result.Should().Be(expected);
            postRepositoryMock.Instance.Verify(m => m.GetPostsCountByCategory(id), Times.Once);
        }

        [Test]
        [TestCase(1)]
        public void GetPostsCountByCategory_WithValidIdAndCountInCache_ShouldReturnCount(int id)
        {
            const int expected = 3;
            string cacheKey = CacheKeys.CategoryCounter + id.ToString();
            string cacheRegion = CacheRegions.Posts.ToString();
            cacheMock.Instance.Object.Add(cacheKey, expected, AppCfgReader.CategoriesListCacheTime, cacheRegion);
            postRepositoryMock.Instance.Setup(m => m.GetPostsCountByCategory(It.IsAny<int>())).Returns(expected);

            int result = target.GetPostsCountByCategory(id);

            result.Should().Be(expected);
            cacheMock.Instance.Verify(m => m.Get(cacheKey, cacheRegion), Times.Once);
            postRepositoryMock.Instance.Verify(m => m.GetPostsCountByCategory(id), Times.Never);
        }

        [Test]
        [TestCase(1)]
        public void GetPostsCountByCategory_WithValidIdAndCountNotInCache_ShouldReturnCountAndPutInCache(int id)
        {
            const int expected = 3;
            string cacheKey = CacheKeys.CategoryCounter + id.ToString();
            string cacheRegion = CacheRegions.Posts.ToString();
            postRepositoryMock.Instance.Setup(m => m.GetPostsCountByCategory(It.IsAny<int>())).Returns(expected);

            int result = target.GetPostsCountByCategory(id);

            result.Should().Be(expected);
            cacheMock.Instance.Verify(m => m.Add(cacheKey, expected, 60, cacheRegion), Times.Once);
            postRepositoryMock.Instance.Verify(m => m.GetPostsCountByCategory(id), Times.Once);
        }

        [Test]
        [TestCase(0)]
        [TestCase(-100)]
        public void GetTaggedPostsCountByTag_WithInvalidId_ShouldThrowException(int id)
        {
            Assert.Throws<BusinessLogicException>(() => target.GetTaggedPostsCountByTag(id));
        }

        [Test]
        [TestCase(1)]
        public void GetTaggedPostsCountByTag_WithValidIdAndCountInCache_ShouldReturnCount(int id)
        {
            const int expected = 3;
            string cacheKey = CacheKeys.TaggedPostsCounter + id.ToString();
            string cacheRegion = CacheRegions.Posts.ToString();
            cacheMock.Instance.Object.Add(cacheKey, expected, AppCfgReader.TagsListCacheTime, cacheRegion);
            postRepositoryMock.Instance.Setup(m => m.GetTaggedPostsCountByTag(It.IsAny<int>())).Returns(expected);

            int result = target.GetTaggedPostsCountByTag(id);

            result.Should().Be(expected);
            cacheMock.Instance.Verify(m => m.Get(cacheKey, cacheRegion), Times.Once);
            postRepositoryMock.Instance.Verify(m => m.GetTaggedPostsCountByTag(id), Times.Never);
        }

        [Test]
        [TestCase(1)]
        public void GetTaggedPostsCountByTag_WithValidIdAndCountNotInCache_ShouldReturnCountAndPutInCache(int id)
        {
            const int expected = 3;
            string cacheKey = CacheKeys.TaggedPostsCounter + id.ToString();
            string cacheRegion = CacheRegions.Posts.ToString();
            postRepositoryMock.Instance.Setup(m => m.GetTaggedPostsCountByTag(It.IsAny<int>())).Returns(expected);

            int result = target.GetTaggedPostsCountByTag(id);

            result.Should().Be(expected);
            cacheMock.Instance.Verify(m => m.Add(cacheKey, expected, 60, cacheRegion), Times.Once);
            postRepositoryMock.Instance.Verify(m => m.GetTaggedPostsCountByTag(id), Times.Once);
        }

        [Test]
        public void GetTopRatedPosts_WithExceptionThrownFromRepository_ShouldThrowException()
        {
            postRepositoryMock.Instance.Setup(m => m.Search(It.IsAny<SearchParameters>())).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.GetTopRatedPosts());
        }

        [Test]
        public void GetTopRatedPosts_WithResultsInCache_ShouldReturnPostsFromCache()
        {
            var expected = new SearchResult<Post>
            {
                Data = postRepositoryMock.Store
            };
            string cacheKey = CacheKeys.TopRatedPosts.ToString();
            string cacheRegion = CacheRegions.Posts.ToString();
            cacheMock.Store.Add(cacheKey + "`" + cacheRegion, expected);

            SearchResult<Post> result = target.GetTopRatedPosts();

            result.Should().NotBeNull();
            postRepositoryMock.Instance.Verify(m => m.Search(It.IsAny<SearchParameters>()), Times.Never);
            cacheMock.Instance.Verify(m => m.Get(cacheKey, cacheRegion), Times.Once);
            cacheMock.Instance.Verify(m => m.Add(cacheKey, result, AppCfgReader.TopRatedPostsCacheTime, cacheRegion), Times.Never);
        }

        [Test]
        public void GetTopRatedPosts_WithResultsNotInCache_ShouldSearchPostsAndPutInCache()
        {
            string cacheKey = CacheKeys.TopRatedPosts.ToString();
            string cacheRegion = CacheRegions.Posts.ToString();

            SearchResult<Post> result = target.GetTopRatedPosts();

            result.Should().NotBeNull();
            postRepositoryMock.Instance.Verify(m => m.Search(It.IsAny<SearchParameters>()), Times.Once);
            cacheMock.Instance.Verify(m => m.Get(cacheKey, cacheRegion), Times.Once);
            cacheMock.Instance.Verify(m => m.Add(cacheKey, result, AppCfgReader.TopRatedPostsCacheTime, cacheRegion), Times.Once);
        }

        [Test]
        public void GetTopRatedPosts_WithResultsNotInCache_ShouldSearchWithCorrectParams()
        {
            SearchResult<Post> result = target.GetTopRatedPosts();

            result.Should().NotBeNull();
            postRepositoryMock.Instance.Verify(
                                               m => m.Search(It.Is<SearchParameters>(s => s.Count == AppCfgReader.PageSize &&
                                                                                          s.Page == 1 &&
                                                                                          s.OrderBy == "(Rating.VotesUp - Rating.VotesDown)" &&
                                                                                          s.OrderDirection == "desc")),
                                               Times.Once);
        }

        [Test]
        public void Search_WithCorrectParams_ShouldReturnPostsFromStorage()
        {
            var param = new SearchParameters
            {
                Count = 2,
                Page = 5
            };

            SearchResult<Post> result = target.Search(param);

            postRepositoryMock.Instance.Verify(m => m.Search(param), Times.Once);
            result.Should().NotBeNull();
        }

        [Test]
        public void Search_WithExceptionThrownFromRepository_ShouldThrowException()
        {
            var param = new SearchParameters();

            postRepositoryMock.Instance.Setup(m => m.Search(param)).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.Search(param));
        }

        [Test]
        [TestCase(null)]
        public void Search_WithNullSearchParameters_ShouldThrowException(SearchParameters param)
        {
            Assert.Throws<BusinessLogicException>(() => target.Search(param));
        }

        [Test]
        public void SearchSimilar_WithCorrectParams_ShouldPerformSearch()
        {
            const string query = "query";
            const int records = 10;

            SearchResult<Post> result = target.SearchSimilar(query, records);

            searchEngineMock.Verify(m => m.Search(It.Is<string>(q => q == query), It.Is<int>(r => r == records)), Times.Once);
            postRepositoryMock.Instance.Verify(m => m.Get(It.IsAny<int>()), Times.Never);
            result.Should().NotBeNull();
        }

        [Test]
        [TestCase(-100)]
        [TestCase(0)]
        public void SearchSimilar_WithInvalidRecordsParam_ShouldGetParamFromConfig(int records)
        {
            const string query = "query";

            target.SearchSimilar(query, records);

            searchEngineMock.Verify(m => m.Search(It.Is<string>(q => q == query), It.Is<int>(r => r == AppCfgReader.PageSize)), Times.Once);
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void SearchSimilar_WithNullSearchQuery_ShouldThrowException(string query)
        {
            Assert.Throws<BusinessLogicException>(() => target.SearchSimilar(query, 0));
        }

        [Test]
        public void SearchSimilarPosts_WithCorrectParams_ShouldPerformSearch()
        {
            const int records = 10;

            SearchResult<Post> result = target.SearchSimilar(testPost, records);

            searchEngineMock.Verify(m => m.Search(It.IsAny<Post>(), It.Is<int>(r => r == records)), Times.Once);
            postRepositoryMock.Instance.Verify(m => m.Get(It.IsAny<int>()), Times.Never);
            result.Should().NotBeNull();
        }

        [Test]
        [TestCase(-100)]
        [TestCase(0)]
        public void SearchSimilarPosts_WithInvalidRecordsParam_ShouldGetParamFromConfig(int records)
        {
            target.SearchSimilar(testPost, records);

            searchEngineMock.Verify(m => m.Search(It.IsAny<Post>(), It.Is<int>(r => r == AppCfgReader.PageSize)), Times.Once);
        }

        [Test]
        [TestCase(null)]
        public void SearchSimilarPosts_WithNullPost_ShouldThrowException(Post post)
        {
            Assert.Throws<BusinessLogicException>(() => target.SearchSimilar(post, 0));
        }

        [Test]
        public void Update_PostWithoutId_ShouldThrowException()
        {
            var entity = new Post
            {
                Category = new Category
                {
                    Id = 1
                }
            };

            Assert.Throws<BusinessLogicException>(() => target.Update(entity));
        }

        [Test]
        public void Update_WithCorrectPost_ShouldAddToStorage()
        {
            Post entity = testPost;

            Post result = target.Update(entity);

            result.Should().NotBeNull();
            result.Should().Be(entity);
            postRepositoryMock.Instance.Verify(m => m.Update(entity), Times.Once);
        }

        [Test]
        public void Update_WithCorrectPost_ShouldNotResetCacheIfPostNotInCache()
        {
            Post entity = testPost;

            Post result = target.Update(entity);

            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.LatestPosts.ToString(), CacheRegions.Posts.ToString()), Times.Never);
        }

        [Test]
        public void Update_WithCorrectPost_ShouldResetCacheIfPostInCache()
        {
            cacheMock.Instance.Object.Add(CacheKeys.LatestPosts.ToString(),
                                          new SearchResult<Post>
                                          {
                                              Data = postRepositoryMock.Store
                                          },
                                          AppCfgReader.LatestPostsCacheTime,
                                          CacheRegions.Posts.ToString());

            Post entity = testPost;

            Post result = target.Update(entity);

            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.LatestPosts.ToString(), CacheRegions.Posts.ToString()), Times.Once);
        }

        [Test]
        public void Update_WithCorrectPostAndNewTags_ShouldResetCache()
        {
            testPost.Tags = new List<Tag>
            {
                new Tag
                {
                    Id = 0
                }
            };

            Post result = target.Update(testPost);

            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.Tags.ToString(), CacheRegions.Tags.ToString()), Times.Once);
            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.TaggedPostsCounter.ToString(), CacheRegions.Tags.ToString()), Times.Once);
        }

        [Test]
        public void Update_WithExceptionThrownFromRepository_ShouldThrowException()
        {
            Post entity = testPost;

            postRepositoryMock.Instance.Setup(m => m.Update(entity)).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.Update(entity));
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Update_WithNullBodyStart_ShouldThrowException(string body)
        {
            testPost.BodyStart = body;

            Assert.Throws<BusinessLogicException>(() => target.Update(testPost));
        }

        [Test]
        public void Update_WithNullCategory_ShouldThrowException()
        {
            testPost.Category = null;

            Assert.Throws<BusinessLogicException>(() => target.Update(testPost));
        }

        [Test]
        [TestCase(null)]
        public void Update_WithNullPost_ShouldThrowException(Post entity)
        {
            Assert.Throws<BusinessLogicException>(() => target.Update(entity));
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Update_WithNullTitle_ShouldThrowException(string title)
        {
            testPost.Title = title;

            Assert.Throws<BusinessLogicException>(() => target.Update(testPost));
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Update_WithNullUrlSlug_ShouldThrowException(string urlslug)
        {
            testPost.UrlSlug = urlslug;

            Assert.Throws<BusinessLogicException>(() => target.Update(testPost));
        }

        [Test]
        public void Update_WithZeroCategoryId_ShouldThrowException()
        {
            testPost.Category.Id = 0;

            Assert.Throws<BusinessLogicException>(() => target.Update(testPost));
        }

        #endregion
    }
}