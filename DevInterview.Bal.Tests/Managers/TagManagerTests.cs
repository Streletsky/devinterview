﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using DevInterview.Bal.Managers.Business;
using DevInterview.Bal.Managers.Business.Interfaces;
using DevInterview.Bal.Tests.Mocks;
using DevInterview.Common.Configuration;
using DevInterview.Common.Enums;
using DevInterview.Common.Exceptions.Bal;
using DevInterview.Common.Exceptions.Dal;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace DevInterview.Bal.Tests.Managers
{
    [TestFixture]
    public class TagManagerTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            tagRepositoryMock = new TagRepositoryMock(FakeDataProvider.PopulateTagStore());
            tagRepositoryMock.Instance.Setup(m => m.GetOnlyTagsInUse())
                             .Returns(new SearchResult<Tag>
                             {
                                 Data = tagRepositoryMock.Store,
                                 Records = tagRepositoryMock.Store.Count,
                                 TotalRecords = tagRepositoryMock.Store.Count
                             });

            cacheMock = new CacheManagerMock(new Dictionary<string, object>());

            target = new TagManager(tagRepositoryMock.Instance.Object, cacheMock.Instance.Object);

            testEntity = new Tag
            {
                Id = 1,
                Title = "title",
                UrlSlug = "url"
            };
        }

        #endregion


        #region All other members

        private CacheManagerMock cacheMock;

        private TagRepositoryMock tagRepositoryMock;

        private ITagManager target;

        private Tag testEntity;

        #endregion


        #region Test Methods

        [Test]
        public void Create_WithCorrectTag_ShouldAddToStorage()
        {
            Tag result = target.Create(testEntity);

            result.Should().NotBeNull();
            result.Should().Be(testEntity);
            tagRepositoryMock.Instance.Verify(m => m.Insert(testEntity), Times.Once);
        }

        [Test]
        public void Create_WithCorrectTag_ShouldResetCache()
        {
            Tag result = target.Create(testEntity);

            cacheMock.Instance.Verify(c => c.Remove(CacheKeys.Tags.ToString(), CacheRegions.Tags.ToString()));
        }

        [Test]
        public void Create_WithExceptionThrownFromRepository_ShouldThrowException()
        {
            tagRepositoryMock.Instance.Setup(m => m.Insert(testEntity)).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.Create(testEntity));
        }

        [Test]
        [TestCase(null)]
        public void Create_WithNullTag_ShouldThrowException(Tag entity)
        {
            Assert.Throws<BusinessLogicException>(() => target.Create(entity));
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Create_WithNullTitle_ShouldThrowException(string title)
        {
            testEntity.Title = title;

            Assert.Throws<BusinessLogicException>(() => target.Create(testEntity));
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Create_WithNullUrlSlug_ShouldThrowException(string urlslug)
        {
            testEntity.UrlSlug = urlslug;

            Assert.Throws<BusinessLogicException>(() => target.Create(testEntity));
        }

        [Test]
        [TestCase(1)]
        public void Delete_WithExceptionThrownFromRepository_ShouldThrowException(int id)
        {
            tagRepositoryMock.Instance.Setup(m => m.Delete(id)).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.Delete(id));
        }

        [Test]
        [TestCase(1)]
        public void Delete_WithValidId_ShouldDeleteFromStorage(int id)
        {
            target.Delete(id);

            tagRepositoryMock.Instance.Verify(m => m.Delete(id), Times.Once);
        }

        [Test]
        [TestCase(1)]
        public void Delete_WithValidId_ShouldResetCache(int id)
        {
            target.Delete(id);

            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.Tags.ToString(), CacheRegions.Tags.ToString()), Times.Once);
        }

        [Test]
        [TestCase(0)]
        [TestCase(-100)]
        public void Delete_WithWrongId_ShouldThrowException(int id)
        {
            Assert.Throws<BusinessLogicException>(() => target.Delete(id));
        }

        [Test]
        [TestCase(1)]
        public void DeletetestEntity_WithExceptionThrownFromRepository_ShouldThrowException(int id)
        {
            tagRepositoryMock.Instance.Setup(m => m.Delete(It.Is<Tag>(t => t.Id == testEntity.Id))).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.Delete(testEntity));
        }

        [Test]
        [TestCase(null)]
        public void DeletetestEntity_WithNulltestEntity_ShouldThrowException(Tag tag)
        {
            Assert.Throws<BusinessLogicException>(() => target.Delete(tag));
        }

        [Test]
        [TestCase(1)]
        public void DeletetestEntity_WithValidId_ShouldDeleteFromStorage(int id)
        {
            target.Delete(testEntity);

            tagRepositoryMock.Instance.Verify(m => m.Delete(It.Is<Tag>(t => t.Id == testEntity.Id)), Times.Once);
        }

        [Test]
        [TestCase(1)]
        public void DeletetestEntity_WithValidId_ShouldResetCache(int id)
        {
            target.Delete(testEntity);

            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.Tags.ToString(), CacheRegions.Tags.ToString()), Times.Once);
        }

        [Test]
        [TestCase(0)]
        [TestCase(-100)]
        public void DeletetestEntity_WithWrongId_ShouldThrowException(int id)
        {
            var tag = new Tag
            {
                Id = id,
                Title = "title",
                UrlSlug = "url"
            };

            Assert.Throws<BusinessLogicException>(() => target.Delete(tag));
        }

        [Test]
        [TestCase(1)]
        public void Get_WithExceptionThrownFromRepository_ShouldThrowException(int id)
        {
            tagRepositoryMock.Instance.Setup(m => m.Get(id)).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.Get(id));
        }

        [Test]
        [TestCase(0)]
        [TestCase(-100)]
        public void Get_WithInvalidId_ShouldThrowException(int id)
        {
            Assert.Throws<BusinessLogicException>(() => target.Get(id));
        }

        [Test]
        [TestCase(1)]
        public void Get_WithTagNotFoundInDb_ShouldReturnNull(int id)
        {
            tagRepositoryMock.Instance.Setup(m => m.Get(id)).Returns((Tag)null);

            Tag result = target.Get(id);

            result.Should().BeNull();
        }

        [Test]
        [TestCase(1)]
        public void Get_WithValidId_ShouldReturnTagFromDb(int id)
        {
            Tag expected = tagRepositoryMock.Store.FirstOrDefault(c => c.Id == id);

            Tag result = target.Get(id);

            result.Should().NotBeNull();
            result.Should().Be(expected);
            tagRepositoryMock.Instance.Verify(m => m.Get(It.IsAny<int>()), Times.Once);
        }

        [Test]
        public void GetAll_WithExceptionThrownFromRepository_ShouldThrowException()
        {
            tagRepositoryMock.Instance.Setup(m => m.GetOnlyTagsInUse()).Throws(new DataAccessException());

            Assert.Throws<DataAccessException>(() => target.GetAll());
        }

        [Test]
        public void GetAll_WithTagsInCache_ShouldReturnTagsFromCache()
        {
            string cacheKey = CacheKeys.Tags.ToString();
            string cacheRegion = CacheRegions.Tags.ToString();
            var expected = new SearchResult<Tag>
            {
                Data = tagRepositoryMock.Store,
                Records = tagRepositoryMock.Store.Count,
                TotalRecords = tagRepositoryMock.Store.Count
            };
            cacheMock.Instance.Object.Add(cacheKey, expected, AppCfgReader.TagsListCacheTime, cacheRegion);

            SearchResult<Tag> result = target.GetAll();

            result.Should().NotBeNull();
            result.Should().Be(expected);
            cacheMock.Instance.Verify(m => m.Get(cacheKey, cacheRegion), Times.Once);
            tagRepositoryMock.Instance.Verify(m => m.GetAll(), Times.Never);
        }

        [Test]
        public void GetAll_WithTagsNotFoundInDb_ShouldReturnNull()
        {
            tagRepositoryMock.Instance.Setup(m => m.GetOnlyTagsInUse()).Returns((SearchResult<Tag>)null);

            SearchResult<Tag> result = target.GetAll();

            result.Should().BeNull();
        }

        [Test]
        public void GetAll_WithTagsNotInCache_ShouldReturnTagsFromDbAndAddToCache()
        {
            string cacheKey = CacheKeys.Tags.ToString();
            string cacheRegion = CacheRegions.Tags.ToString();
            var expected = new SearchResult<Tag>
            {
                Data = tagRepositoryMock.Store,
                Records = tagRepositoryMock.Store.Count,
                TotalRecords = tagRepositoryMock.Store.Count
            };

            SearchResult<Tag> result = target.GetAll();

            result.Should().NotBeNull();
            result.Data.Should().BeEquivalentTo(expected.Data);
            result.Records.Should().Be(expected.Records);
            result.TotalRecords.Should().Be(expected.TotalRecords);
            cacheMock.Instance.Verify(m => m.Get(cacheKey, cacheRegion), Times.Once);
            cacheMock.Instance.Verify(
                                      m =>
                                          m.Add(cacheKey,
                                                It.Is<SearchResult<Tag>>(searchResult => searchResult.Data == expected.Data &&
                                                                                         searchResult.Records == expected.Records &&
                                                                                         searchResult.TotalRecords == expected.TotalRecords),
                                                AppCfgReader.TagsListCacheTime,
                                                cacheRegion),
                                      Times.Once);
            tagRepositoryMock.Instance.Verify(m => m.GetOnlyTagsInUse(), Times.Once);
        }

        [Test]
        public void Update_TagWithoutId_ShouldThrowException()
        {
            var tag = new Tag
            {
                Id = 0,
                Title = "title",
                UrlSlug = "url"
            };

            Assert.Throws<BusinessLogicException>(() => target.Update(tag));
        }

        [Test]
        public void Update_WithCorrectTag_ShouldAddToStorage()
        {
            Tag result = target.Update(testEntity);

            result.Should().NotBeNull();
            result.Should().Be(testEntity);
            tagRepositoryMock.Instance.Verify(m => m.Update(testEntity), Times.Once);
        }

        [Test]
        public void Update_WithCorrectTag_ShouldResetCache()
        {
            Tag result = target.Update(testEntity);

            cacheMock.Instance.Verify(m => m.Remove(CacheKeys.Tags.ToString(), CacheRegions.Tags.ToString()), Times.Once);
        }

        [Test]
        public void Update_WithExceptionThrownFromRepository_ShouldThrowException()
        {
            tagRepositoryMock.Instance.Setup(m => m.Update(testEntity)).Throws(new DataException());

            Assert.Throws<DataAccessException>(() => target.Update(testEntity));
        }

        [Test]
        [TestCase(null)]
        public void Update_WithNullTag_ShouldThrowException(Tag tag)
        {
            Assert.Throws<BusinessLogicException>(() => target.Update(tag));
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Update_WithNullTitle_ShouldThrowException(string title)
        {
            testEntity.Title = title;

            Assert.Throws<BusinessLogicException>(() => target.Update(testEntity));
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void Update_WithNullUrlSlug_ShouldThrowException(string urlslug)
        {
            testEntity.UrlSlug = urlslug;

            Assert.Throws<BusinessLogicException>(() => target.Update(testEntity));
        }

        #endregion
    }
}