﻿using System.Collections.Generic;
using System.Linq;
using DevInterview.Dal.Entities.Base;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Dal.Repositories.Base;
using Moq;

namespace DevInterview.Bal.Tests.Mocks.Base
{
    public abstract class BaseRepositoryMock<TEntity, TId, TRepository> where TEntity : IEntity<TId> where TRepository : class, IRepository<TEntity, TId>
    {
        #region Constructors and Destructors

        protected BaseRepositoryMock(List<TEntity> store)
        {
            Store = store;
            Instance = new Mock<TRepository>();
            CreateMock(Instance);
        }

        #endregion


        #region Public Properties

        public Mock<TRepository> Instance { get; set; }

        public IList<TEntity> Store { get; set; }

        #endregion


        #region Methods

        protected virtual void CreateMock(Mock<TRepository> repoMock)
        {
            MockGet(repoMock);
            MockGetAll(repoMock);
            MockSearch(repoMock);
            MockInsert(repoMock);
            MockUpdate(repoMock);
        }

        private void MockGet(Mock<TRepository> mock)
        {
            mock.Setup(m => m.Get(It.IsAny<TId>())).Returns<TId>(id => Store.FirstOrDefault(u => u.Id.Equals(id)));
        }

        private void MockGetAll(Mock<TRepository> mock)
        {
            mock.Setup(m => m.GetAll())
                .Returns(() => new SearchResult<TEntity>
                {
                    Data = Store,
                    Records = Store.Count,
                    TotalRecords = Store.Count
                });
        }

        private void MockInsert(Mock<TRepository> mock)
        {
            mock.Setup(m => m.Insert(It.IsAny<TEntity>())).Returns<TEntity>(post => post);
        }

        private void MockSearch(Mock<TRepository> mock)
        {
            mock.Setup(m => m.Search(It.IsAny<SearchParameters>()))
                .Returns<SearchParameters>(param => new SearchResult<TEntity>
                {
                    Data = Store.Take(param.Count),
                    Records = Store.Count,
                    TotalRecords = Store.Count
                });
        }

        private void MockUpdate(Mock<TRepository> mock)
        {
            mock.Setup(m => m.Update(It.IsAny<TEntity>())).Returns<TEntity>(post => post);
        }

        #endregion
    }
}