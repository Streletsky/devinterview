﻿using System.Collections.Generic;
using System.Linq;
using DevInterview.Bal.Managers.Caching.Interfaces;
using DevInterview.Common.Utilities.Extensions;
using Moq;

namespace DevInterview.Bal.Tests.Mocks
{
    public class CacheManagerMock
    {
        #region Constants

        private const char Separator = '`';

        #endregion


        #region Constructors and Destructors

        public CacheManagerMock(IDictionary<string, object> store)
        {
            Store = store;
            Instance = GetMock();
        }

        #endregion


        #region Public Properties

        public Mock<ICacheManager> Instance { get; set; }

        public IDictionary<string, object> Store { get; set; }

        #endregion


        #region Methods

        private string GetKey(string key, string region)
        {
            if (region == null)
            {
                return key;
            }

            return key + Separator + region;
        }

        private Mock<ICacheManager> GetMock()
        {
            var cacheMock = new Mock<ICacheManager>();

            MockGet(cacheMock);
            MockAdd(cacheMock);
            MockClear(cacheMock);
            MockRemove(cacheMock);
            MockContains(cacheMock);

            return cacheMock;
        }

        private string GetRegion(string key)
        {
            string[] parts = key.Split(Separator);

            if (parts.Length <= 1)
            {
                return null;
            }

            return parts[0];
        }

        private void MockAdd(Mock<ICacheManager> cacheMock)
        {
            cacheMock.Setup(c => c.Add(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int>(), It.IsAny<string>()))
                     .Callback<string, object, int, string>((key, data, timeout, region) => Store.Add(GetKey(key, region), data));
        }

        private void MockClear(Mock<ICacheManager> cacheMock)
        {
            cacheMock.Setup(c => c.Clear(It.IsAny<string>()))
                     .Callback<string>(region =>
                     {
                         IEnumerable<string> items;
                         if (region.IsNullOrEmpty())
                         {
                             items = Store.Select(i => i.Key);
                         }
                         else
                         {
                             items = Store.Where(i => GetRegion(i.Key) == region).Select(i => i.Key);
                         }

                         foreach (string item in items)
                         {
                             Store.Remove(item);
                         }
                     });
        }

        private void MockContains(Mock<ICacheManager> cacheMock)
        {
            cacheMock.Setup(c => c.Contains(It.IsAny<string>(), It.IsAny<string>())).Returns<string, string>((key, region) => Store.ContainsKey(GetKey(key, region)));
        }

        private void MockGet(Mock<ICacheManager> cacheMock)
        {
            cacheMock.Setup(c => c.Get(It.IsAny<string>(), It.IsAny<string>()))
                     .Returns<string, string>((key, region) => Store.FirstOrDefault(i => i.Key == GetKey(key, region)).Value);
        }

        private void MockRemove(Mock<ICacheManager> cacheMock)
        {
            cacheMock.Setup(c => c.Remove(It.IsAny<string>(), It.IsAny<string>())).Callback<string, string>((key, region) => Store.Remove(GetKey(key, region)));
        }

        #endregion
    }
}