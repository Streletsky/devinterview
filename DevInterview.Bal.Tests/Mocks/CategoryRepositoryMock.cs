﻿using System.Collections.Generic;
using DevInterview.Bal.Tests.Mocks.Base;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Repositories.Business.Interfaces;

namespace DevInterview.Bal.Tests.Mocks
{
    public class CategoryRepositoryMock : BaseRepositoryMock<Category, int, ICategoryRepository>
    {
        #region Constructors and Destructors

        public CategoryRepositoryMock(List<Category> store) : base(store) { }

        #endregion
    }
}