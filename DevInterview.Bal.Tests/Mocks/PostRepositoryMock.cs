﻿using System.Collections.Generic;
using System.Linq;
using DevInterview.Bal.Tests.Mocks.Base;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Dal.Repositories.Business.Interfaces;
using Moq;

namespace DevInterview.Bal.Tests.Mocks
{
    public class PostRepositoryMock : BaseRepositoryMock<Post, int, IPostRepository>
    {
        #region Constructors and Destructors

        public PostRepositoryMock(List<Post> postStore) : base(postStore) { }

        #endregion


        #region Methods

        protected override void CreateMock(Mock<IPostRepository> mock)
        {
            MockGetPostsByTag(mock);

            base.CreateMock(mock);
        }

        private void MockGetPostsByTag(Mock<IPostRepository> mock)
        {
            mock.Setup(m => m.GetPostsByTag(It.IsAny<SearchParameters>()))
                .Returns<SearchParameters>(param => new SearchResult<Post>
                {
                    Data = Store.Take(param.Count),
                    Records = Store.Count,
                    TotalRecords = Store.Count
                });
        }

        #endregion
    }
}