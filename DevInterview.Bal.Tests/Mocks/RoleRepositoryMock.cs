﻿using System.Collections.Generic;
using System.Linq;
using DevInterview.Dal.Entities.Identity;
using DevInterview.Dal.Repositories.Identity.Interfaces;
using Moq;

namespace DevInterview.Bal.Tests.Mocks
{
    public class RoleRepositoryMock
    {
        #region Constructors and Destructors

        public RoleRepositoryMock(List<AppRole> store)
        {
            Store = store;
            Instance = GetMock();
        }

        #endregion


        #region Public Properties

        public Mock<IRoleRepositoryAsync> Instance { get; set; }

        public IList<AppRole> Store { get; set; }

        #endregion


        #region Methods

        private Mock<IRoleRepositoryAsync> GetMock()
        {
            var mock = new Mock<IRoleRepositoryAsync>();

            MockGet(mock);

            return mock;
        }

        private void MockGet(Mock<IRoleRepositoryAsync> mock)
        {
            mock.SetupGet(r => r.Roles).Returns(new EnumerableQuery<AppRole>(Store));
        }

        #endregion
    }
}