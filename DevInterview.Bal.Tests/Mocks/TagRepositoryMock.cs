﻿using System.Collections.Generic;
using DevInterview.Bal.Tests.Mocks.Base;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Repositories.Business.Interfaces;

namespace DevInterview.Bal.Tests.Mocks
{
    public class TagRepositoryMock : BaseRepositoryMock<Tag, int, ITagRepository>
    {
        #region Constructors and Destructors

        public TagRepositoryMock(List<Tag> store) : base(store) { }

        #endregion
    }
}