﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevInterview.Common.Utilities.Extensions;
using DevInterview.Dal.Entities.Identity;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Dal.Repositories.Identity.Interfaces;
using Moq;

namespace DevInterview.Bal.Tests.Mocks
{
    public class UserRepositoryMock
    {
        #region Constructors and Destructors

        public UserRepositoryMock(List<AppUser> store)
        {
            Store = store;
            Instance = GetMock();
        }

        #endregion


        #region Public Properties

        public Mock<IUserRepositoryAsync> Instance { get; set; }

        public List<AppUser> Store { get; set; }

        #endregion


        #region Methods

        private Mock<IUserRepositoryAsync> GetMock()
        {
            var userRepoMock = new Mock<IUserRepositoryAsync>();

            MockCreate(userRepoMock);
            MockFindById(userRepoMock);
            MockFindByName(userRepoMock);
            MockSetPasswordHash(userRepoMock);
            MockGetPasswordHash(userRepoMock);
            MockHasPasswordHash(userRepoMock);
            MockSearch(userRepoMock);

            return userRepoMock;
        }

        private void MockCreate(Mock<IUserRepositoryAsync> userRepoMock)
        {
            userRepoMock.Setup(r => r.CreateAsync(It.IsAny<AppUser>()))
                        .Returns(async (AppUser u) =>
                        {
                            await Task.Run(() =>
                            {
                                u.Id = Store.Max(a => a.Id) + 1;
                                Store.Add(u);
                            });
                        });
        }

        private void MockFindById(Mock<IUserRepositoryAsync> userRepoMock)
        {
            userRepoMock.Setup(r => r.FindByIdAsync(It.IsAny<int>())).Returns(async (int id) => await Task.FromResult(Store.SingleOrDefault(a => a.Id == id)));
        }

        private void MockFindByName(Mock<IUserRepositoryAsync> userRepoMock)
        {
            userRepoMock.Setup(r => r.FindByNameAsync(It.IsAny<string>())).Returns(async (string name) => await Task.FromResult(Store.SingleOrDefault(a => a.UserName == name)));
        }

        private void MockGetPasswordHash(Mock<IUserRepositoryAsync> userRepoMock)
        {
            userRepoMock.Setup(r => r.GetPasswordHashAsync(It.IsAny<AppUser>())).Returns(async (AppUser user) => await Task.FromResult(user.PasswordHash));
        }

        private void MockHasPasswordHash(Mock<IUserRepositoryAsync> userRepoMock)
        {
            userRepoMock.Setup(r => r.HasPasswordAsync(It.IsAny<AppUser>())).Returns(async (AppUser user) => await Task.FromResult(!user.PasswordHash.IsNullOrEmpty()));
        }

        private void MockSearch(Mock<IUserRepositoryAsync> userRepoMock)
        {
            userRepoMock.Setup(r => r.SearchAsync(It.IsAny<SearchParameters>()))
                        .Returns(async (SearchParameters p) => await Task.FromResult(new SearchResult<AppUser>
                        {
                            Data = Store.Take(p.Count),
                            Records = p.Count,
                            TotalRecords = Store.Count
                        }));
        }

        private void MockSetPasswordHash(Mock<IUserRepositoryAsync> userRepoMock)
        {
            userRepoMock.Setup(r => r.SetPasswordHashAsync(It.IsAny<AppUser>(), It.IsAny<string>()))
                        .Returns(async (AppUser user, string passHash) => await Task.Run(() => { user.PasswordHash = passHash; }));
        }

        #endregion
    }
}