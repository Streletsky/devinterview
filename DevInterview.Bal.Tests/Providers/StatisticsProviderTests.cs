﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevInterview.Bal.Managers.Business.Interfaces;
using DevInterview.Bal.Providers;
using DevInterview.Bal.Providers.Interfaces;
using DevInterview.Bal.Tests.Mocks;
using DevInterview.Common.Configuration;
using DevInterview.Common.Enums;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace DevInterview.Bal.Tests.Providers
{
    [TestFixture]
    public class StatisticsProviderTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            testPostsResult = new SearchResult<Post>
            {
                TotalRecords = FakeDataProvider.PopulatePostStore().Count,
                Records = FakeDataProvider.PopulatePostStore().Count,
                Data = FakeDataProvider.PopulatePostStore()
            };

            testCatsResult = new SearchResult<Category>
            {
                TotalRecords = FakeDataProvider.PopulateCategoryStore().Count,
                Records = FakeDataProvider.PopulateCategoryStore().Count,
                Data = FakeDataProvider.PopulateCategoryStore()
            };

            testTagsResult = new SearchResult<Tag>
            {
                TotalRecords = FakeDataProvider.PopulateTagStore().Count,
                Records = FakeDataProvider.PopulateTagStore().Count,
                Data = FakeDataProvider.PopulateTagStore()
            };

            testVotesResult = new Tuple<int, int>(FakeDataProvider.PopulatePostStore().Sum(p => p.Rating.VotesUp),
                                                  FakeDataProvider.PopulatePostStore().Sum(p => p.Rating.VotesDown));

            cacheMock = new CacheManagerMock(new Dictionary<string, object>());

            postManagerMock = new Mock<IPostManager>();
            postManagerMock.Setup(m => m.GetAll()).Returns(() => testPostsResult);

            catManagerMock = new Mock<ICategoryManager>();
            catManagerMock.Setup(m => m.GetAll()).Returns(() => testCatsResult);

            tagManagerMock = new Mock<ITagManager>();
            tagManagerMock.Setup(m => m.GetAll()).Returns(() => testTagsResult);

            target = new StatisticsProvider(cacheMock.Instance.Object, postManagerMock.Object, catManagerMock.Object, tagManagerMock.Object);
        }

        [TearDown]
        public void TearDown()
        {
            cacheMock.Store.Clear();
        }

        #endregion


        #region All other members

        private CacheManagerMock cacheMock;

        private Mock<IPostManager> postManagerMock;

        private Mock<ICategoryManager> catManagerMock;

        private Mock<ITagManager> tagManagerMock;

        private IStatisticsProvider target;

        private SearchResult<Post> testPostsResult;

        private SearchResult<Category> testCatsResult;

        private SearchResult<Tag> testTagsResult;

        private Tuple<int, int> testVotesResult;

        #endregion


        #region Test Methods

        [Test]
        public void CategoriesCount_WithCountInCache_ShouldGetCountFromCache()
        {
            cacheMock.Instance.Setup(m => m.Get(CacheKeys.CategoryCounter.ToString(), CacheRegions.Categories.ToString())).Returns(testCatsResult.TotalRecords);

            int result = target.CategoriesCount();

            cacheMock.Instance.Verify(m => m.Get(CacheKeys.CategoryCounter.ToString(), CacheRegions.Categories.ToString()), Times.Once);
            catManagerMock.Verify(m => m.GetAll(), Times.Never);
            cacheMock.Instance.Verify(m => m.Add(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int>(), It.IsAny<string>()), Times.Never);

            result.Should().Be(testCatsResult.TotalRecords);
        }

        [Test]
        public void CategoriesCount_WithoutCountInCache_ShouldCalculateFromStorageAndPutInCache()
        {
            int result = target.CategoriesCount();

            cacheMock.Instance.Verify(m => m.Get(CacheKeys.CategoryCounter.ToString(), CacheRegions.Categories.ToString()), Times.Once);
            catManagerMock.Verify(m => m.GetAll(), Times.Once);
            cacheMock.Instance.Verify(m => m.Add(CacheKeys.CategoryCounter.ToString(),
                                                 testCatsResult.TotalRecords,
                                                 AppCfgReader.CategoriesCountCacheTime,
                                                 CacheRegions.Categories.ToString()),
                                      Times.Once);

            result.Should().Be(testCatsResult.TotalRecords);
        }

        [Test]
        public void DaysSinceStart_ShouldCalculateDiffrenceBetweenTodayAndDateFromConfig()
        {
            var expected = (int)DateTime.Now.Subtract(AppCfgReader.ProjectStartDate).TotalDays;

            int result = target.DaysSinceStart();

            result.Should().Be(expected);
        }

        [Test]
        public void PostsCount_WithCountInCache_ShouldGetCountFromCache()
        {
            cacheMock.Instance.Setup(m => m.Get(CacheKeys.TotalPostsCounter.ToString(), CacheRegions.Posts.ToString())).Returns(testPostsResult.TotalRecords);

            int result = target.PostsCount();

            cacheMock.Instance.Verify(m => m.Get(CacheKeys.TotalPostsCounter.ToString(), CacheRegions.Posts.ToString()), Times.Once);
            postManagerMock.Verify(m => m.GetAll(), Times.Never);
            cacheMock.Instance.Verify(m => m.Add(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int>(), It.IsAny<string>()), Times.Never);

            result.Should().Be(testPostsResult.TotalRecords);
        }

        [Test]
        public void PostsCount_WithoutCountInCache_ShouldCalculateFromStorageAndPutInCache()
        {
            int result = target.PostsCount();

            cacheMock.Instance.Verify(m => m.Get(CacheKeys.TotalPostsCounter.ToString(), CacheRegions.Posts.ToString()), Times.Once);
            postManagerMock.Verify(m => m.GetAll(), Times.Once);
            cacheMock.Instance.Verify(m => m.Add(CacheKeys.TotalPostsCounter.ToString(),
                                                 testPostsResult.TotalRecords,
                                                 AppCfgReader.PostsCountCacheTime,
                                                 CacheRegions.Posts.ToString()),
                                      Times.Once);

            result.Should().Be(testPostsResult.TotalRecords);
        }

        [Test]
        public void TagsCount_WithCountInCache_ShouldGetCountFromCache()
        {
            cacheMock.Instance.Setup(m => m.Get(CacheKeys.TotalTagsCounter.ToString(), CacheRegions.Posts.ToString())).Returns(testTagsResult.TotalRecords);

            int result = target.TagsCount();

            cacheMock.Instance.Verify(m => m.Get(CacheKeys.TotalTagsCounter.ToString(), CacheRegions.Posts.ToString()), Times.Once);
            tagManagerMock.Verify(m => m.GetAll(), Times.Never);
            cacheMock.Instance.Verify(m => m.Add(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int>(), It.IsAny<string>()), Times.Never);

            result.Should().Be(testTagsResult.TotalRecords);
        }

        [Test]
        public void TagsCount_WithoutCountInCache_ShouldCalculateFromStorageAndPutInCache()
        {
            int result = target.TagsCount();

            cacheMock.Instance.Verify(m => m.Get(CacheKeys.TotalTagsCounter.ToString(), CacheRegions.Posts.ToString()), Times.Once);
            postManagerMock.Verify(m => m.GetAll(), Times.Never);
            cacheMock.Instance.Verify(m => m.Add(CacheKeys.TotalTagsCounter.ToString(),
                                                 testTagsResult.TotalRecords,
                                                 AppCfgReader.TagsCountCacheTime,
                                                 CacheRegions.Posts.ToString()),
                                      Times.Once);

            result.Should().Be(testTagsResult.TotalRecords);
        }

        [Test]
        public void VotesCount_WithCountInCache_ShouldGetCountFromCache()
        {
            cacheMock.Instance.Setup(m => m.Get(CacheKeys.TotalVotes.ToString(), CacheRegions.Posts.ToString())).Returns(testVotesResult);

            Tuple<int, int> result = target.VotesCount();

            cacheMock.Instance.Verify(m => m.Get(CacheKeys.TotalVotes.ToString(), CacheRegions.Posts.ToString()), Times.Once);
            postManagerMock.Verify(m => m.GetAll(), Times.Never);
            cacheMock.Instance.Verify(m => m.Add(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<int>(), It.IsAny<string>()), Times.Never);

            result.Should().Be(testVotesResult);
        }

        [Test]
        public void VotesCount_WithoutCountInCache_ShouldCalculateFromStorageAndPutInCache()
        {
            Tuple<int, int> result = target.VotesCount();

            cacheMock.Instance.Verify(m => m.Get(CacheKeys.TotalVotes.ToString(), CacheRegions.Posts.ToString()), Times.Once);
            postManagerMock.Verify(m => m.GetAll(), Times.Once);
            cacheMock.Instance.Verify(m => m.Add(CacheKeys.TotalVotes.ToString(), testVotesResult, AppCfgReader.VotesCacheTime, CacheRegions.Posts.ToString()), Times.Once);

            result.Should().Be(testVotesResult);
        }

        #endregion
    }
}