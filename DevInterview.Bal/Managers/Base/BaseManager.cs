﻿using System;
using System.Data;
using DevInterview.Common.Exceptions;
using DevInterview.Common.Exceptions.Bal;
using DevInterview.Common.Exceptions.Dal;
using DevInterview.Dal.Entities.Base;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Dal.Repositories.Base;

namespace DevInterview.Bal.Managers.Base
{
    /// <summary>
    ///     Manages <typeparamref name="TEntity" /> objects in system.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    public abstract class BaseManager<TEntity> : IManager<TEntity, int> where TEntity : class, IEntity<int>
    {
        #region Fields

        protected readonly IRepository<TEntity, int> Repository;

        #endregion


        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="BaseManager{TEntity}" /> class.
        /// </summary>
        /// <param name="repository">
        ///     Implementation of <see cref="IRepository{TEntity,TId}" />, which will be used for storage
        ///     access.
        /// </param>
        protected BaseManager(IRepository<TEntity, int> repository)
        {
            if (repository == null)
            {
                var inner = new ArgumentNullException(nameof(repository), "repository can not be null");

                throw new BusinessLogicException(
                                                 new ExceptionMessage
                                                 {
                                                     FailedOperationDescription = $"Can not instantiate manager for type {TypeName}.",
                                                     PossibleReason = "Repository implementation was not provided.",
                                                     TypeOfException = inner.GetType()
                                                 },
                                                 inner);
            }

            Repository = repository;
        }

        #endregion


        #region Properties

        protected string TypeName => typeof(TEntity).Name;

        #endregion


        #region Public Methods and Operators

        public virtual TEntity Create(TEntity entity)
        {
            ThrowNullEntityException(entity);

            try
            {
                entity = Repository.Insert(entity);
            }
            catch (DataException ex)
            {
                throw new DataAccessException(
                                              new ExceptionMessage
                                              {
                                                  FailedOperationDescription = $"Can not insert entity of type {TypeName} to storage.",
                                                  TypeOfException = ex.GetType(),
                                                  PossibleReason = ExceptionHelper.ReferenceToInnerException
                                              },
                                              ex);
            }

            ThrowNullEntityException(entity);
            ThrowWrongIdException(entity.Id);

            return entity;
        }

        public virtual void Delete(int id)
        {
            ThrowWrongIdException(id);

            try
            {
                Repository.Delete(id);
            }
            catch (DataException ex)
            {
                throw new DataAccessException(
                                              new ExceptionMessage
                                              {
                                                  FailedOperationDescription = $"Can not delete entity of type {TypeName} from storage.",
                                                  TypeOfException = ex.GetType(),
                                                  PossibleReason = ExceptionHelper.ReferenceToInnerException
                                              },
                                              ex);
            }
        }

        public virtual void Delete(TEntity entity)
        {
            ThrowNullEntityException(entity);
            ThrowWrongIdException(entity.Id);

            try
            {
                Repository.Delete(entity);
            }
            catch (DataException ex)
            {
                throw new DataAccessException(
                                              new ExceptionMessage
                                              {
                                                  FailedOperationDescription = $"Can not delete entity of type {TypeName} from storage.",
                                                  TypeOfException = ex.GetType(),
                                                  PossibleReason = ExceptionHelper.ReferenceToInnerException
                                              },
                                              ex);
            }
        }

        public virtual TEntity Get(int id)
        {
            ThrowWrongIdException(id);

            TEntity result;
            try
            {
                result = Repository.Get(id);
            }
            catch (DataException ex)
            {
                throw new DataAccessException(
                                              new ExceptionMessage
                                              {
                                                  FailedOperationDescription = $"Can not get entity of type {TypeName} by ID from storage.",
                                                  TypeOfException = ex.GetType(),
                                                  PossibleReason = ExceptionHelper.ReferenceToInnerException
                                              },
                                              ex);
            }

            return result;
        }

        public virtual SearchResult<TEntity> GetAll()
        {
            SearchResult<TEntity> result;
            try
            {
                result = Repository.GetAll();
            }
            catch (DataException ex)
            {
                throw new DataAccessException(
                                              new ExceptionMessage
                                              {
                                                  FailedOperationDescription = $"Can not get entities of type {TypeName} from storage.",
                                                  TypeOfException = ex.GetType(),
                                                  PossibleReason = ExceptionHelper.ReferenceToInnerException
                                              },
                                              ex);
            }

            return result;
        }

        public virtual SearchResult<TEntity> Search(SearchParameters param)
        {
            if (param == null)
            {
                var inner = new ArgumentNullException(nameof(param), "param should not be null.");

                throw new BusinessLogicException(
                                                 new ExceptionMessage
                                                 {
                                                     FailedOperationDescription = $"Search of {TypeName} can't be performed.",
                                                     TypeOfException = inner.GetType(),
                                                     PossibleReason = "Search parameters should not be null."
                                                 },
                                                 inner);
            }

            SearchResult<TEntity> result = null;
            try
            {
                result = Repository.Search(param);
            }
            catch (DataException ex)
            {
                throw new DataAccessException(
                                              new ExceptionMessage
                                              {
                                                  FailedOperationDescription = $"Can not perform search of entities of type {TypeName}.",
                                                  TypeOfException = ex.GetType(),
                                                  PossibleReason = ExceptionHelper.ReferenceToInnerException
                                              },
                                              ex);
            }

            return result;
        }

        public virtual TEntity Update(TEntity entity)
        {
            ThrowNullEntityException(entity);
            ThrowWrongIdException(entity.Id);

            try
            {
                entity = Repository.Update(entity);
            }
            catch (DataException ex)
            {
                throw new DataAccessException(
                                              new ExceptionMessage
                                              {
                                                  FailedOperationDescription = $"Can not update entity of type {TypeName} in storage.",
                                                  TypeOfException = ex.GetType(),
                                                  PossibleReason = ExceptionHelper.ReferenceToInnerException
                                              },
                                              ex);
            }

            return entity;
        }

        #endregion


        #region Methods

        /// <summary>
        ///     Throws exception if entity is null.
        /// </summary>
        /// <param name="entity">Entity to check.</param>
        /// <exception cref="BusinessLogicException"></exception>
        protected virtual void ThrowNullEntityException(TEntity entity)
        {
            if (entity == null)
            {
                var inner = new ArgumentNullException(nameof(entity), "Entity should not be null.");

                throw new BusinessLogicException(
                                                 new ExceptionMessage
                                                 {
                                                     FailedOperationDescription = $"Operation failed on null entity of type {TypeName}.",
                                                     TypeOfException = inner.GetType(),
                                                     PossibleReason = "Entity should not be null."
                                                 },
                                                 inner);
            }
        }

        /// <summary>
        ///     Throws exception if ID is incorrect.
        /// </summary>
        /// <param name="id">ID to check.</param>
        /// <exception cref="BusinessLogicException"></exception>
        protected virtual void ThrowWrongIdException(int id)
        {
            if (id <= 0)
            {
                var inner = new ArgumentException("ID should be positive.", nameof(id));

                throw new BusinessLogicException(
                                                 new ExceptionMessage
                                                 {
                                                     FailedOperationDescription = $"Operation failed on entity of type {TypeName} with negative or zero ID.",
                                                     TypeOfException = inner.GetType(),
                                                     PossibleReason = $"ID of entity can't be negative or zero. Passed ID is {id}."
                                                 },
                                                 inner);
            }
        }

        #endregion
    }
}