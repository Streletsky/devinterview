﻿using DevInterview.Dal.Entities.Base;
using DevInterview.Dal.Entities.NotBusiness;

namespace DevInterview.Bal.Managers.Base
{
    /// <summary>
    ///     Manages <typeparamref name="TEntity" /> objects in system.
    /// </summary>
    /// <typeparam name="TEntity">The type of the entity.</typeparam>
    /// <typeparam name="TId">The type of the identifier.</typeparam>
    public interface IManager<TEntity, in TId> where TEntity : class, IEntity<TId>
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Creates <typeparamref name="TEntity" /> in storage.
        /// </summary>
        /// <param name="entity">
        ///     New <typeparamref name="TEntity" />.
        /// </param>
        /// <returns>
        ///     Created <typeparamref name="TEntity" />.
        /// </returns>
        TEntity Create(TEntity entity);

        /// <summary>
        ///     Deletes <typeparamref name="TEntity" /> from storage by ID.
        /// </summary>
        /// <param name="id">
        ///     ID of <typeparamref name="TEntity" />.
        /// </param>
        void Delete(TId id);

        /// <summary>
        ///     Deletes <typeparamref name="TEntity" /> from storage.
        /// </summary>
        /// <param name="entity">
        ///     <typeparamref name="TEntity" /> to delete from storage.
        /// </param>
        void Delete(TEntity entity);

        /// <summary>
        ///     Gets <typeparamref name="TEntity" /> from storage by ID.
        /// </summary>
        /// <param name="id">
        ///     ID of <typeparamref name="TEntity" />.
        /// </param>
        /// <returns>
        ///     <typeparamref name="TEntity" /> from storage.
        /// </returns>
        TEntity Get(TId id);

        /// <summary>
        ///     Gets all <typeparamref name="TEntity" /> objects from storage.
        /// </summary>
        /// <returns>
        ///     <see cref="SearchResult{T}" /> with search result set.
        /// </returns>
        SearchResult<TEntity> GetAll();

        /// <summary>
        ///     Search <typeparamref name="TEntity" /> entity in storage by provided parameters.
        /// </summary>
        /// <param name="param">
        ///     Parameters for search.
        /// </param>
        /// <returns>
        ///     Found <typeparamref name="TEntity" /> entities wrapped in <see cref="SearchResult{TEntity}" /> class.
        /// </returns>
        SearchResult<TEntity> Search(SearchParameters param);

        /// <summary>
        ///     Updates <typeparamref name="TEntity" /> in storage.
        /// </summary>
        /// <param name="entity">
        ///     Updated <typeparamref name="TEntity" />.
        /// </param>
        /// <returns>
        ///     Updated <typeparamref name="TEntity" />.
        /// </returns>
        TEntity Update(TEntity entity);

        #endregion
    }
}