﻿using System.Linq;
using DevInterview.Bal.Managers.Base;
using DevInterview.Bal.Managers.Business.Interfaces;
using DevInterview.Bal.Managers.Caching.Interfaces;
using DevInterview.Common.Configuration;
using DevInterview.Common.Enums;
using DevInterview.Common.Exceptions;
using DevInterview.Common.Exceptions.Bal;
using DevInterview.Common.Utilities.Extensions;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Dal.Repositories.Business.Interfaces;

namespace DevInterview.Bal.Managers.Business
{
    /// <summary>
    ///     Manages <see cref="Category" /> objects in system.
    /// </summary>
    public class CategoryManager : BaseManager<Category>, ICategoryManager
    {
        #region Fields

        private readonly ICacheManager cache;

        #endregion


        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="CategoryManager" /> class.
        /// </summary>
        /// <param name="categoryRepository">
        ///     Implementation of <see cref="ICategoryRepository" /> repository, which will be used
        ///     for storage access.
        /// </param>
        /// <param name="cache">
        ///     Implementation of <see cref="ICacheManager" />, which will be used for cache access.
        /// </param>
        public CategoryManager(ICategoryRepository categoryRepository, ICacheManager cache) : base(categoryRepository)
        {
            this.cache = cache;
        }

        #endregion


        #region Public Methods and Operators

        public override Category Create(Category entity)
        {
            ValidateCategory(entity);

            Category result = base.Create(entity);

            cache.Remove(CacheKeys.Categories.ToString(), CacheRegions.Categories.ToString());

            return result;
        }

        public override void Delete(Category entity)
        {
            base.Delete(entity);

            cache.Remove(CacheKeys.Categories.ToString(), CacheRegions.Categories.ToString());
        }

        public override void Delete(int id)
        {
            base.Delete(id);

            cache.Remove(CacheKeys.Categories.ToString(), CacheRegions.Categories.ToString());
        }

        public override SearchResult<Category> GetAll()
        {
            var result = cache.Get(CacheKeys.Categories.ToString(), CacheRegions.Categories.ToString()) as SearchResult<Category>;

            if (result == null)
            {
                result = base.GetAll();
                if (result == null)
                {
                    return result;
                }

                result.Data = result.Data.OrderBy(c => c.Title);
                cache.Add(CacheKeys.Categories.ToString(), result, AppCfgReader.CategoriesListCacheTime, CacheRegions.Categories.ToString());
            }

            return result;
        }

        public override Category Update(Category entity)
        {
            ValidateCategory(entity);

            Category result = base.Update(entity);

            cache.Remove(CacheKeys.Categories.ToString(), CacheRegions.Categories.ToString());

            return result;
        }

        #endregion


        #region Methods

        private void ValidateCategory(Category entity)
        {
            ThrowNullEntityException(entity);

            var inner = new ValidationException();
            var exThrown = false;

            if (entity.Title.IsNullOrEmpty())
            {
                inner = new ValidationException("Title of a Post can not be null or empty.");
                exThrown = true;
            }

            if (entity.UrlSlug.IsNullOrEmpty())
            {
                inner = new ValidationException("UrlSlug of a Post can not be null or empty.");
                exThrown = true;
            }

            if (exThrown)
            {
                throw new BusinessLogicException(
                                                 new ExceptionMessage
                                                 {
                                                     FailedOperationDescription = $"Can not create or update entry of type {TypeName}.",
                                                     TypeOfException = inner.GetType(),
                                                     PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                 },
                                                 inner);
            }
        }

        #endregion
    }
}