﻿using DevInterview.Bal.Managers.Base;
using DevInterview.Dal.Entities.Business;

namespace DevInterview.Bal.Managers.Business.Interfaces
{
    /// <summary>
    ///     Manages <see cref="Category" /> objects in system.
    /// </summary>
    public interface ICategoryManager : IManager<Category, int> { }
}