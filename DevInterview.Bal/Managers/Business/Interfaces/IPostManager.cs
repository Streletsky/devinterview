﻿using DevInterview.Bal.Managers.Base;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;

namespace DevInterview.Bal.Managers.Business.Interfaces
{
    /// <summary>
    ///     Manages <see cref="Post" /> objects in system.
    /// </summary>
    public interface IPostManager : IManager<Post, int>
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Gets latest <see cref="Post" /> entries.
        /// </summary>
        /// <returns>
        ///     <see cref="SearchResult" /> with results data set.
        /// </returns>
        SearchResult<Post> GetLatestPosts();

        /// <summary>
        ///     Gets most viewed <see cref="Post" /> entries.
        /// </summary>
        /// <returns>
        ///     <see cref="SearchResult" /> with results data set.
        /// </returns>
        SearchResult<Post> GetMostViewedPosts();

        /// <summary>
        ///     Gets <see cref="Post" /> entries, which have <see cref="Category" /> with given ID.
        /// </summary>
        /// <param name="catId">
        ///     ID of <see cref="Category" />.
        /// </param>
        /// <param name="page">
        ///     Number of page.
        /// </param>
        /// <returns>
        ///     Counter of <see cref="Post" />.
        /// </returns>
        SearchResult<Post> GetPostsByCategory(int catId, int page);

        /// <summary>
        ///     Gets <see cref="Post" /> entries, which have <see cref="Tag" /> with given ID.
        /// </summary>
        /// <param name="tagId">
        ///     ID of <see cref="Tag" />.
        /// </param>
        /// <param name="page">
        ///     Number of page.
        /// </param>
        /// <returns>
        ///     Counter of <see cref="Post" />.
        /// </returns>
        SearchResult<Post> GetPostsByTag(int tagId, int page);

        /// <summary>
        ///     Gets count of <see cref="Post" />, which have <see cref="Category" /> with given ID.
        /// </summary>
        /// <param name="id">
        ///     ID of <see cref="Category" />.
        /// </param>
        /// <returns>
        ///     Counter of <see cref="Post" />.
        /// </returns>
        int GetPostsCountByCategory(int id);

        /// <summary>
        ///     Gets count of <see cref="Post" />, which have <see cref="Tag" /> with given ID.
        /// </summary>
        /// <param name="id">
        ///     ID of <see cref="Tag" />.
        /// </param>
        /// <returns>
        ///     Counter of <see cref="Post" />.
        /// </returns>
        int GetTaggedPostsCountByTag(int id);

        /// <summary>
        ///     Gets top rated <see cref="Post" /> entries.
        /// </summary>
        /// <returns>
        ///     <see cref="SearchResult" /> with results data set.
        /// </returns>
        SearchResult<Post> GetTopRatedPosts();

        /// <summary>
        ///     Searches <see cref="Post" /> entries with Lucene search engine.
        /// </summary>
        /// <param name="searchQuery">
        ///     The search query.
        /// </param>
        /// <param name="records">
        ///     Number of records to get.
        /// </param>
        /// <returns>
        ///     <see cref="SearchResult" /> with results data set.
        /// </returns>
        SearchResult<Post> SearchSimilar(string searchQuery, int records);

        /// <summary>
        ///     Searches <see cref="Post" /> entries with Lucene search engine.
        /// </summary>
        /// <param name="post">
        ///     <see cref="Post" />, which will be used as search criteria.
        /// </param>
        /// <param name="records">
        ///     Number of records to get.
        /// </param>
        /// <returns>
        ///     <see cref="SearchResult" /> with results data set.
        /// </returns>
        SearchResult<Post> SearchSimilar(Post post, int records);

        #endregion
    }
}