﻿using DevInterview.Bal.Managers.Base;
using DevInterview.Dal.Entities.Business;

namespace DevInterview.Bal.Managers.Business.Interfaces
{
    /// <summary>
    ///     Manages <see cref="Tag" /> entities in system.
    /// </summary>
    public interface ITagManager : IManager<Tag, int> { }
}