﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using DevInterview.Bal.Managers.Base;
using DevInterview.Bal.Managers.Business.Interfaces;
using DevInterview.Bal.Managers.Caching.Interfaces;
using DevInterview.Bal.SearchEngine.Base;
using DevInterview.Common.Configuration;
using DevInterview.Common.Enums;
using DevInterview.Common.Exceptions;
using DevInterview.Common.Exceptions.Bal;
using DevInterview.Common.Exceptions.Dal;
using DevInterview.Common.Utilities.Extensions;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Dal.Repositories.Business.Interfaces;
using Lucene.Net.Documents;

namespace DevInterview.Bal.Managers.Business
{
    /// <summary>
    ///     Manages <see cref="Post" /> objects in system.
    /// </summary>
    public class PostManager : BaseManager<Post>, IPostManager
    {
        #region Fields

        private readonly ICacheManager cache;

        private readonly ILuceneSearchEngine searchEngine;

        private IPostRepository postRepository;

        #endregion


        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="PostManager" /> class.
        /// </summary>
        /// <param name="postRepository">
        ///     Implementation of <see cref="IPostRepository" /> repository, which will be used for
        ///     storage access.
        /// </param>
        /// <param name="cache">
        ///     Implementation of <see cref="ICacheManager" />, which will be used for cache access.
        /// </param>
        /// <param name="searchEngine">
        ///     Implementation of <see cref="ILuceneSearchEngine" />, which will be used for text search in <see cref="Post" />
        ///     entities.
        /// </param>
        public PostManager(IPostRepository postRepository, ICacheManager cache, ILuceneSearchEngine searchEngine) : base(postRepository)
        {
            this.cache = cache;
            this.searchEngine = searchEngine;
        }

        #endregion


        #region Properties

        private IPostRepository PostRepository
        {
            get
            {
                if (postRepository == null)
                {
                    postRepository = Repository as IPostRepository;
                    if (postRepository == null)
                    {
                        var inner = new InvalidCastException("Can't cast type.");

                        throw new BusinessLogicException(
                                                         new ExceptionMessage
                                                         {
                                                             FailedOperationDescription = "Can not cast IRepository to IPostRepository.",
                                                             TypeOfException = inner.GetType(),
                                                             PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                         },
                                                         inner);
                    }
                }

                return postRepository;
            }
        }

        #endregion


        #region Public Methods and Operators

        public override Post Create(Post entity)
        {
            ValidatePost(entity);
            ClearHtml(entity);

            Post result = base.Create(entity);

            ClearCountersInCache(entity);

            cache.Remove(CacheKeys.LatestPosts.ToString(), CacheRegions.Posts.ToString());

            searchEngine.AddPostToIndex(result);

            return result;
        }

        public override void Delete(int id)
        {
            Post post = Get(id);
            ClearCountersInCache(post);
            searchEngine.RemovePostFromIndex(post);

            base.Delete(id);

            ClearCachedPostByIdInLatestPosts(id);
            CheckCachedPostByIdInMostViewedPosts(id);
            CheckCachedPostByIdInTopRatedPosts(id);
        }

        public override void Delete(Post entity)
        {
            base.Delete(entity);

            ClearCountersInCache(entity);
            ClearCachedPostByIdInLatestPosts(entity.Id);
            CheckCachedPostByIdInMostViewedPosts(entity.Id);
            CheckCachedPostByIdInTopRatedPosts(entity.Id);

            searchEngine.RemovePostFromIndex(entity);
        }

        public SearchResult<Post> GetLatestPosts()
        {
            SearchResult<Post> result = null;
            try
            {
                result = cache.Get(CacheKeys.LatestPosts.ToString(), CacheRegions.Posts.ToString()) as SearchResult<Post>;

                if (result == null)
                {
                    var param = new SearchParameters
                    {
                        Page = 1,
                        Count = AppCfgReader.PageSize,
                        OrderBy = "Created",
                        OrderDirection = "desc",
                        SearchBy = ""
                    };

                    result = Repository.Search(param);
                    cache.Add(CacheKeys.LatestPosts.ToString(), result, AppCfgReader.LatestPostsCacheTime, CacheRegions.Posts.ToString());
                }
            }
            catch (DataException ex)
            {
                throw new DataAccessException(
                                              new ExceptionMessage
                                              {
                                                  FailedOperationDescription = $"Can not perform search of entities of type {TypeName}.",
                                                  TypeOfException = ex.GetType(),
                                                  PossibleReason = ExceptionHelper.ReferenceToInnerException
                                              },
                                              ex);
            }

            return result;
        }

        public SearchResult<Post> GetMostViewedPosts()
        {
            SearchResult<Post> result = null;
            try
            {
                result = cache.Get(CacheKeys.MostViewedPosts.ToString(), CacheRegions.Posts.ToString()) as SearchResult<Post>;

                if (result == null)
                {
                    var param = new SearchParameters
                    {
                        Page = 1,
                        Count = AppCfgReader.PageSize,
                        OrderBy = "Views",
                        OrderDirection = "desc"
                    };

                    result = Repository.Search(param);
                    cache.Add(CacheKeys.MostViewedPosts.ToString(), result, AppCfgReader.MostViewedPostsCacheTime, CacheRegions.Posts.ToString());
                }
            }
            catch (DataException ex)
            {
                throw new DataAccessException(
                                              new ExceptionMessage
                                              {
                                                  FailedOperationDescription = $"Can not perform search of entities of type {TypeName}.",
                                                  TypeOfException = ex.GetType(),
                                                  PossibleReason = ExceptionHelper.ReferenceToInnerException
                                              },
                                              ex);
            }

            return result;
        }

        public SearchResult<Post> GetPostsByCategory(int catId, int page)
        {
            ThrowWrongIdException(catId);

            return PostRepository.GetPostsByCategory(new SearchParameters
            {
                Count = AppCfgReader.PageSize,
                Page = page,
                OrderBy = "Created",
                OrderDirection = "desc",
                CustomSearchQuery =
                    "Category.Id = " +
                    catId // Custom search query, because default approach throws EF exception: Operator '=' incompatible with operand types 'Int32' and 'String' (at index 12)
            });
        }

        public SearchResult<Post> GetPostsByTag(int tagId, int page)
        {
            ThrowWrongIdException(tagId);

            return PostRepository.GetPostsByTag(new SearchParameters
            {
                Count = AppCfgReader.PageSize,
                Page = page,
                OrderBy = "Created",
                OrderDirection = "desc",
                SearchQuery = tagId.ToString()
            });
        }

        public int GetPostsCountByCategory(int id)
        {
            ThrowWrongIdException(id);

            object result = cache.Get(CacheKeys.CategoryCounter + id.ToString(), CacheRegions.Posts.ToString());
            if (result == null)
            {
                result = PostRepository.GetPostsCountByCategory(id);
                cache.Add(CacheKeys.CategoryCounter + id.ToString(), result, 60, CacheRegions.Posts.ToString());
            }

            return (int)result;
        }

        public int GetTaggedPostsCountByTag(int id)
        {
            ThrowWrongIdException(id);

            object result = cache.Get(CacheKeys.TaggedPostsCounter + id.ToString(), CacheRegions.Posts.ToString());
            if (result == null)
            {
                result = PostRepository.GetTaggedPostsCountByTag(id);
                cache.Add(CacheKeys.TaggedPostsCounter + id.ToString(), result, 60, CacheRegions.Posts.ToString());
            }

            return (int)result;
        }

        public SearchResult<Post> GetTopRatedPosts()
        {
            SearchResult<Post> result = null;
            try
            {
                result = cache.Get(CacheKeys.TopRatedPosts.ToString(), CacheRegions.Posts.ToString()) as SearchResult<Post>;

                if (result == null)
                {
                    var param = new SearchParameters
                    {
                        Page = 1,
                        Count = AppCfgReader.PageSize,
                        OrderBy = "(Rating.VotesUp - Rating.VotesDown)",
                        OrderDirection = "desc"
                    };

                    result = Repository.Search(param);
                    cache.Add(CacheKeys.TopRatedPosts.ToString(), result, AppCfgReader.TopRatedPostsCacheTime, CacheRegions.Posts.ToString());
                }
            }
            catch (DataException ex)
            {
                throw new DataAccessException(
                                              new ExceptionMessage
                                              {
                                                  FailedOperationDescription = $"Can not perform search of entities of type {TypeName}.",
                                                  TypeOfException = ex.GetType(),
                                                  PossibleReason = ExceptionHelper.ReferenceToInnerException
                                              },
                                              ex);
            }

            return result;
        }

        public SearchResult<Post> SearchSimilar(string searchQuery, int records)
        {
            if (searchQuery.IsNullOrEmpty())
            {
                var inner = new ArgumentNullException(nameof(searchQuery), "searchQuery should not be null.");
                throw new BusinessLogicException(
                                                 new ExceptionMessage
                                                 {
                                                     FailedOperationDescription = $"Can not search entry of type {TypeName}.",
                                                     TypeOfException = inner.GetType(),
                                                     PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                 },
                                                 inner);
            }

            if (records <= 0)
            {
                records = AppCfgReader.PageSize;
            }

            IEnumerable<Document> searchResult = searchEngine.Search(searchQuery, records);

            return MapLuceneResults(searchResult.ToList(), records);
        }

        public SearchResult<Post> SearchSimilar(Post post, int records)
        {
            if (post == null)
            {
                var inner = new ArgumentNullException(nameof(post), "post should not be null.");
                throw new BusinessLogicException(
                                                 new ExceptionMessage
                                                 {
                                                     FailedOperationDescription = $"Can not search entry of type {TypeName}.",
                                                     TypeOfException = inner.GetType(),
                                                     PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                 },
                                                 inner);
            }

            if (records <= 0)
            {
                records = AppCfgReader.PageSize;
            }

            IEnumerable<Document> searchResult = searchEngine.Search(post, records);

            return MapLuceneResults(searchResult.ToList(), records);
        }

        public override Post Update(Post entity)
        {
            ValidatePost(entity);
            ClearHtml(entity);

            Post result = base.Update(entity);

            ClearCountersInCache(entity);
            ClearCachedPostByIdInLatestPosts(result.Id);
            CheckCachedPostByIdInMostViewedPosts(result.Id);
            CheckCachedPostByIdInTopRatedPosts(result.Id);

            return result;
        }

        #endregion


        #region Methods

        private void CheckCachedPostByIdInMostViewedPosts(int id)
        {
            if (cache.Contains(CacheKeys.MostViewedPosts.ToString(), CacheRegions.Posts.ToString()))
            {
                SearchResult<Post> mostViewedPosts = GetMostViewedPosts();
                if (mostViewedPosts != null && mostViewedPosts.Data.Any(p => p.Id == id))
                {
                    cache.Remove(CacheKeys.MostViewedPosts.ToString(), CacheRegions.Posts.ToString());
                }
            }
        }

        private void CheckCachedPostByIdInTopRatedPosts(int id)
        {
            if (cache.Contains(CacheKeys.TopRatedPosts.ToString(), CacheRegions.Posts.ToString()))
            {
                SearchResult<Post> topRatedPosts = GetTopRatedPosts();
                if (topRatedPosts != null && topRatedPosts.Data.Any(p => p.Id == id))
                {
                    cache.Remove(CacheKeys.TopRatedPosts.ToString(), CacheRegions.Posts.ToString());
                }
            }
        }

        private void ClearCachedPostByIdInLatestPosts(int id)
        {
            if (cache.Contains(CacheKeys.LatestPosts.ToString(), CacheRegions.Posts.ToString()))
            {
                SearchResult<Post> latestPosts = GetLatestPosts();
                if (latestPosts != null && latestPosts.Data.Any(p => p.Id == id))
                {
                    cache.Remove(CacheKeys.LatestPosts.ToString(), CacheRegions.Posts.ToString());
                }
            }
        }

        private void ClearCountersInCache(Post post)
        {
            cache.Remove(CacheKeys.Tags.ToString(), CacheRegions.Tags.ToString());
            cache.Remove(CacheKeys.TaggedPostsCounter.ToString(), CacheRegions.Tags.ToString());
            cache.Remove(CacheKeys.TotalTagsCounter.ToString(), CacheRegions.Tags.ToString());
            cache.Remove(CacheKeys.CategoryCounter + post.Category.Id.ToString(), CacheRegions.Posts.ToString());
            cache.Remove(CacheKeys.TotalPostsCounter.ToString(), CacheRegions.Posts.ToString());
        }

        private void ClearHtml(Post post)
        {
            post.BodyStart = post.BodyStart.Replace("&nbsp;", " ");

            if (post.BodyEnd != null)
            {
                post.BodyEnd = post.BodyEnd.Replace("&nbsp;", " ");
            }
        }

        private SearchResult<Post> MapLuceneResults(List<Document> searchResult, int records)
        {
            var posts = new List<Post>();
            var result = new SearchResult<Post>
            {
                TotalRecords = searchResult.Count(),
                Records = records
            };

            foreach (Document post in searchResult.Take(records))
            {
                int id;
                if (!int.TryParse(post.Get("Id"), out id))
                {
                    var inner = new ValidationException("Id of Post in Lucene search index is corrupted.");

                    throw new BusinessLogicException(
                                                     new ExceptionMessage
                                                     {
                                                         FailedOperationDescription =
                                                             $"Can not perform search of {TypeName} entity: can not retrieve Post from Lucene search index by ID.",
                                                         TypeOfException = inner.GetType(),
                                                         PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                     },
                                                     inner);
                }

                posts.Add(Get(id));
            }

            result.Data = posts;

            return result;
        }

        private void ValidatePost(Post entity)
        {
            ThrowNullEntityException(entity);

            var inner = new ValidationException();
            var exThrown = false;

            if (entity.Category == null)
            {
                inner = new ValidationException("Category of a Post can not be null.");
                exThrown = true;
            }
            else if (entity.Category.Id == 0)
            {
                inner = new ValidationException("ID of Category of a Post can not be 0.");
                exThrown = true;
            }

            if (entity.BodyStart.IsNullOrEmpty())
            {
                inner = new ValidationException("BodyStart of a Post can not be null or empty.");
                exThrown = true;
            }

            if (entity.Title.IsNullOrEmpty())
            {
                inner = new ValidationException("Title of a Post can not be null or empty.");
                exThrown = true;
            }

            if (entity.UrlSlug.IsNullOrEmpty())
            {
                inner = new ValidationException("UrlSlug of a Post can not be null or empty.");
                exThrown = true;
            }

            if (exThrown)
            {
                throw new BusinessLogicException(
                                                 new ExceptionMessage
                                                 {
                                                     FailedOperationDescription = $"Can not create or update entry of type {TypeName}.",
                                                     TypeOfException = inner.GetType(),
                                                     PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                 },
                                                 inner);
            }
        }

        #endregion
    }
}