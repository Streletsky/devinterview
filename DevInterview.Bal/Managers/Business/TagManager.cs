﻿using System;
using DevInterview.Bal.Managers.Base;
using DevInterview.Bal.Managers.Business.Interfaces;
using DevInterview.Bal.Managers.Caching.Interfaces;
using DevInterview.Common.Configuration;
using DevInterview.Common.Enums;
using DevInterview.Common.Exceptions;
using DevInterview.Common.Exceptions.Bal;
using DevInterview.Common.Utilities.Extensions;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Dal.Repositories.Business.Interfaces;

namespace DevInterview.Bal.Managers.Business
{
    /// <summary>
    ///     Manages <see cref="Tag" /> entities in system.
    /// </summary>
    public class TagManager : BaseManager<Tag>, ITagManager
    {
        #region Fields

        private readonly ICacheManager cache;

        private ITagRepository tagRepository;

        #endregion


        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="TagManager" /> class.
        /// </summary>
        /// <param name="tagRepository">
        ///     Implementation of <see cref="ITagRepository" />, which will be used for storage access.
        /// </param>
        /// <param name="cache">
        ///     Implementation of <see cref="ICacheManager" />, which will be used for cache access.
        /// </param>
        public TagManager(ITagRepository tagRepository, ICacheManager cache) : base(tagRepository)
        {
            this.cache = cache;
        }

        #endregion


        #region Properties

        private ITagRepository TagRepository
        {
            get
            {
                if (tagRepository == null)
                {
                    tagRepository = Repository as ITagRepository;
                    if (tagRepository == null)
                    {
                        var inner = new InvalidCastException("Can't cast type.");

                        throw new BusinessLogicException(
                                                         new ExceptionMessage
                                                         {
                                                             FailedOperationDescription = "Can not cast IRepository to ITagRepository.",
                                                             TypeOfException = inner.GetType(),
                                                             PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                         },
                                                         inner);
                    }
                }

                return tagRepository;
            }
        }

        #endregion


        #region Public Methods and Operators

        public override Tag Create(Tag entity)
        {
            ValidateTag(entity);

            Tag result = base.Create(entity);

            cache.Remove(CacheKeys.Tags.ToString(), CacheRegions.Tags.ToString());

            return result;
        }

        public override void Delete(Tag entity)
        {
            base.Delete(entity);

            cache.Remove(CacheKeys.Tags.ToString(), CacheRegions.Tags.ToString());
        }

        public override void Delete(int id)
        {
            base.Delete(id);

            cache.Remove(CacheKeys.Tags.ToString(), CacheRegions.Tags.ToString());
        }

        public override SearchResult<Tag> GetAll()
        {
            var result = cache.Get(CacheKeys.Tags.ToString(), CacheRegions.Tags.ToString()) as SearchResult<Tag>;

            if (result == null)
            {
                result = TagRepository.GetOnlyTagsInUse();
                cache.Add(CacheKeys.Tags.ToString(), result, AppCfgReader.TagsListCacheTime, CacheRegions.Tags.ToString());
            }

            return result;
        }

        public override Tag Update(Tag entity)
        {
            ValidateTag(entity);

            Tag result = base.Update(entity);

            cache.Remove(CacheKeys.Tags.ToString(), CacheRegions.Tags.ToString());

            return result;
        }

        #endregion


        #region Methods

        private void ValidateTag(Tag entity)
        {
            ThrowNullEntityException(entity);

            var inner = new ValidationException();
            var exThrown = false;

            if (entity.Title.IsNullOrEmpty())
            {
                inner = new ValidationException("Title of a Post can not be null or empty.");
                exThrown = true;
            }

            if (entity.UrlSlug.IsNullOrEmpty())
            {
                inner = new ValidationException("UrlSlug of a Post can not be null or empty.");
                exThrown = true;
            }

            if (exThrown)
            {
                throw new BusinessLogicException(
                                                 new ExceptionMessage
                                                 {
                                                     FailedOperationDescription = $"Can not create or update entry of type {TypeName}.",
                                                     TypeOfException = inner.GetType(),
                                                     PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                 },
                                                 inner);
            }
        }

        #endregion
    }
}