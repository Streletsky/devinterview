﻿namespace DevInterview.Bal.Managers.Caching.Interfaces
{
    /// <summary>
    ///     Provides assess to cache in system.
    /// </summary>
    public interface ICacheManager
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Adds object with specified key to cache.
        /// </summary>
        /// <param name="key">The key of object in cache.</param>
        /// <param name="data">Data to store in cache.</param>
        /// <param name="timeout">Minutes before object will be deleted from cache.</param>
        /// <param name="region">Optional region name in cache.</param>
        void Add(string key, object data, int timeout, string region = null);

        /// <summary>
        ///     Clears whole cache or specified region in it.
        /// </summary>
        /// <param name="region">Optional region name in cache.</param>
        void Clear(string region = null);

        /// <summary>
        ///     Determines whether cache contains object with the specified key.
        /// </summary>
        /// <param name="key">The key of object in cache.</param>
        /// <param name="region">Optional region name in cache.</param>
        /// <returns><c>True</c> if object with specified key is in cache.</returns>
        bool Contains(string key, string region = null);

        /// <summary>
        ///     Gets object with specified key from cache.
        /// </summary>
        /// <param name="key">The key of object in cache.</param>
        /// <param name="region">Optional region name in cache.</param>
        /// <returns>Object stored in cache.</returns>
        object Get(string key, string region = null);

        /// <summary>
        ///     Removes object with specified key from cache.
        /// </summary>
        /// <param name="key">The key of object in cache.</param>
        /// <param name="region">Optional region name in cache.</param>
        void Remove(string key, string region = null);

        #endregion
    }
}