﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using DevInterview.Bal.Managers.Caching.Interfaces;
using DevInterview.Common.Exceptions;
using DevInterview.Common.Exceptions.Bal;
using DevInterview.Common.Utilities.Extensions;

namespace DevInterview.Bal.Managers.Caching
{
    /// <summary>
    ///     Provides access to cache in system.
    /// </summary>
    public class MemoryCacheManager : ICacheManager
    {
        #region Constants

        private const char Separator = '`';

        #endregion


        #region Public Methods and Operators

        public void Add(string key, object data, int timeout, string region = null)
        {
            ThrowInvalidKeyException(key, "Cannot add item to cache with given key.");

            MemoryCache.Default.Add(GetKey(key, region), data, new DateTimeOffset(DateTime.Now.AddMinutes(timeout)));
        }

        public void Clear(string region = null)
        {
            IEnumerable<string> items;
            if (region.IsNullOrEmpty())
            {
                items = MemoryCache.Default.Select(i => i.Key);
            }
            else
            {
                items = MemoryCache.Default.Where(i => GetRegion(i.Key) == region).Select(i => i.Key);
            }

            foreach (string item in items)
            {
                MemoryCache.Default.Remove(item);
            }
        }

        public bool Contains(string key, string region = null)
        {
            ThrowInvalidKeyException(key, "Cannot check item in cache with given key.");

            return MemoryCache.Default.Contains(GetKey(key, region));
        }

        public object Get(string key, string region = null)
        {
            ThrowInvalidKeyException(key, "Cannot get item from MemoryCacheManager by given key.");

            return MemoryCache.Default.Get(GetKey(key, region));
        }

        public void Remove(string key, string region = null)
        {
            ThrowInvalidKeyException(key, "Cannot remove item from MemoryCacheManager with given key.");

            MemoryCache.Default.Remove(GetKey(key, region));
        }

        #endregion


        #region Methods

        private string GetKey(string key, string region)
        {
            if (region == null)
            {
                return key;
            }

            return key + Separator + region;
        }

        private string GetRegion(string key)
        {
            string[] parts = key.Split(Separator);

            return parts.Length <= 1 ? null : parts[1];
        }

        private void ThrowInvalidKeyException(string key, string operationDescription)
        {
            if (!key.IsNullOrEmpty() && !key.Contains("`"))
            {
                return;
            }

            var inner = new ArgumentException("key parameter cannot be null, empty or contain wrong characters: '`'", nameof(key));

            throw new BusinessLogicException(
                                             new ExceptionMessage
                                             {
                                                 FailedOperationDescription = operationDescription,
                                                 TypeOfException = inner.GetType(),
                                                 PossibleReason = "Key was null, empty or contained wrong characters: '`'."
                                             },
                                             inner);
        }

        #endregion
    }
}