﻿using DevInterview.Dal.Entities.Identity;
using DevInterview.Dal.Repositories.Identity.Interfaces;
using Microsoft.AspNet.Identity;

namespace DevInterview.Bal.Managers.Identity
{
    /// <summary>
    ///     Manages application roles in ASP.NET Identity way.
    /// </summary>
    public class AppRoleManager : RoleManager<AppRole, int>
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="AppRoleManager" /> class.
        /// </summary>
        /// <param name="store">
        ///     <see cref="IRoleRepositoryAsync" /> implementation which will be used for interaction with storage.
        /// </param>
        public AppRoleManager(IRoleRepositoryAsync store) : base(store) { }

        #endregion
    }
}