﻿using DevInterview.Dal.Entities.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;

namespace DevInterview.Bal.Managers.Identity
{
    /// <summary>
    ///     Manages Sign In operations for app users.
    /// </summary>
    public class AppSignInManager : SignInManager<AppUser, int>
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="AppSignInManager" /> class.
        /// </summary>
        /// <param name="userManager">
        ///     <see cref="AppUserManager" /> implementation, which will be used for managing app users.
        /// </param>
        /// <param name="authenticationManager">
        ///     <see cref="IAuthenticationManager" /> implementation, which will be used for auth operations.
        /// </param>
        public AppSignInManager(AppUserManager userManager, IAuthenticationManager authenticationManager) : base(userManager, authenticationManager) { }

        #endregion
    }
}