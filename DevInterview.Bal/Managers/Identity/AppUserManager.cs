﻿using System.Threading.Tasks;
using DevInterview.Dal.Entities.Identity;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Dal.Repositories.Identity.Interfaces;
using Microsoft.AspNet.Identity;

namespace DevInterview.Bal.Managers.Identity
{
    /// <summary>
    ///     Manages application users in ASP.NET Identity way.
    /// </summary>
    public class AppUserManager : UserManager<AppUser, int>
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="AppUserManager" /> class.
        /// </summary>
        /// <param name="store">
        ///     <see cref="IUserRepositoryAsync" /> implementation which will be used for interaction with storage.
        /// </param>
        public AppUserManager(IUserRepositoryAsync store) : base(store) { }

        #endregion


        #region Properties

        private IUserRepositoryAsync UserStore => Store as IUserRepositoryAsync;

        #endregion


        #region Public Methods and Operators

        public virtual async Task<SearchResult<AppUser>> GetAllAsync() => await UserStore.GetAllAsync();

        #endregion
    }
}