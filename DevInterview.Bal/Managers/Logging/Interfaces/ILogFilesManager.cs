﻿using System.Collections.Generic;
using DevInterview.Dal.Entities.NotBusiness;

namespace DevInterview.Bal.Managers.Logging.Interfaces
{
    public interface ILogFilesManager
    {
        #region Public Methods and Operators

        void Delete(string fileName);

        IEnumerable<LogFile> GetAll();

        #endregion
    }
}