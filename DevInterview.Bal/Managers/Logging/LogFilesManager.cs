﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using DevInterview.Bal.Managers.Logging.Interfaces;
using DevInterview.Common.Exceptions;
using DevInterview.Common.Exceptions.Bal;
using DevInterview.Dal.Entities.NotBusiness;

namespace DevInterview.Bal.Managers.Logging
{
    public class LogFilesManager : ILogFilesManager
    {
        #region Fields

        private readonly string DirectoryName = "~/Logs";

        #endregion


        #region Public Methods and Operators

        public void Delete(string fileName)
        {
            string path = GetFilePath(DirectoryName + "/" + fileName);
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        public IEnumerable<LogFile> GetAll()
        {
            string path = GetFilePath(DirectoryName);
            if (!Directory.Exists(path))
            {
                var inner = new FileNotFoundException("Log folder was not found.", DirectoryName);

                throw new BusinessLogicException(new ExceptionMessage
                                                 {
                                                     FailedOperationDescription = "Can not get list of log files.",
                                                     TypeOfException = inner.GetType(),
                                                     PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                 },
                                                 inner);
            }

            List<string> files = Directory.GetFiles(path).ToList();

            return files.Select(f => new LogFile
            {
                FileName = Path.GetFileName(f),
                LastModifiedOn = File.GetLastWriteTimeUtc(f)
            });
        }

        #endregion


        #region Methods

        private string GetFilePath(string filePath) => HostingEnvironment.MapPath(filePath);

        #endregion
    }
}