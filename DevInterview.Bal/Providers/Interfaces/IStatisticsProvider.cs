﻿using System;

namespace DevInterview.Bal.Providers.Interfaces
{
    public interface IStatisticsProvider
    {
        #region Public Methods and Operators

        int CategoriesCount();

        int DaysSinceStart();

        int PostsCount();

        int TagsCount();

        Tuple<int, int> VotesCount();

        #endregion
    }
}