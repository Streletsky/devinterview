﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevInterview.Bal.Managers.Business.Interfaces;
using DevInterview.Bal.Managers.Caching.Interfaces;
using DevInterview.Bal.Providers.Interfaces;
using DevInterview.Common.Configuration;
using DevInterview.Common.Enums;
using DevInterview.Dal.Entities.Business;

namespace DevInterview.Bal.Providers
{
    public class StatisticsProvider : IStatisticsProvider
    {
        #region Fields

        private readonly ICacheManager cache;

        private readonly ICategoryManager categoryManager;

        private readonly IPostManager postManager;

        private readonly ITagManager tagManager;

        #endregion


        #region Constructors and Destructors

        public StatisticsProvider(ICacheManager cache, IPostManager postManager, ICategoryManager categoryManager, ITagManager tagManager)
        {
            this.cache = cache;
            this.postManager = postManager;
            this.categoryManager = categoryManager;
            this.tagManager = tagManager;
        }

        #endregion


        #region Public Methods and Operators

        public int CategoriesCount()
        {
            object result = cache.Get(CacheKeys.CategoryCounter.ToString(), CacheRegions.Categories.ToString());
            if (result == null)
            {
                result = categoryManager.GetAll().TotalRecords;
                cache.Add(CacheKeys.CategoryCounter.ToString(), result, AppCfgReader.CategoriesCountCacheTime, CacheRegions.Categories.ToString());
            }

            return (int)result;
        }

        public int DaysSinceStart() => (int)DateTime.Now.Subtract(AppCfgReader.ProjectStartDate).TotalDays;

        public int PostsCount()
        {
            object result = cache.Get(CacheKeys.TotalPostsCounter.ToString(), CacheRegions.Posts.ToString());
            if (result == null)
            {
                result = postManager.GetAll().TotalRecords;
                cache.Add(CacheKeys.TotalPostsCounter.ToString(), result, AppCfgReader.PostsCountCacheTime, CacheRegions.Posts.ToString());
            }

            return (int)result;
        }

        public int TagsCount()
        {
            object result = cache.Get(CacheKeys.TotalTagsCounter.ToString(), CacheRegions.Posts.ToString());
            if (result == null)
            {
                result = tagManager.GetAll().TotalRecords;
                cache.Add(CacheKeys.TotalTagsCounter.ToString(), result, AppCfgReader.TagsCountCacheTime, CacheRegions.Posts.ToString());
            }

            return (int)result;
        }

        public Tuple<int, int> VotesCount()
        {
            object result = cache.Get(CacheKeys.TotalVotes.ToString(), CacheRegions.Posts.ToString());
            if (result == null)
            {
                List<Post> posts = postManager.GetAll().Data.ToList();
                int votesUp = posts.Select(p => p.Rating.VotesUp).Sum();
                int votesDown = posts.Select(p => p.Rating.VotesDown).Sum();
                result = new Tuple<int, int>(votesUp, votesDown);
                cache.Add(CacheKeys.TotalVotes.ToString(), result, AppCfgReader.VotesCacheTime, CacheRegions.Posts.ToString());
            }

            return result as Tuple<int, int>;
        }

        #endregion
    }
}