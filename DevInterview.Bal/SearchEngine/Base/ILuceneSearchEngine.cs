﻿using System;
using System.Collections.Generic;
using DevInterview.Dal.Entities.Business;
using Lucene.Net.Documents;

namespace DevInterview.Bal.SearchEngine.Base
{
    public interface ILuceneSearchEngine : IDisposable
    {
        #region Public Methods and Operators

        void AddPostToIndex(Post post);

        void AddPostToIndex(IEnumerable<Post> post);

        void ClearIndex();

        void RemovePostFromIndex(Post post);

        IEnumerable<Document> Search(string query, int records);

        IEnumerable<Document> Search(Post post, int records);

        #endregion
    }
}