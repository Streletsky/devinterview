﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;
using DevInterview.Bal.SearchEngine.Base;
using DevInterview.Common.Configuration;
using DevInterview.Common.Exceptions;
using DevInterview.Common.Exceptions.Bal;
using DevInterview.Dal.Entities.Business;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Directory = Lucene.Net.Store.Directory;
using Version = Lucene.Net.Util.Version;

namespace DevInterview.Bal.SearchEngine
{
    public class LuceneSearchEngine : ILuceneSearchEngine
    {
        #region Constants

        private const string BodyFieldName = "Body";

        private const string IdFieldName = "Id";

        private const string TagsFieldName = "Tags";

        private const string TitleFieldName = "Title";

        #endregion


        #region Static Fields

        private static FSDirectory directoryTemp;

        #endregion


        #region Properties

        protected Directory Directory
        {
            get
            {
                if (directoryTemp == null)
                {
                    var dir = new DirectoryInfo(FolderPath);
                    if (!dir.Exists)
                    {
                        dir = System.IO.Directory.CreateDirectory(FolderPath);
                    }

                    directoryTemp = FSDirectory.Open(dir, new SimpleFSLockFactory());
                }

                if (IndexWriter.IsLocked(directoryTemp))
                {
                    IndexWriter.Unlock(directoryTemp);
                }

                string lockFilePath = Path.Combine(FolderPath, "write.lock");
                if (File.Exists(lockFilePath))
                {
                    File.Delete(lockFilePath);
                }

                return directoryTemp;
            }
        }

        protected string FolderPath => HostingEnvironment.ApplicationPhysicalPath + "\\LuceneIndex";

        #endregion


        #region Public Methods and Operators

        public void AddPostToIndex(Post post)
        {
            AddPostToIndex(new List<Post>
            {
                post
            });
        }

        public void AddPostToIndex(IEnumerable<Post> posts)
        {
            var analyzer = new StandardAnalyzer(Version.LUCENE_30);
            using (var writer = new IndexWriter(Directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                foreach (Post post in posts)
                {
                    AddPostToIndex(post, writer);
                }

                writer.Commit();
                writer.Flush(true, true, true);
                analyzer.Close();
            }
        }

        public void ClearIndex()
        {
            var analyzer = new StandardAnalyzer(Version.LUCENE_30);
            using (var writer = new IndexWriter(Directory, analyzer, true, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                analyzer.Close();
                writer.DeleteAll();
                writer.Commit();
            }
        }

        /// <summary>
        ///     Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Directory.Dispose();
        }

        public void RemovePostFromIndex(Post post)
        {
            var analyzer = new StandardAnalyzer(Version.LUCENE_30);
            using (var writer = new IndexWriter(Directory, analyzer, true, IndexWriter.MaxFieldLength.UNLIMITED))
            {
                RemovePostFromIndex(post, writer);
                analyzer.Close();
            }
        }

        public IEnumerable<Document> Search(string searchQuery, int records)
        {
            if (string.IsNullOrEmpty(searchQuery.Replace("*", "").Replace("?", "")))
            {
                return new List<Document>();
            }

            var fields = new[]
            {
                BodyFieldName,
                TitleFieldName,
                TagsFieldName
            };

            var boosts = new Dictionary<string, float>
            {
                {
                    BodyFieldName, AppCfgReader.LuceneBodyFieldBoost
                },
                {
                    TitleFieldName, AppCfgReader.LuceneTitleFieldBoost
                },
                {
                    TagsFieldName, AppCfgReader.LuceneTagsFieldBoost
                }
            };

            using (var searcher = new IndexSearcher(Directory, true))
            {
                var analyzer = new StandardAnalyzer(Version.LUCENE_30);
                var parser = new MultiFieldQueryParser(Version.LUCENE_30, fields, analyzer, boosts)
                {
                    DefaultOperator = QueryParser.Operator.OR
                };
                Query query = ParseQuery(searchQuery, parser);
                List<Document> results = searcher.Search(query, null, records, Sort.RELEVANCE).ScoreDocs.Select(hit => searcher.Doc(hit.Doc)).ToList();

                analyzer.Close();

                return results;
            }
        }

        public IEnumerable<Document> Search(Post post, int records)
        {
            string queryForPost = post.Title + " " + string.Join(" ", post.Tags.Select(t => t.Title));

            return Search(queryForPost, records);
        }

        #endregion


        #region Methods

        private void AddPostToIndex(Post post, IndexWriter writer)
        {
            RemovePostFromIndex(post, writer);

            var doc = new Document();
            doc.Add(new Field(IdFieldName, post.Id.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED));
            doc.Add(new Field(BodyFieldName, post.BodyStart + post.BodyEnd, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field(TitleFieldName, post.Title, Field.Store.YES, Field.Index.ANALYZED));
            doc.Add(new Field(TagsFieldName, string.Join(" ", post.Tags.Select(t => t.Title)), Field.Store.YES, Field.Index.ANALYZED));

            writer.AddDocument(doc);
        }

        private Query ParseQuery(string searchQuery, MultiFieldQueryParser parser)
        {
            List<string> terms = searchQuery.ToLowerInvariant()
                                            .Replace("-", " ")
                                            .Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries)
                                            .Select(x => QueryParser.Escape(x) + "~")
                                            .ToList();
            var finalQuery = new BooleanQuery();

            try
            {
                foreach (string term in terms)
                {
                    finalQuery.Add(parser.Parse(term), Occur.SHOULD);
                }
            }
            catch (ParseException ex)
            {
                throw new BusinessLogicException(new ExceptionMessage
                                                 {
                                                     FailedOperationDescription = "Can not perform Lucene search.",
                                                     TypeOfException = ex.GetType(),
                                                     PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                 },
                                                 ex);
            }

            return finalQuery;
        }

        private void RemovePostFromIndex(Post post, IndexWriter writer)
        {
            var searchQuery = new TermQuery(new Term(IdFieldName, post.Id.ToString()));
            writer.DeleteDocuments(searchQuery);
            writer.Commit();
        }

        #endregion
    }
}