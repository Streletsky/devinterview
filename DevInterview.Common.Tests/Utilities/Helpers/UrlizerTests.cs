﻿using DevInterview.Common.Utilities.Helpers;
using FluentAssertions;
using NUnit.Framework;

namespace DevInterview.Common.Tests.Utilities.Helpers
{
    [TestFixture]
    public class UrlizerTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            target = new Urlizer();
        }

        #endregion


        #region All other members

        private IUrlizer target;

        #endregion


        #region Test Methods

        [Test]
        [TestCase("")]
        [TestCase(null)]
        public void GetFriendlyUrl_WithInvalidString_ShouldReturnNull(string str)
        {
            string result = target.GetFriendlyUrl(str);

            result.Should().BeNull();
        }

        [Test]
        public void GetFriendlyUrl_WithValidString_ShouldCollapseDashes()
        {
            const string str = "asp--net---mvc";
            var expected = "asp-net-mvc";

            string result = target.GetFriendlyUrl(str);

            result.Should().Be(expected);
        }

        [Test]
        public void GetFriendlyUrl_WithValidString_ShouldLowerCase()
        {
            const string str = "ASPDOTNET";
            string expected = str.ToLowerInvariant();

            string result = target.GetFriendlyUrl(str);

            result.Should().Be(expected);
        }

        [Test]
        public void GetFriendlyUrl_WithValidString_ShouldRemoveDashesFromBothSides()
        {
            const string str = "-aspnet--";
            var expected = "aspnet";

            string result = target.GetFriendlyUrl(str);

            result.Should().Be(expected);
        }

        [Test]
        public void GetFriendlyUrl_WithValidString_ShouldRemoveHtmlSpecialEntities()
        {
            const string str = "asp&nbsp;net";
            var expected = "aspnet";

            string result = target.GetFriendlyUrl(str);

            result.Should().Be(expected);
        }

        [Test]
        public void GetFriendlyUrl_WithValidString_ShouldRemoveNotAlphanumericChars()
        {
            const string str = "asp&;net?!!@$%^&#&))))";
            var expected = "aspnet";

            string result = target.GetFriendlyUrl(str);

            result.Should().Be(expected);
        }

        [Test]
        public void GetFriendlyUrl_WithValidString_ShouldReplaceWhiteSpaces()
        {
            const string str = "asp net mvc";
            var expected = "asp-net-mvc";

            string result = target.GetFriendlyUrl(str);

            result.Should().Be(expected);
        }

        #endregion
    }
}