﻿using System;

namespace DevInterview.Common.Configuration
{
    /// <summary>
    ///     Helper class, which helps to read configuration data from web.config using Cfg third-party library.
    /// </summary>
    public class AppCfgReader
    {
        #region Public Properties

        /// <summary>
        ///     Gets or sets the categories count cache time.
        /// </summary>
        /// <value>
        ///     The categories count cache time.
        /// </value>
        public static int CategoriesCountCacheTime { get; set; }

        /// <summary>
        ///     Gets or sets the categories list cache time.
        /// </summary>
        /// <value>
        ///     The categories list cache time.
        /// </value>
        public static int CategoriesListCacheTime { get; set; }

        /// <summary>
        ///     Gets or sets the Cookie Life Time in days.
        /// </summary>
        /// <value>
        ///     The cookie life time.
        /// </value>
        public static int CookieLifeTime { get; set; }

        /// <summary>
        ///     Gets or sets the Cookie Name.
        /// </summary>
        /// <value>
        ///     The cookie name.
        /// </value>
        public static string CookieName { get; set; }

        /// <summary>
        ///     Gets or sets the Cookie Viewed Posts Key.
        /// </summary>
        /// <value>
        ///     The cookie value key.
        /// </value>
        public static string CookieViewedPostsKey { get; set; }

        /// <summary>
        ///     Gets or sets the Cookie Voted Posts Key.
        /// </summary>
        /// <value>
        ///     The cookie value key.
        /// </value>
        public static string CookieVotedPostsKey { get; set; }

        /// <summary>
        ///     Gets or sets the name of the domain.
        /// </summary>
        /// <value>
        ///     The name of the domain.
        /// </value>
        public static string DomainName { get; set; }

        /// <summary>
        ///     Gets or sets the latest posts cache time.
        /// </summary>
        /// <value>
        ///     The latest posts cache time.
        /// </value>
        public static int LatestPostsCacheTime { get; set; }

        /// <summary>
        ///     Gets or sets the "Body" field boost (weight) for Lucene search engine.
        /// </summary>
        /// <value>
        ///     The "Body" field boost (weight) for Lucene search engine.
        /// </value>
        public static int LuceneBodyFieldBoost { get; set; }

        /// <summary>
        ///     Gets or sets the "Tags" field boost (weight) for Lucene search engine.
        /// </summary>
        /// <value>
        ///     The "Tags" field boost (weight) for Lucene search engine.
        /// </value>
        public static int LuceneTagsFieldBoost { get; set; }

        /// <summary>
        ///     Gets or sets the "Title" field boost (weight) for Lucene search engine.
        /// </summary>
        /// <value>
        ///     The "Title" field boost (weight) for Lucene search engine.
        /// </value>
        public static int LuceneTitleFieldBoost { get; set; }

        /// <summary>
        ///     Gets or sets the most viewed posts cache time.
        /// </summary>
        /// <value>
        ///     The most viewed posts cache time.
        /// </value>
        public static int MostViewedPostsCacheTime { get; set; }

        /// <summary>
        ///     Gets or sets the size of the page.
        /// </summary>
        /// <value>
        ///     The size of the page.
        /// </value>
        public static int PageSize { get; set; }

        /// <summary>
        ///     Gets or sets the posts count cache time.
        /// </summary>
        /// <value>
        ///     The posts count cache time.
        /// </value>
        public static int PostsCountCacheTime { get; set; }

        /// <summary>
        ///     Gets or sets the Prooject Start Date.
        /// </summary>
        /// <value>
        ///     The project start date.
        /// </value>
        public static DateTime ProjectStartDate { get; set; }

        /// <summary>
        ///     Gets or sets the similar posts list cache time.
        /// </summary>
        /// <value>
        ///     The similar posts list cache time.
        /// </value>
        public static int SimilarPostsCacheTime { get; set; }

        /// <summary>
        ///     Gets or sets the size of similar posts list.
        /// </summary>
        /// <value>
        ///     The size of similar posts list.
        /// </value>
        public static int SimilarPostsSize { get; set; }

        /// <summary>
        ///     Gets or sets the tags count cache time.
        /// </summary>
        /// <value>
        ///     The tags count cache time.
        /// </value>
        public static int TagsCountCacheTime { get; set; }

        /// <summary>
        ///     Gets or sets the tags list cache time.
        /// </summary>
        /// <value>
        ///     The tags list cache time.
        /// </value>
        public static int TagsListCacheTime { get; set; }

        /// <summary>
        ///     Gets or sets the top rated posts cache time.
        /// </summary>
        /// <value>
        ///     The top rated posts cache time.
        /// </value>
        public static int TopRatedPostsCacheTime { get; set; }

        /// <summary>
        ///     Gets or sets the votes cache time.
        /// </summary>
        /// <value>
        ///     The votes cache time.
        /// </value>
        public static int VotesCacheTime { get; set; }

        #endregion
    }
}