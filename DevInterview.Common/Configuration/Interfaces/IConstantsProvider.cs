﻿using System;

namespace DevInterview.Common.Configuration.Interfaces
{
    public interface IConstantsProvider
    {
        #region Public Properties

        /// <summary>
        ///     Gets the categories count cache time.
        /// </summary>
        /// <value>
        ///     The categories count cache time.
        /// </value>
        int CategoriesCountCacheTime { get; }

        /// <summary>
        ///     Gets the categories list cache time.
        /// </summary>
        /// <value>
        ///     The categories list cache time.
        /// </value>
        int CategoriesListCacheTime { get; }

        /// <summary>
        ///     Gets the Cookie Life Time in days.
        /// </summary>
        /// <value>
        ///     The cookie life time.
        /// </value>
        int CookieLifeTime { get; }

        /// <summary>
        ///     Gets the Cookie Name.
        /// </summary>
        /// <value>
        ///     The cookie name.
        /// </value>
        string CookieName { get; }

        /// <summary>
        ///     Gets the Cookie Viewed Posts Key.
        /// </summary>
        /// <value>
        ///     The cookie value key.
        /// </value>
        string CookieViewedPostsKey { get; }

        /// <summary>
        ///     Gets the Cookie Voted Posts Key.
        /// </summary>
        /// <value>
        ///     The cookie value key.
        /// </value>
        string CookieVotedPostsKey { get; }

        /// <summary>
        ///     Gets the name of the domain.
        /// </summary>
        /// <value>
        ///     The name of the domain.
        /// </value>
        string DomainName { get; }

        /// <summary>
        ///     Gets the latest posts cache time.
        /// </summary>
        /// <value>
        ///     The latest posts cache time.
        /// </value>
        int LatestPostsCacheTime { get; }

        /// <summary>
        ///     Gets the "Body" field boost (weight) for Lucene search engine.
        /// </summary>
        /// <value>
        ///     The "Body" field boost (weight) for Lucene search engine.
        /// </value>
        int LuceneBodyFieldBoost { get; }

        /// <summary>
        ///     Gets the "Tags" field boost (weight) for Lucene search engine.
        /// </summary>
        /// <value>
        ///     The "Tags" field boost (weight) for Lucene search engine.
        /// </value>
        int LuceneTagsFieldBoost { get; }

        /// <summary>
        ///     Gets the "Title" field boost (weight) for Lucene search engine.
        /// </summary>
        /// <value>
        ///     The "Title" field boost (weight) for Lucene search engine.
        /// </value>
        int LuceneTitleFieldBoost { get; }

        /// <summary>
        ///     Gets the most viewed posts cache time.
        /// </summary>
        /// <value>
        ///     The most viewed posts cache time.
        /// </value>
        int MostViewedPostsCacheTime { get; }

        /// <summary>
        ///     Gets the size of the page.
        /// </summary>
        /// <value>
        ///     The size of the page.
        /// </value>
        int PageSize { get; }

        /// <summary>
        ///     Gets the posts count cache time.
        /// </summary>
        /// <value>
        ///     The posts count cache time.
        /// </value>
        int PostsCountCacheTime { get; }

        /// <summary>
        ///     Gets the Prooject Start Date.
        /// </summary>
        /// <value>
        ///     The project start date.
        /// </value>
        DateTime ProjectStartDate { get; }

        /// <summary>
        ///     Gets the similar posts list cache time.
        /// </summary>
        /// <value>
        ///     The similar posts list cache time.
        /// </value>
        int SimilarPostsCacheTime { get; }

        /// <summary>
        ///     Gets the size of similar posts list.
        /// </summary>
        /// <value>
        ///     The size of similar posts list.
        /// </value>
        int SimilarPostsSize { get; }

        /// <summary>
        ///     Gets the tags count cache time.
        /// </summary>
        /// <value>
        ///     The tags count cache time.
        /// </value>
        int TagsCountCacheTime { get; }

        /// <summary>
        ///     Gets the tags list cache time.
        /// </summary>
        /// <value>
        ///     The tags list cache time.
        /// </value>
        int TagsListCacheTime { get; }

        /// <summary>
        ///     Gets the top rated posts cache time.
        /// </summary>
        /// <value>
        ///     The top rated posts cache time.
        /// </value>
        int TopRatedPostsCacheTime { get; }

        /// <summary>
        ///     Gets the votes cache time.
        /// </summary>
        /// <value>
        ///     The votes cache time.
        /// </value>
        int VotesCacheTime { get; }

        #endregion


        #region Public Methods and Operators

        /// <summary>
        ///     Get connection string by its name.
        /// </summary>
        /// <param name="name">
        ///     Name of connection string.
        /// </param>
        /// <returns>
        ///     Connection string.
        /// </returns>
        string GetConnectionString(string name);

        /// <summary>
        ///     Get default connection string.
        /// </summary>
        /// <returns>
        ///     Connection string.
        /// </returns>
        string GetDefaultConnectionString();

        #endregion
    }
}