using System.Collections.Generic;

namespace DevInterview.Common.Configuration.Interfaces
{
    public interface IWebConfigManager
    {
        #region Public Methods and Operators

        List<KeyValuePair<string, string>> GetAllAppSettings();

        void SaveAppSettingEntry(string key, string value);

        #endregion
    }
}