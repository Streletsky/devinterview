﻿using System;
using Cfg;
using DevInterview.Common.Configuration.Interfaces;

namespace DevInterview.Common.Configuration
{
    public class WebConfigConstantsProvider : IConstantsProvider
    {
        #region Public Properties

        public int CategoriesCountCacheTime => AppCfgReader.CategoriesCountCacheTime;

        public int CategoriesListCacheTime => AppCfgReader.CategoriesListCacheTime;

        public int CookieLifeTime => AppCfgReader.CookieLifeTime;

        public string CookieName => AppCfgReader.CookieName;

        public string CookieViewedPostsKey => AppCfgReader.CookieViewedPostsKey;

        public string CookieVotedPostsKey => AppCfgReader.CookieVotedPostsKey;

        public string DomainName => AppCfgReader.DomainName;

        public int LatestPostsCacheTime => AppCfgReader.LatestPostsCacheTime;

        public int LuceneBodyFieldBoost => AppCfgReader.LuceneBodyFieldBoost;

        public int LuceneTagsFieldBoost => AppCfgReader.LuceneTagsFieldBoost;

        public int LuceneTitleFieldBoost => AppCfgReader.LuceneTitleFieldBoost;

        public int MostViewedPostsCacheTime => AppCfgReader.MostViewedPostsCacheTime;

        public int PageSize => AppCfgReader.PageSize;

        public int PostsCountCacheTime => AppCfgReader.PostsCountCacheTime;

        public DateTime ProjectStartDate => AppCfgReader.ProjectStartDate;

        public int SimilarPostsCacheTime => AppCfgReader.SimilarPostsCacheTime;

        public int SimilarPostsSize => AppCfgReader.SimilarPostsSize;

        public int TagsCountCacheTime => AppCfgReader.TagsCountCacheTime;

        public int TagsListCacheTime => AppCfgReader.TagsListCacheTime;

        public int TopRatedPostsCacheTime => AppCfgReader.TopRatedPostsCacheTime;

        public int VotesCacheTime => AppCfgReader.VotesCacheTime;

        #endregion


        #region Public Methods and Operators

        public string GetConnectionString(string name) => ConnectionStrings.Get(name);

        public string GetDefaultConnectionString() => ConnectionStrings.Get("LocalConnection");

        #endregion
    }
}