﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Configuration;
using DevInterview.Common.Configuration.Interfaces;

namespace DevInterview.Common.Configuration
{
    public class WebConfigManager : IWebConfigManager
    {
        #region Public Methods and Operators

        public List<KeyValuePair<string, string>> GetAllAppSettings()
        {
            System.Configuration.Configuration config = WebConfigurationManager.OpenWebConfiguration("~");

            return (from KeyValueConfigurationElement setting in config.AppSettings.Settings
                    select new KeyValuePair<string, string>(setting.Key, setting.Value)).ToList();
        }

        public void SaveAppSettingEntry(string key, string value)
        {
            System.Configuration.Configuration config = WebConfigurationManager.OpenWebConfiguration("~");
            config.AppSettings.Settings[key].Value = value;
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");
        }

        #endregion
    }
}