﻿namespace DevInterview.Common.Enums
{
    /// <summary>
    ///     Keys for standard cache entries in system.
    /// </summary>
    public enum CacheKeys
    {
        TotalVotes,

        LatestPosts,

        TopRatedPosts,

        MostViewedPosts,

        Categories,

        Category,

        Tags,

        CategoryCounter,

        TaggedPostsCounter,

        TotalTagsCounter,

        TotalPostsCounter
    }
}