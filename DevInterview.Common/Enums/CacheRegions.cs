﻿namespace DevInterview.Common.Enums
{
    /// <summary>
    ///     Names of cache regions in cache.
    /// </summary>
    public enum CacheRegions
    {
        Categories,

        Posts,

        Tags
    }
}