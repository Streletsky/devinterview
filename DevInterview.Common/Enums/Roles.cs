﻿namespace DevInterview.Common.Enums
{
    /// <summary>
    ///     Standard roles in system.
    /// </summary>
    public enum Roles
    {
        Common,

        Publisher,

        Administrator
    }
}