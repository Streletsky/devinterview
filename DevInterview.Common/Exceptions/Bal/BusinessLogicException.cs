﻿using System;
using System.Runtime.Serialization;

namespace DevInterview.Common.Exceptions.Bal
{
    /// <summary>
    ///     Exception which can occur in Business Logic layer of application.
    /// </summary>
    [Serializable]
    public class BusinessLogicException : Exception, ISerializable
    {
        #region Constructors and Destructors

        public BusinessLogicException() { }

        public BusinessLogicException(string message) : base(message) { }

        public BusinessLogicException(string message, Exception inner) : base(message, inner) { }

        public BusinessLogicException(ExceptionMessage message, Exception inner) : base(ExceptionHelper.GetErrorMessage(message), inner) { }

        protected BusinessLogicException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        #endregion
    }
}