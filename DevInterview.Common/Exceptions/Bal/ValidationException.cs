﻿using System;
using System.Runtime.Serialization;

namespace DevInterview.Common.Exceptions.Bal
{
    /// <summary>
    ///     Exception which can occur due to entity not passed business logic validation.
    /// </summary>
    [Serializable]
    public class ValidationException : Exception, ISerializable
    {
        #region Constructors and Destructors

        public ValidationException() { }

        public ValidationException(string message) : base(message) { }

        public ValidationException(string message, Exception inner) : base(message, inner) { }

        protected ValidationException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        #endregion
    }
}