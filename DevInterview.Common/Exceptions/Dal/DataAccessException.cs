﻿using System;
using System.Runtime.Serialization;

namespace DevInterview.Common.Exceptions.Dal
{
    /// <summary>
    ///     Exception which can occur in Data Access layer of application.
    /// </summary>
    [Serializable]
    public class DataAccessException : Exception, ISerializable
    {
        #region Constructors and Destructors

        public DataAccessException() { }

        public DataAccessException(string message) : base(message) { }

        public DataAccessException(string message, Exception inner) : base(message, inner) { }

        public DataAccessException(ExceptionMessage message, Exception inner) : base(ExceptionHelper.GetErrorMessage(message), inner) { }

        protected DataAccessException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        #endregion
    }
}