﻿using System;

namespace DevInterview.Common.Exceptions
{
    /// <summary>
    ///     Helper class for exceptions generating and handling.
    /// </summary>
    public static class ExceptionHelper
    {
        #region Constants

        /// <summary>
        ///     Generic error message template.
        /// </summary>
        public const string ErrorMessageTemplate = "{0} {1} occured. {2} Check log and stack trace for details.";

        /// <summary>
        ///     Error message to point user to inner exception.
        /// </summary>
        public const string ReferenceToInnerException = "See inner exception for a reason.";

        #endregion


        #region Public Methods and Operators

        /// <summary>
        ///     Get detailed error message. Template is "{0} {1} occured. {2} Check log and stack trace for details."
        /// </summary>
        /// <param name="failedOperation">
        ///     Description of operation, which has failed.
        /// </param>
        /// <param name="typeOfException">
        ///     Exact type of thrown exception as string.
        /// </param>
        /// <param name="reason">
        ///     Possible reason of exception.
        /// </param>
        /// <returns>
        ///     Detailed exception message.
        /// </returns>
        public static string GetErrorMessage(string failedOperation, string typeOfException, string reason) => string.Format(ErrorMessageTemplate,
                                                                                                                             failedOperation,
                                                                                                                             typeOfException,
                                                                                                                             reason);

        /// <summary>
        ///     Get detailed error message. Template is "{0} {1} occured. {2} Check log and stack trace for details."
        /// </summary>
        /// <param name="failedOperation">
        ///     Description of operation, which has failed.
        /// </param>
        /// <param name="typeOfException">
        ///     Type of thrown exception.
        /// </param>
        /// <param name="reason">
        ///     Possible reason of exception.
        /// </param>
        /// <returns>
        ///     Detailed exception message.
        /// </returns>
        public static string GetErrorMessage(string failedOperation, Type typeOfException, string reason) => string.Format(ErrorMessageTemplate,
                                                                                                                           failedOperation,
                                                                                                                           typeOfException.Name,
                                                                                                                           reason);

        /// <summary>
        ///     Get detailed error message. Template is "{message.FailedOperationDescription} {message.TypeOfException} occured.
        ///     {message.PossibleReason} Check log and stack trace for details."
        /// </summary>
        /// <param name="message">
        ///     Structure with exception message details.
        /// </param>
        /// <returns>
        ///     Detailed exception message.
        /// </returns>
        public static string GetErrorMessage(ExceptionMessage message) => string.Format(ErrorMessageTemplate,
                                                                                        message.FailedOperationDescription,
                                                                                        message.TypeOfException.Name,
                                                                                        message.PossibleReason);

        #endregion
    }
}