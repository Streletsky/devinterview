﻿using System;

namespace DevInterview.Common.Exceptions
{
    /// <summary>
    ///     Information about exception thrown in system.
    /// </summary>
    public struct ExceptionMessage
    {
        #region Public Properties

        /// <summary>
        ///     Failed operation description.
        /// </summary>
        /// <value>
        ///     Failed operation description.
        /// </value>
        public string FailedOperationDescription { get; set; }

        /// <summary>
        ///     Possible reason of exception.
        /// </summary>
        /// <value>
        ///     Possible reason of exception.
        /// </value>
        public string PossibleReason { get; set; }

        /// <summary>
        ///     Type of exception.
        /// </summary>
        /// <value>
        ///     Type of exception.
        /// </value>
        public Type TypeOfException { get; set; }

        #endregion
    }
}