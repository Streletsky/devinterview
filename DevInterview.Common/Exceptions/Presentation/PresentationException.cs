﻿using System;
using System.Runtime.Serialization;

namespace DevInterview.Common.Exceptions.Presentation
{
    /// <summary>
    ///     Exception which can occur in Presentation layer of application.
    /// </summary>
    [Serializable]
    public class PresentationException : Exception, ISerializable
    {
        #region Constructors and Destructors

        public PresentationException() { }

        public PresentationException(string message) : base(message) { }

        public PresentationException(string message, Exception inner) : base(message, inner) { }

        public PresentationException(ExceptionMessage message, Exception inner) : base(ExceptionHelper.GetErrorMessage(message), inner) { }

        protected PresentationException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        #endregion
    }
}