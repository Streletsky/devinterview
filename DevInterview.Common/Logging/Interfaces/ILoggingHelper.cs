﻿using System;

namespace DevInterview.Common.Logging.Interfaces
{
    /// <summary>
    ///     Represents interface for interaction with third party logging tools.
    /// </summary>
    public interface ILoggingHelper
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Writes the diagnostic message at the Debug level.
        /// </summary>
        /// <param name="message">Log message.</param>
        void Debug(string message);

        /// <summary>
        ///     Writes the diagnostic message and exception at the Debug level.
        /// </summary>
        /// <param name="ex">An exception to be logged.</param>
        /// <param name="message">Log message.</param>
        void Debug(Exception ex, string message);

        /// <summary>
        ///     Writes the exception at the Debug level.
        /// </summary>
        /// <param name="ex">An exception to be logged.</param>
        void Debug(Exception ex);

        /// <summary>
        ///     Writes the diagnostic message at the Error level.
        /// </summary>
        /// <param name="message">Log message.</param>
        void Error(string message);

        /// <summary>
        ///     Writes the diagnostic message and exception at the Error level.
        /// </summary>
        /// <param name="ex">An exception to be logged.</param>
        /// <param name="message">Log message.</param>
        void Error(Exception ex, string message);

        /// <summary>
        ///     Writes the exception at the Error level.
        /// </summary>
        /// <param name="ex">An exception to be logged.</param>
        void Error(Exception ex);

        /// <summary>
        ///     Writes the diagnostic message at the Fatal level.
        /// </summary>
        /// <param name="message">Log message.</param>
        void Fatal(string message);

        /// <summary>
        ///     Writes the diagnostic message and exception at the Fatal level.
        /// </summary>
        /// <param name="ex">An exception to be logged.</param>
        /// <param name="message">Log message.</param>
        void Fatal(Exception ex, string message);

        /// <summary>
        ///     Writes the exception at the Fatal level.
        /// </summary>
        /// <param name="ex">An exception to be logged.</param>
        void Fatal(Exception ex);

        /// <summary>
        ///     Writes the diagnostic message at the Info level.
        /// </summary>
        /// <param name="message">Log message.</param>
        void Info(string message);

        /// <summary>
        ///     Writes the diagnostic message and exception at the Info level.
        /// </summary>
        /// <param name="ex">An exception to be logged.</param>
        /// <param name="message">Log message.</param>
        void Info(Exception ex, string message);

        /// <summary>
        ///     Writes the exception at the Info level.
        /// </summary>
        /// <param name="ex">An exception to be logged.</param>
        void Info(Exception ex);

        /// <summary>
        ///     Writes the diagnostic message at the Warn level.
        /// </summary>
        /// <param name="message">Log message.</param>
        void Warn(string message);

        /// <summary>
        ///     Writes the diagnostic message and exception at the Warn level.
        /// </summary>
        /// <param name="ex">An exception to be logged.</param>
        /// <param name="message">Log message.</param>
        void Warn(Exception ex, string message);

        /// <summary>
        ///     Writes the exception at the Warn level.
        /// </summary>
        /// <param name="ex">An exception to be logged.</param>
        void Warn(Exception ex);

        #endregion
    }
}