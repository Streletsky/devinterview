﻿using System;
using DevInterview.Common.Logging.Interfaces;

namespace DevInterview.Common.Logging
{
    /// <summary>
    ///     Logging helper based on NLog logging tool.
    /// </summary>
    public class NLogLoggingHelper : ILoggingHelper
    {
        #region Public Methods and Operators

        public void Debug(string message)
        {
            NLogSingleton.Instance.Debug(message);
        }

        public void Debug(Exception ex, string message)
        {
            NLogSingleton.Instance.Debug(ex, message);
        }

        public void Debug(Exception ex)
        {
            NLogSingleton.Instance.Debug(ex);
        }

        public void Error(string message)
        {
            NLogSingleton.Instance.Error(message);
        }

        public void Error(Exception ex, string message)
        {
            NLogSingleton.Instance.Error(ex, message);
        }

        public void Error(Exception ex)
        {
            NLogSingleton.Instance.Error(ex);
        }

        public void Fatal(string message)
        {
            NLogSingleton.Instance.Fatal(message);
        }

        public void Fatal(Exception ex, string message)
        {
            NLogSingleton.Instance.Fatal(ex, message);
        }

        public void Fatal(Exception ex)
        {
            NLogSingleton.Instance.Fatal(ex);
        }

        public void Info(string message)
        {
            NLogSingleton.Instance.Info(message);
        }

        public void Info(Exception ex, string message)
        {
            NLogSingleton.Instance.Info(ex, message);
        }

        public void Info(Exception ex)
        {
            NLogSingleton.Instance.Info(ex);
        }

        public void Warn(string message)
        {
            NLogSingleton.Instance.Warn(message);
        }

        public void Warn(Exception ex, string message)
        {
            NLogSingleton.Instance.Warn(ex, message);
        }

        public void Warn(Exception ex)
        {
            NLogSingleton.Instance.Warn(ex);
        }

        #endregion
    }
}