﻿using NLog;

namespace DevInterview.Common.Logging
{
    /// <summary>
    ///     Helper class, which wraps NLog <see cref="Logger" />.
    /// </summary>
    public class NLogSingleton
    {
        #region Static Fields

        private static readonly object locker = new object();

        private static volatile Logger instance;

        #endregion


        #region Constructors and Destructors

        private NLogSingleton() { }

        #endregion


        #region Public Properties

        /// <summary>
        ///     Current instance of NLog <see cref="NLog.Logger" /> class.
        /// </summary>
        public static Logger Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (locker)
                    {
                        if (instance == null)
                        {
                            instance = LogManager.GetCurrentClassLogger();
                        }
                    }
                }

                return instance;
            }
        }

        #endregion
    }
}