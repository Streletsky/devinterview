﻿namespace DevInterview.Common.Utilities.Extensions
{
    /// <summary>
    ///     Extensions for String class.
    /// </summary>
    public static class StringExtensions
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Check if string is empty.
        /// </summary>
        /// <param name="str">
        ///     String to check.
        /// </param>
        /// <returns>
        ///     <c>true</c> if string is empty.
        /// </returns>
        public static bool IsEmpty(this string str) => str == string.Empty;

        /// <summary>
        ///     Check if string is null.
        /// </summary>
        /// <param name="str">
        ///     String to check.
        /// </param>
        /// <returns>
        ///     <c>true</c> if string is null.
        /// </returns>
        public static bool IsNull(this string str) => str == null;

        /// <summary>
        ///     Check if string is empty or null.
        /// </summary>
        /// <param name="str">
        ///     String to check.
        /// </param>
        /// <returns>
        ///     <c>true</c> if string is empty or null.
        /// </returns>
        public static bool IsNullOrEmpty(this string str) => string.IsNullOrEmpty(str);

        #endregion
    }
}