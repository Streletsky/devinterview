﻿using System;
using System.Linq;
using System.Linq.Expressions;
using NUnit.Framework;

namespace DevInterview.Common.Utilities.Extensions.UnitTesting
{
    /// <summary>
    ///     Extension for unit testing purposes, which checks whether method has given attribute or no.
    /// </summary>
    public static class AttributeCheckerExtension
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Check if method of class has given attributes.
        /// </summary>
        /// <typeparam name="T">
        ///     Type of object.
        /// </typeparam>
        /// <typeparam name="TT">
        ///     Type of returned object.
        /// </typeparam>
        /// <param name="obj">
        ///     Object to check.
        /// </param>
        /// <param name="exp">
        ///     Expression which points method to check.
        /// </param>
        /// <param name="attributes">
        ///     List of expected attribute types.
        /// </param>
        public static void ShouldHave<T, TT>(this T obj, Expression<Func<T, TT>> exp, params Type[] attributes)
        {
            var memberExpression = exp.Body as MethodCallExpression;

            foreach (Type attribute in attributes)
            {
                Assert.IsTrue(memberExpression != null && memberExpression.Method.GetCustomAttributes(attribute, false).Any(), attribute.Name + " not found");
            }
        }

        #endregion
    }
}