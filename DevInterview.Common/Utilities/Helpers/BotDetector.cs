﻿using System.Text.RegularExpressions;

namespace DevInterview.Common.Utilities.Helpers
{
    public class BotDetector : IBotDetector
    {
        #region Public Methods and Operators

        public bool CheckUserAgentForBot(string ua) => Regex.IsMatch(ua,
                                                                     @"bot|crawler|baiduspider|80legs|ia_archiver|voyager|curl|wget|yahoo! slurp|mediapartners-google|bingpreview",
                                                                     RegexOptions.IgnoreCase);

        #endregion
    }
}