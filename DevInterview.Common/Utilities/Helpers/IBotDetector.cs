﻿namespace DevInterview.Common.Utilities.Helpers
{
    public interface IBotDetector
    {
        #region Public Methods and Operators

        bool CheckUserAgentForBot(string ua);

        #endregion
    }
}