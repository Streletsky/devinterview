﻿namespace DevInterview.Common.Utilities.Helpers
{
    public interface IUrlizer
    {
        #region Public Methods and Operators

        string GetFriendlyUrl(string str);

        #endregion
    }
}