﻿using System.Text.RegularExpressions;

namespace DevInterview.Common.Utilities.Helpers
{
    public class Urlizer : IUrlizer
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Gets friendly URL from any string.
        /// </summary>
        /// <param name="str">Raw string.</param>
        /// <returns>Friendly URL.</returns>
        public string GetFriendlyUrl(string str)
        {
            if (string.IsNullOrEmpty(str))
            {
                return null;
            }

            // make it all lower case
            str = str.ToLower();

            // remove entities
            str = Regex.Replace(str, @"&\w+;", "");

            // remove anything that is not letters, numbers, dash, or space
            str = Regex.Replace(str, @"[^a-z0-9\-\s]", "");

            // replace spaces
            str = str.Replace(' ', '-');

            // collapse dashes
            str = Regex.Replace(str, @"-{2,}", "-");

            // trim excessive dashes at the beginning
            str = str.TrimStart('-');

            // remove trailing dashes
            str = str.TrimEnd('-');

            return str;
        }

        #endregion
    }
}