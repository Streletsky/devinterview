﻿using DevInterview.Dal.Entities.Base;

namespace DevInterview.Dal.Entities.Business.Base
{
    /// <summary>
    ///     Serves as base class for entities, which have own URL in application and title.
    /// </summary>
    public abstract class WebEntry : BaseEntity
    {
        #region Public Properties

        /// <summary>
        ///     Title of entity.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        ///     String, which will be used as descriptive part of URL of entity.
        /// </summary>
        public string UrlSlug { get; set; }

        #endregion
    }
}