﻿using DevInterview.Dal.Entities.Business.Base;

namespace DevInterview.Dal.Entities.Business
{
    /// <summary>
    ///     Represents category of a web entry.
    /// </summary>
    public class Category : WebEntry
    {
        #region Public Properties

        /// <summary>
        ///     Description of category.
        /// </summary>
        public string Description { get; set; }

        #endregion
    }
}