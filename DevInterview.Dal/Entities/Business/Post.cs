﻿using System;
using System.Collections.Generic;
using DevInterview.Dal.Entities.Business.Base;
using DevInterview.Dal.Entities.Identity;

namespace DevInterview.Dal.Entities.Business
{
    /// <summary>
    ///     Represents web entry.
    /// </summary>
    public class Post : WebEntry
    {
        #region Constructors and Destructors

        public Post()
        {
            Tags = new List<Tag>();
        }

        #endregion


        #region Public Properties

        /// <summary>
        ///     Part of post entry, which will be visible only on full post page.
        /// </summary>
        public string BodyEnd { get; set; }

        /// <summary>
        ///     Part of post entry, which will be shown as short description.
        /// </summary>
        public string BodyStart { get; set; }

        /// <summary>
        ///     <see cref="Category" /> of post entry.
        /// </summary>
        public virtual Category Category { get; set; }

        /// <summary>
        ///     Date of creation of post entry.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        ///     Author of post entry.
        /// </summary>
        public virtual AppUser CreatedBy { get; set; }

        /// <summary>
        ///     Description, which will be used as HTML meta description (SEO important).
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        ///     Flag, which indicates whether post entry marked as deleted.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        ///     Flag, which indicates whether post should be updated with links to other posts.
        /// </summary>
        public bool IsLinkingRequired { get; set; }

        /// <summary>
        ///     Flag, which indicates whether post entry marked as visible.
        /// </summary>
        public bool IsVisible { get; set; }

        /// <summary>
        ///     Date of last update of post entry.
        /// </summary>
        public DateTime LastUpdated { get; set; }

        /// <summary>
        ///     User, who modified post entry last.
        /// </summary>
        public virtual AppUser LastUpdatedBy { get; set; }

        /// <summary>
        ///     <see cref="Rating" /> of post entry.
        /// </summary>
        public virtual Rating Rating { get; set; }

        /// <summary>
        ///     Collection of <see cref="Tag" />, which were assigned to post entry.
        /// </summary>
        public virtual ICollection<Tag> Tags { get; set; }

        /// <summary>
        ///     Views counter of post entry.
        /// </summary>
        public int Views { get; set; }

        #endregion
    }
}