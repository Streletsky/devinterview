﻿using System.ComponentModel.DataAnnotations.Schema;
using DevInterview.Dal.Entities.Base;

namespace DevInterview.Dal.Entities.Business
{
    /// <summary>
    ///     Rating of web entry.
    /// </summary>
    public class Rating : BaseEntity
    {
        #region Public Properties

        /// <summary>
        ///     Difference of positive and negative votes.
        /// </summary>
        [NotMapped]
        public int VotesDifference => VotesUp - VotesDown;

        /// <summary>
        ///     Negative votes for entry.
        /// </summary>
        public int VotesDown { get; set; }

        /// <summary>
        ///     Positive votes for entry.
        /// </summary>
        public int VotesUp { get; set; }

        #endregion
    }
}