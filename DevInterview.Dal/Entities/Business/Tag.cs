﻿using System.Collections.Generic;
using DevInterview.Dal.Entities.Business.Base;

namespace DevInterview.Dal.Entities.Business
{
    /// <summary>
    ///     Tag of web entry.
    /// </summary>
    public class Tag : WebEntry
    {
        #region Public Properties

        /// <summary>
        ///     Collection of <see cref="Post" />, which are tagged with tag.
        /// </summary>
        public ICollection<Post> Posts { get; set; }

        #endregion
    }
}