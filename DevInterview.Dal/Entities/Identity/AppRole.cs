﻿using DevInterview.Dal.Entities.Base;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DevInterview.Dal.Entities.Identity
{
    /// <summary>
    ///     User role.
    /// </summary>
    public class AppRole : IdentityRole<int, AppUserRole>, IEntity<int> { }
}