﻿using System;
using DevInterview.Dal.Entities.Base;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DevInterview.Dal.Entities.Identity
{
    /// <summary>
    ///     User of application.
    /// </summary>
    public class AppUser : IdentityUser<int, AppUserLogin, AppUserRole, AppUserClaim>, IEntity<int>
    {
        #region Public Properties

        /// <summary>
        ///     Date of creation of user.
        /// </summary>
        public DateTime Created { get; set; }

        /// <summary>
        ///     Flag, which indicates whether user marked as active.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        ///     Flag, which indicates whether user marked as deleted.
        /// </summary>
        public bool IsDeleted { get; set; }

        #endregion
    }
}