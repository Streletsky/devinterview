﻿using DevInterview.Dal.Entities.Base;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DevInterview.Dal.Entities.Identity
{
    /// <summary>
    ///     User claim.
    /// </summary>
    public class AppUserClaim : IdentityUserClaim<int>, IEntity<int> { }
}