﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace DevInterview.Dal.Entities.Identity
{
    /// <summary>
    ///     User login.
    /// </summary>
    public class AppUserLogin : IdentityUserLogin<int> { }
}