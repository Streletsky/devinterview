﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace DevInterview.Dal.Entities.Identity
{
    /// <summary>
    ///     User to Role relationship.
    /// </summary>
    public class AppUserRole : IdentityUserRole<int> { }
}