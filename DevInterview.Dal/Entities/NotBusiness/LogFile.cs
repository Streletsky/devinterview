﻿using System;

namespace DevInterview.Dal.Entities.NotBusiness
{
    public class LogFile
    {
        #region Public Properties

        public string FileName { get; set; }

        public DateTime LastModifiedOn { get; set; }

        #endregion
    }
}