﻿namespace DevInterview.Dal.Entities.NotBusiness
{
    /// <summary>
    ///     Parameters for web entry search.
    /// </summary>
    public class SearchParameters
    {
        #region Public Properties

        /// <summary>
        ///     Count of entries to return.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        ///     Query string to search in entry.
        /// </summary>
        public string CustomSearchQuery { get; set; }

        /// <summary>
        ///     Is search should be performed with contains operation rather than equals.
        /// </summary>
        /// <value>
        ///     <c>true</c> if search is implicit; otherwise, <c>false</c>.
        /// </value>
        public bool IsImplicitSearch { get; set; }

        /// <summary>
        ///     Name of property (column) to order returned entries by.
        /// </summary>
        public string OrderBy { get; set; }

        /// <summary>
        ///     Order direction ("asc" or "desc").
        /// </summary>
        public string OrderDirection { get; set; }

        /// <summary>
        ///     Number of page in entries result set.
        /// </summary>
        public int Page { get; set; }

        /// <summary>
        ///     Name of property (column) to search entries by.
        /// </summary>
        public string SearchBy { get; set; }

        /// <summary>
        ///     Query string to search in entry.
        /// </summary>
        public string SearchQuery { get; set; }

        #endregion
    }
}