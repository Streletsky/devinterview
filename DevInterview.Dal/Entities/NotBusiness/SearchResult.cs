﻿using System.Collections.Generic;

namespace DevInterview.Dal.Entities.NotBusiness
{
    /// <summary>
    ///     Wrapper for results of search query.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SearchResult<T>
    {
        #region Public Properties

        /// <summary>
        ///     Actual result of search.
        /// </summary>
        public IEnumerable<T> Data { get; set; }

        /// <summary>
        ///     Records, returned as result of current search.
        /// </summary>
        public int Records { get; set; }

        /// <summary>
        ///     Total records of entries in storage.
        /// </summary>
        public int TotalRecords { get; set; }

        #endregion
    }
}