﻿using System.Data.Entity;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DevInterview.Dal.OrmInfrastructure
{
    /// <summary>
    ///     Entity Framework database context for application.
    /// </summary>
    public class AppContext : IdentityDbContext<AppUser, AppRole, int, AppUserLogin, AppUserRole, AppUserClaim>
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Instantiates AppContext object.
        /// </summary>
        public AppContext()
        {
            RequireUniqueEmail = true;
            Configuration.LazyLoadingEnabled = true;
        }

        #endregion


        #region Public Properties

        /// <summary>
        ///     All <see cref="Category" /> entries in storage.
        /// </summary>
        public DbSet<Category> Categories { get; set; }

        /// <summary>
        ///     All <see cref="Post" /> entries in storage.
        /// </summary>
        public DbSet<Post> Posts { get; set; }

        /// <summary>
        ///     All <see cref="Rating" /> entries in storage.
        /// </summary>
        public DbSet<Rating> Ratings { get; set; }

        /// <summary>
        ///     All <see cref="Tag" /> entries in storage.
        /// </summary>
        public DbSet<Tag> Tags { get; set; }

        #endregion
    }
}