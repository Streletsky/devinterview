﻿using System.Data.Entity.Migrations;

namespace DevInterview.Dal.OrmInfrastructure
{
    /// <summary>
    ///     Configures EF behavior for DB schema update.
    /// </summary>
    public sealed class MigrationConfiguration : DbMigrationsConfiguration<AppContext>
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Instantiates MigrationConfiguration object.
        /// </summary>
        public MigrationConfiguration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = false;
            ContextKey = "DevInterview.Dal.OrmInfrastructure.AppContext";
        }

        #endregion


        #region Methods

        protected override void Seed(AppContext context)
        {
            //context.Categories.AddOrUpdate(c => c.Id, OrmInfrastructure.Seed.Instance.Categories.ToArray());
            //context.SaveChanges();

            //context.Roles.AddOrUpdate(p => p.Name, OrmInfrastructure.Seed.Instance.Roles.ToArray());
            //context.SaveChanges();
        }

        #endregion
    }
}