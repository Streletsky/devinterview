﻿using System;
using System.Collections.Generic;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.Identity;

namespace DevInterview.Dal.OrmInfrastructure
{
    /// <summary>
    ///     Class, which is used to populate storage with initial data.
    /// </summary>
    public class Seed
    {
        #region Static Fields

        private static Seed instance;

        #endregion


        #region Fields

        /// <summary>
        ///     <see cref="Category" /> seed entries.
        /// </summary>
        public IEnumerable<Category> Categories;

        /// <summary>
        ///     <see cref="Post" /> seed entries.
        /// </summary>
        public IEnumerable<Post> Posts;

        /// <summary>
        ///     <see cref="Rating" /> seed entries.
        /// </summary>
        public IEnumerable<Rating> Ratings;

        /// <summary>
        ///     <see cref="AppRole" /> seed entries.
        /// </summary>
        public IEnumerable<AppRole> Roles;

        /// <summary>
        ///     <see cref="Tag" /> seed entries.
        /// </summary>
        public IEnumerable<Tag> Tags;

        #endregion


        #region Constructors and Destructors

        private Seed()
        {
            Categories = new List<Category>
            {
                new Category
                {
                    Id = 1,
                    Description = "",
                    Title = ".NET",
                    UrlSlug = "dotnet"
                },
                new Category
                {
                    Id = 2,
                    Description = "",
                    Title = "ASP.NET WebForms",
                    UrlSlug = "aspnet-webforms"
                },
                new Category
                {
                    Id = 3,
                    Description = "",
                    Title = "ASP.NET MVC",
                    UrlSlug = "aspnet-mvc"
                },
                new Category
                {
                    Id = 4,
                    Description = "",
                    Title = "C#",
                    UrlSlug = "csharp"
                },
                new Category
                {
                    Id = 5,
                    Description = "",
                    Title = "HTML/CSS",
                    UrlSlug = "html-css"
                },
                new Category
                {
                    Id = 6,
                    Description = "",
                    Title = "JavaScript",
                    UrlSlug = "javascript"
                },
                new Category
                {
                    Id = 7,
                    Description = "",
                    Title = "SQL and Databases",
                    UrlSlug = "sql-databases"
                },
                new Category
                {
                    Id = 8,
                    Description = "",
                    Title = "OOP and OOD",
                    UrlSlug = "oop-ood"
                },
                new Category
                {
                    Id = 9,
                    Description = "",
                    Title = "Algorithms 2",
                    UrlSlug = "algorithms"
                },
                new Category
                {
                    Id = 10,
                    Description = "",
                    Title = "Software Development",
                    UrlSlug = "development"
                }
            };

            Posts = new List<Post>
            {
                new Post
                {
                    Id = 1,
                    BodyStart =
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non nisl ligula. Mauris interdum luctus commodo. Aliquam egestas iaculis tincidunt. Proin tristique tellus enim, ac pretium velit aliquet sed. Vivamus sed velit velit. Etiam hendrerit molestie massa, sit amet consectetur neque pharetra at. Nullam consectetur magna sit amet nulla pulvinar, in sagittis quam tristique. Nunc eleifend faucibus nunc sit amet dignissim. Fusce felis sem, ultricies ac felis et, faucibus adipiscing odio. Cras porta massa et tortor pharetra aliquam. Sed eu tincidunt massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur congue tincidunt suscipit. Proin venenatis suscipit lacus, posuere porta nulla accumsan sed. Nulla faucibus tincidunt est, non malesuada sem posuere et. In in dui sem.",
                    BodyEnd =
                        " Fusce felis sem, ultricies ac felis et, faucibus adipiscing odio. Cras porta massa et tortor pharetra aliquam. Sed eu tincidunt massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur congue tincidunt suscipit. Proin venenatis suscipit lacus, posuere porta nulla accumsan sed. Nulla faucibus tincidunt est, non malesuada sem posuere et. In in dui sem. ",
                    IsDeleted = false,
                    IsVisible = true,
                    LastUpdated = DateTime.Now,
                    Title = "Visual Studio 2013 Update 4",
                    UrlSlug = "Visual Studio 2013 Update 4",
                    Views = 562,
                    Created = DateTime.Now
                },
                new Post
                {
                    Id = 2,
                    BodyStart =
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non nisl ligula. Mauris interdum luctus commodo. Aliquam egestas iaculis tincidunt. Proin tristique tellus enim, ac pretium velit aliquet sed. Vivamus sed velit velit. Etiam hendrerit molestie massa, sit amet consectetur neque pharetra at. Nullam consectetur magna sit amet nulla pulvinar, in sagittis quam tristique. Nunc eleifend faucibus nunc sit amet dignissim. Fusce felis sem, ultricies ac felis et, faucibus adipiscing odio. Cras porta massa et tortor pharetra aliquam. Sed eu tincidunt massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur congue tincidunt suscipit. Proin venenatis suscipit lacus, posuere porta nulla accumsan sed. Nulla faucibus tincidunt est, non malesuada sem posuere et. In in dui sem.",
                    BodyEnd =
                        " Fusce felis sem, ultricies ac felis et, faucibus adipiscing odio. Cras porta massa et tortor pharetra aliquam. Sed eu tincidunt massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur congue tincidunt suscipit. Proin venenatis suscipit lacus, posuere porta nulla accumsan sed. Nulla faucibus tincidunt est, non malesuada sem posuere et. In in dui sem. ",
                    IsDeleted = false,
                    IsVisible = true,
                    LastUpdated = DateTime.Now,
                    Title = "Visual Studio 2013 Update 4",
                    UrlSlug = "Visual Studio 2013 Update 4",
                    Views = 562,
                    Created = DateTime.Now
                },
                new Post
                {
                    Id = 3,
                    BodyStart =
                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non nisl ligula. Mauris interdum luctus commodo. Aliquam egestas iaculis tincidunt. Proin tristique tellus enim, ac pretium velit aliquet sed. Vivamus sed velit velit. Etiam hendrerit molestie massa, sit amet consectetur neque pharetra at. Nullam consectetur magna sit amet nulla pulvinar, in sagittis quam tristique. Nunc eleifend faucibus nunc sit amet dignissim. Fusce felis sem, ultricies ac felis et, faucibus adipiscing odio. Cras porta massa et tortor pharetra aliquam. Sed eu tincidunt massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur congue tincidunt suscipit. Proin venenatis suscipit lacus, posuere porta nulla accumsan sed. Nulla faucibus tincidunt est, non malesuada sem posuere et. In in dui sem.",
                    BodyEnd =
                        " Fusce felis sem, ultricies ac felis et, faucibus adipiscing odio. Cras porta massa et tortor pharetra aliquam. Sed eu tincidunt massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Curabitur congue tincidunt suscipit. Proin venenatis suscipit lacus, posuere porta nulla accumsan sed. Nulla faucibus tincidunt est, non malesuada sem posuere et. In in dui sem. ",
                    IsDeleted = false,
                    IsVisible = true,
                    LastUpdated = DateTime.Now,
                    Title = "Visual Studio 2013 Update 4",
                    UrlSlug = "Visual Studio 2013 Update 4",
                    Views = 562,
                    Created = DateTime.Now
                }
            };

            Tags = new List<Tag>
            {
                new Tag
                {
                    Id = 1,
                    Title = "Agile",
                    UrlSlug = "agile"
                },
                new Tag
                {
                    Id = 2,
                    Title = "Scrum",
                    UrlSlug = "scrum"
                }
            };

            Ratings = new List<Rating>
            {
                new Rating
                {
                    Id = 1,
                    VotesDown = 8,
                    VotesUp = 56
                }
            };

            Roles = new List<AppRole>
            {
                new AppRole
                {
                    Name = Common.Enums.Roles.Administrator.ToString()
                },
                new AppRole
                {
                    Name = Common.Enums.Roles.Publisher.ToString()
                },
                new AppRole
                {
                    Name = Common.Enums.Roles.Common.ToString()
                }
            };
        }

        #endregion


        #region Public Properties

        /// <summary>
        ///     Instance of Seed singleton.
        /// </summary>
        public static Seed Instance => instance ?? (instance = new Seed());

        #endregion
    }
}