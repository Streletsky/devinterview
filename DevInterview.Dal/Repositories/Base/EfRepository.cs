﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using DevInterview.Common.Utilities.Extensions;
using DevInterview.Dal.Entities.Base;
using DevInterview.Dal.Entities.NotBusiness;

namespace DevInterview.Dal.Repositories.Base
{
    /// <summary>
    ///     Serves as base class for EF based repositories with CRUD operations implemented.
    /// </summary>
    /// <typeparam name="TEntity">
    ///     Type of entity repository will work with.
    /// </typeparam>
    /// <typeparam name="TId">
    ///     Type of ID of <typeparamref name="TEntity" />.
    /// </typeparam>
    public abstract class EfRepository<TEntity, TId> : IRepository<TEntity, TId> where TEntity : class, IEntity<TId>
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Instantiates EfRepository object.
        /// </summary>
        /// <param name="context">
        ///     <see cref="DbContext" /> which will be used for interaction with storage.
        /// </param>
        protected EfRepository(DbContext context)
        {
            Context = context;
            EntitiesSet = Context.Set<TEntity>();
        }

        #endregion


        #region Properties

        /// <summary>
        ///     Current <see cref="DbContext" /> instance.
        /// </summary>
        protected DbContext Context { get; set; }

        /// <summary>
        ///     Current <see cref="DbSet" /> of entities.
        /// </summary>
        protected DbSet<TEntity> EntitiesSet { get; set; }

        #endregion


        #region Public Methods and Operators

        public virtual void Delete(TId id)
        {
            TEntity entity = Get(id);

            Delete(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            if (Context.Entry(entity).State == EntityState.Detached)
            {
                EntitiesSet.Attach(entity);
            }

            EntitiesSet.Remove(entity);

            Context.SaveChanges();
        }

        public virtual TEntity Get(TId id) => EntitiesSet.Find(id);

        public virtual SearchResult<TEntity> GetAll()
        {
            List<TEntity> result = EntitiesSet.ToList();
            int count = result.Count();

            return new SearchResult<TEntity>
            {
                Data = result,
                TotalRecords = count,
                Records = count
            };
        }

        public virtual TEntity Insert(TEntity entity)
        {
            EntitiesSet.Add(entity);

            Context.SaveChanges();

            return entity;
        }

        public virtual SearchResult<TEntity> Search(SearchParameters param)
        {
            IQueryable<TEntity> dataFiltered = param.SearchBy.IsNullOrEmpty() && param.CustomSearchQuery.IsNullOrEmpty()
                                                   ? EntitiesSet
                                                   : !param.CustomSearchQuery.IsNullOrEmpty()
                                                       ? EntitiesSet.Where(param.CustomSearchQuery)
                                                       : param.IsImplicitSearch
                                                           ? EntitiesSet.Where(param.SearchBy + ".Contains(@0)", param.SearchQuery)
                                                           : EntitiesSet.Where(param.SearchBy + " = @0", param.SearchQuery);

            IQueryable<TEntity> dataPage = GetDataPage(dataFiltered, param);

            return new SearchResult<TEntity>
            {
                Data = dataPage,
                TotalRecords = dataFiltered.Count(),
                Records = dataPage.Count()
            };
        }

        public virtual TEntity Update(TEntity entity)
        {
            if (Context.Entry(entity).State == EntityState.Detached)
            {
                EntitiesSet.Attach(entity);
            }

            Context.Entry(entity).State = EntityState.Modified;

            Context.SaveChanges();

            return entity;
        }

        #endregion


        #region Methods

        protected IQueryable<TEntity> GetDataPage(IQueryable<TEntity> data, SearchParameters param)
        {
            IQueryable<TEntity> dataPage;
            if (!string.IsNullOrEmpty(param.OrderBy) && !string.IsNullOrEmpty(param.OrderDirection))
            {
                dataPage = param.OrderDirection.ToLowerInvariant() == "desc" ? data.OrderBy(param.OrderBy + " descending") : data.OrderBy(param.OrderBy);
            }
            else
            {
                dataPage = data.OrderBy(entity => entity.Id);
            }

            return dataPage.Skip(param.Count * (param.Page - 1)).Take(param.Count);
        }

        #endregion
    }
}