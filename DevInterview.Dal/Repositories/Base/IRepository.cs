﻿using DevInterview.Dal.Entities.Base;
using DevInterview.Dal.Entities.NotBusiness;

namespace DevInterview.Dal.Repositories.Base
{
    /// <summary>
    ///     Represents interface to interact with data storage.
    /// </summary>
    /// <typeparam name="TEntity">
    ///     Type of entity repository will work with.
    /// </typeparam>
    /// <typeparam name="TId">
    ///     Type of ID of <typeparamref name="TEntity" />.
    /// </typeparam>
    public interface IRepository<TEntity, in TId> where TEntity : IEntity<TId>
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Delete <typeparamref name="TEntity" /> entity from storage by ID.
        /// </summary>
        /// <param name="id">
        ///     ID of <typeparamref name="TEntity" /> entity.
        /// </param>
        /// <returns>
        ///     <c>true</c> if <typeparamref name="TEntity" /> entity was deleted successfully.
        /// </returns>
        void Delete(TId id);

        /// <summary>
        ///     Delete given <typeparamref name="TEntity" /> entity from storage.
        /// </summary>
        /// <param name="entity">
        ///     <typeparamref name="TEntity" /> entity to delete.
        /// </param>
        /// <returns>
        ///     <c>true</c> if <typeparamref name="TEntity" /> entity was deleted successfully.
        /// </returns>
        void Delete(TEntity entity);

        /// <summary>
        ///     Get <typeparamref name="TEntity" /> entity from storage by ID.
        /// </summary>
        /// <param name="id">
        ///     ID of <typeparamref name="TEntity" />.
        /// </param>
        /// <returns>
        ///     Found <typeparamref name="TEntity" /> entity.
        /// </returns>
        TEntity Get(TId id);

        /// <summary>
        ///     Get all <typeparamref name="TEntity" /> entities from storage.
        /// </summary>
        /// <returns>
        ///     All <typeparamref name="TEntity" /> entities.
        /// </returns>
        SearchResult<TEntity> GetAll();

        /// <summary>
        ///     Create <typeparamref name="TEntity" /> entity in storage.
        /// </summary>
        /// <param name="entity">
        ///     <typeparamref name="TEntity" /> entity to create.
        /// </param>
        /// <returns>
        ///     <c>true</c> if <typeparamref name="TEntity" /> entity was inserted successfully.
        /// </returns>
        TEntity Insert(TEntity entity);

        /// <summary>
        ///     Search <typeparamref name="TEntity" /> entity in storage by provided parameters.
        /// </summary>
        /// <param name="param">
        ///     Parameters for search.
        /// </param>
        /// <returns>
        ///     Found <typeparamref name="TEntity" /> entities wrapped in <see cref="SearchResult{T}" /> class.
        /// </returns>
        SearchResult<TEntity> Search(SearchParameters param);

        /// <summary>
        ///     Update given <typeparamref name="TEntity" /> entity in storage.
        /// </summary>
        /// <param name="entity">
        ///     <typeparamref name="TEntity" /> entity to update.
        /// </param>
        /// <returns>
        ///     <c>true</c> if <typeparamref name="TEntity" /> entity was updated successfully.
        /// </returns>
        TEntity Update(TEntity entity);

        #endregion
    }
}