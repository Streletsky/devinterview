﻿using System.Data.Entity;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Repositories.Base;
using DevInterview.Dal.Repositories.Business.Interfaces;

namespace DevInterview.Dal.Repositories.Business
{
    /// <summary>
    ///     Provides access to Category entries in storage.
    /// </summary>
    public class CategoryRepository : EfRepository<Category, int>, ICategoryRepository
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Instantiates CategoryRepository object.
        /// </summary>
        /// <param name="context">
        ///     <see cref="DbContext" /> which will be used for interaction with storage.
        /// </param>
        public CategoryRepository(DbContext context) : base(context) { }

        #endregion
    }
}