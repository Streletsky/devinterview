﻿using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Repositories.Base;

namespace DevInterview.Dal.Repositories.Business.Interfaces
{
    /// <summary>
    ///     Represents interface for interaction with <see cref="Category" /> entries storage.
    /// </summary>
    public interface ICategoryRepository : IRepository<Category, int> { }
}