﻿using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Dal.Repositories.Base;

namespace DevInterview.Dal.Repositories.Business.Interfaces
{
    /// <summary>
    ///     Represents interface for interaction with <see cref="Post" /> entries storage.
    /// </summary>
    // TODO: move Count methods to separate repository
    public interface IPostRepository : IRepository<Post, int>
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Gets <see cref="Post" /> entries by given params.
        /// </summary>
        /// <returns></returns>
        SearchResult<Post> GetPostsByCategory(SearchParameters param);

        /// <summary>
        ///     Gets <see cref="Post" /> entries by given params.
        /// </summary>
        /// <returns></returns>
        SearchResult<Post> GetPostsByTag(SearchParameters param);

        /// <summary>
        ///     Get count of <see cref="Post" />, which have <see cref="Category" /> with given ID.
        /// </summary>
        /// <param name="id">
        ///     ID of <see cref="Category" />.
        /// </param>
        /// <returns>
        ///     Counter of <see cref="Post" />.
        /// </returns>
        int GetPostsCountByCategory(int id);

        /// <summary>
        ///     Get count of <see cref="Post" />, which have <see cref="Tag" /> with given ID.
        /// </summary>
        /// <param name="id">
        ///     ID of <see cref="Tag" />.
        /// </param>
        /// <returns>
        ///     Counter of <see cref="Post" />.
        /// </returns>
        int GetTaggedPostsCountByTag(int id);

        #endregion
    }
}