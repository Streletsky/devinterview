﻿using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Dal.Repositories.Base;

namespace DevInterview.Dal.Repositories.Business.Interfaces
{
    /// <summary>
    ///     Manages <see cref="Tag" /> entries in storage.
    /// </summary>
    public interface ITagRepository : IRepository<Tag, int>
    {
        #region Public Methods and Operators

        SearchResult<Tag> GetOnlyTagsInUse();

        #endregion
    }
}