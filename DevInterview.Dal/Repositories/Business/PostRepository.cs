﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Linq;
using DevInterview.Common.Exceptions;
using DevInterview.Common.Exceptions.Dal;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Dal.Repositories.Base;
using DevInterview.Dal.Repositories.Business.Interfaces;

namespace DevInterview.Dal.Repositories.Business
{
    /// <summary>
    ///     Provides access to Post entries in storage.
    /// </summary>
    public class PostRepository : EfRepository<Post, int>, IPostRepository
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Instantiates PostRepository object.
        /// </summary>
        /// <param name="context">
        ///     <see cref="DbContext" /> which will be used for interaction with storage.
        /// </param>
        public PostRepository(DbContext context) : base(context) { }

        #endregion


        #region Public Methods and Operators

        public SearchResult<Post> GetPostsByCategory(SearchParameters param) => Search(param);

        public SearchResult<Post> GetPostsByTag(SearchParameters param)
        {
            IQueryable<Post> dataFiltered = EntitiesSet.Where(p => p.Tags.Any(t => t.Id.ToString() == param.SearchQuery));

            IQueryable<Post> dataPage = GetDataPage(dataFiltered, param);

            return new SearchResult<Post>
            {
                Data = dataPage,
                TotalRecords = dataFiltered.Count(),
                Records = dataPage.Count()
            };
        }

        public int GetPostsCountByCategory(int id)
        {
            return EntitiesSet.Count(p => p.Category.Id == id);
        }

        public int GetTaggedPostsCountByTag(int id)
        {
            return EntitiesSet.Count(p => p.Tags.Any(t => t.Id == id));
        }

        public override Post Insert(Post entity)
        {
            entity.Created = DateTime.Now;

            PreparePostForSaving(entity);

            return base.Insert(entity);
        }

        public override Post Update(Post entity)
        {
            PreparePostForSaving(entity);

            return base.Update(entity);
        }

        #endregion


        #region Methods

        /// <summary>
        ///     Prepares the <see cref="Post" /> for further saving: gets and sets tags and category of <see cref="Post" /> from
        ///     storage.
        /// </summary>
        /// <param name="entity">The <see cref="Post" /> entry.</param>
        private void PreparePostForSaving(Post entity)
        {
            entity.LastUpdated = DateTime.Now;

            List<Tag> tags = entity.Tags.ToList();
            entity.Tags.Clear();

            foreach (Tag tag in tags)
            {
                Tag existingTag = Context.Set<Tag>().FirstOrDefault(t => t.Title == tag.Title);
                if (existingTag != null)
                {
                    Context.Entry(existingTag).State = EntityState.Modified;
                    entity.Tags.Add(existingTag);
                }
                else if (tag.Id != 0)
                {
                    existingTag = Context.Set<Tag>().FirstOrDefault(t => t.Id == tag.Id);
                    if (existingTag == null)
                    {
                        tag.Id = 0;
                        Context.Entry(tag).State = EntityState.Added;
                        entity.Tags.Add(tag);
                    }
                    else
                    {
                        Context.Entry(existingTag).State = EntityState.Modified;
                        entity.Tags.Add(existingTag);
                    }
                }
                else
                {
                    Context.Entry(tag).State = EntityState.Added;
                    entity.Tags.Add(tag);
                }
            }

            Category category = Context.Set<Category>().FirstOrDefault(c => c.Id == entity.Category.Id);
            if (category == null)
            {
                var inner = new ObjectNotFoundException($"Category with ID {entity.Category.Id} was not found in storage.");
                throw new DataAccessException(
                                              new ExceptionMessage
                                              {
                                                  FailedOperationDescription = "Can not update entity of type Post in storage.",
                                                  TypeOfException = inner.GetType(),
                                                  PossibleReason = ExceptionHelper.ReferenceToInnerException
                                              },
                                              inner);
            }

            entity.Category = category;
        }

        #endregion
    }
}