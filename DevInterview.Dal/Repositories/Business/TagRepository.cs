﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Dal.Repositories.Base;
using DevInterview.Dal.Repositories.Business.Interfaces;

namespace DevInterview.Dal.Repositories.Business
{
    /// Manages
    /// <see cref="Tag" />
    /// entities in storage.
    public class TagRepository : EfRepository<Tag, int>, ITagRepository
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Instantiates TagRepository object.
        /// </summary>
        /// <param name="context">
        ///     <see cref="DbContext" /> which will be used for interaction with storage.
        /// </param>
        public TagRepository(DbContext context) : base(context) { }

        #endregion


        #region Public Methods and Operators

        public SearchResult<Tag> GetOnlyTagsInUse()
        {
            List<Tag> result = EntitiesSet.Where(t => t.Posts.Count > 0).ToList();
            int count = result.Count();

            return new SearchResult<Tag>
            {
                Data = result,
                TotalRecords = count,
                Records = count
            };
        }

        #endregion
    }
}