﻿using DevInterview.Dal.Entities.Identity;
using Microsoft.AspNet.Identity;

namespace DevInterview.Dal.Repositories.Identity.Interfaces
{
    /// <summary>
    ///     Extends ASP.NET Identity interface for interaction with <see cref="AppRole" /> entities in storage.
    /// </summary>
    public interface IRoleRepositoryAsync : IRoleStore<AppRole, int>, IQueryableRoleStore<AppRole, int> { }
}