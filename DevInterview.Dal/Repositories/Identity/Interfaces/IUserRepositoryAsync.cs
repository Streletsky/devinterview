﻿using System.Threading.Tasks;
using DevInterview.Dal.Entities.Identity;
using DevInterview.Dal.Entities.NotBusiness;
using Microsoft.AspNet.Identity;

namespace DevInterview.Dal.Repositories.Identity.Interfaces
{
    /// <summary>
    ///     Extends ASP.NET Identity interface for interaction with <see cref="AppUser" /> entities in storage.
    /// </summary>
    public interface IUserRepositoryAsync : IUserStore<AppUser, int>,
                                            IQueryableUserStore<AppUser, int>,
                                            IUserClaimStore<AppUser, int>,
                                            IUserEmailStore<AppUser, int>,
                                            IUserLockoutStore<AppUser, int>,
                                            IUserLoginStore<AppUser, int>,
                                            IUserPasswordStore<AppUser, int>,
                                            IUserPhoneNumberStore<AppUser, int>,
                                            IUserRoleStore<AppUser, int>,
                                            IUserSecurityStampStore<AppUser, int>,
                                            IUserTwoFactorStore<AppUser, int>
    {
        #region Public Methods and Operators

        /// <summary>
        ///     Gets all <see cref="AppUser" /> entities in storage.
        /// </summary>
        /// <returns>
        ///     <see cref="SearchResult" /> with results data set.
        /// </returns>
        Task<SearchResult<AppUser>> GetAllAsync();

        /// <summary>
        ///     Search <see cref="AppUser" /> entities in storage by provided parameters.
        /// </summary>
        /// <param name="param">
        ///     Parameters for search.
        /// </param>
        /// <returns>
        ///     Found <see cref="AppUser" /> entities wrapped in <see cref="SearchResult{T}" /> class.
        /// </returns>
        Task<SearchResult<AppUser>> SearchAsync(SearchParameters param);

        #endregion
    }
}