﻿using System.Data.Entity;
using DevInterview.Dal.Entities.Identity;
using DevInterview.Dal.Repositories.Identity.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DevInterview.Dal.Repositories.Identity
{
    /// <summary>
    ///     Role store implementation based on ASP.NET Identity implementation.
    /// </summary>
    public class RoleRepositoryAsync : RoleStore<AppRole, int, AppUserRole>, IRoleRepositoryAsync
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Instantiates RoleRepositoryAsync object.
        /// </summary>
        /// <param name="context">
        ///     <see cref="DbContext" /> which will be used for interaction with storage.
        /// </param>
        public RoleRepositoryAsync(DbContext context) : base(context) { }

        #endregion
    }
}