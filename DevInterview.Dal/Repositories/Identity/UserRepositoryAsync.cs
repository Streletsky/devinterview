﻿using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using DevInterview.Dal.Entities.Identity;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Dal.Repositories.Identity.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DevInterview.Dal.Repositories.Identity
{
    /// <summary>
    ///     <see cref="AppUser" /> repository based on ASP.NET Identity implementation.
    /// </summary>
    public class UserRepositoryAsync : UserStore<AppUser, AppRole, int, AppUserLogin, AppUserRole, AppUserClaim>, IUserRepositoryAsync
    {
        #region Constructors and Destructors

        /// <summary>
        ///     Instantiates UserRepositoryAsync object.
        /// </summary>
        /// <param name="context">
        ///     <see cref="DbContext" /> which will be used for interaction with storage.
        /// </param>
        public UserRepositoryAsync(DbContext context) : base(context)
        {
            AutoSaveChanges = true;
        }

        #endregion


        #region Public Methods and Operators

        public async Task<SearchResult<AppUser>> GetAllAsync()
        {
            int count = await Users.CountAsync();

            return await Task.FromResult(new SearchResult<AppUser>
            {
                Data = await Users.ToListAsync(),
                TotalRecords = count,
                Records = count
            });
        }

        public async Task<SearchResult<AppUser>> SearchAsync(SearchParameters param)
        {
            IQueryable<AppUser> dataFiltered = string.IsNullOrEmpty(param.SearchBy) ? Users : Users.Where(param.SearchBy + " = @0", param.SearchQuery);

            IQueryable<AppUser> dataPage = GetDataPage(dataFiltered, param);

            return await Task.FromResult(new SearchResult<AppUser>
            {
                Data = dataPage,
                TotalRecords = dataFiltered.Count(),
                Records = dataPage.Count()
            });
        }

        #endregion


        #region Methods

        private IQueryable<AppUser> GetDataPage(IQueryable<AppUser> data, SearchParameters param)
        {
            IQueryable<AppUser> dataPage;
            if (!string.IsNullOrEmpty(param.OrderBy) && !string.IsNullOrEmpty(param.OrderDirection))
            {
                dataPage = param.OrderDirection.ToLowerInvariant() == "desc" ? data.OrderBy(param.OrderBy + " descending") : data.OrderBy(param.OrderBy);
            }
            else
            {
                dataPage = data.OrderBy(entity => entity.Id);
            }

            return dataPage.Skip(param.Count * (param.Page - 1)).Take(param.Count);
        }

        #endregion
    }
}