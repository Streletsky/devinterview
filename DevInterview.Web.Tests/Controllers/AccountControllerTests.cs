﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using DevInterview.Bal.Managers.Identity;
using DevInterview.Bal.Tests;
using DevInterview.Bal.Tests.Mocks;
using DevInterview.Common.Utilities.Extensions.UnitTesting;
using DevInterview.Dal.Entities.Identity;
using DevInterview.Web.Controllers;
using DevInterview.Web.Models;
using FluentAssertions;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Moq;
using NUnit.Framework;

namespace DevInterview.Web.Tests.Controllers
{
    [TestFixture]
    public class AccountControllerTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            ModelBindingManager.Init();

            userStoreMock = new UserRepositoryMock(FakeDataProvider.PopulateUserStore());
            userManagerMock = new Mock<AppUserManager>(userStoreMock.Instance.Object);
            authManagerMock = new Mock<IAuthenticationManager>();
            signInManagerMock = new Mock<AppSignInManager>(userManagerMock.Object, authManagerMock.Object);

            signInManagerMock.Setup(m => m.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()))
                             .Returns(Task.FromResult(SignInStatus.Success));

            authManagerMock.Setup(m => m.SignOut());

            userManagerMock.Setup(m => m.CreateAsync(It.IsAny<AppUser>(), It.IsAny<string>())).Returns(Task.FromResult(IdentityResult.Success));

            target = new AccountController(userManagerMock.Object, signInManagerMock.Object);
            target.SetFakeControllerContext();
        }

        #endregion


        #region All other members

        private Mock<IAuthenticationManager> authManagerMock;

        private Mock<AppSignInManager> signInManagerMock;

        private AccountController target;

        private Mock<AppUserManager> userManagerMock;

        private UserRepositoryMock userStoreMock;

        #endregion


        #region Test Methods

        [Test]
        public void Login_ShouldHaveAttributes()
        {
            const string url = "/login";

            target.ShouldHave(t => t.Login(url), typeof(AllowAnonymousAttribute));
        }

        [Test]
        public void Login_ShouldReturnView()
        {
            const string url = "/login";

            ActionResult result = target.Login(url);

            result.Should().NotBeNull();
            result.GetType().ToString().Should().Be(typeof(ViewResult).ToString());
        }

        [Test]
        public void Login_WithUrlParam_ShouldSetUrlInViewBag()
        {
            const string url = "/login";

            target.Login(url);

            url.Should().Be(target.ViewBag.ReturnUrl);
        }

        [Test]
        public void LoginPost_ShouldHaveAttributes()
        {
            const string url = "/login";

            target.ShouldHave(t => t.Login(new LoginModel(), url), typeof(HttpPostAttribute));
            target.ShouldHave(t => t.Login(new LoginModel(), url), typeof(AllowAnonymousAttribute));
            target.ShouldHave(t => t.Login(new LoginModel(), url), typeof(ValidateAntiForgeryTokenAttribute));
        }

        [Test]
        public async Task LoginPost_WithInvalidModel_ShouldReturnLoginView()
        {
            var model = new LoginModel();
            target.ModelState.AddModelError("", "");

            ActionResult result = await target.Login(model, "");

            signInManagerMock.Verify(m => m.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()), Times.Never);
            result.Should().NotBeNull();
            result.GetType().ToString().Should().Be(typeof(ViewResult).ToString());
        }

        [Test]
        public async Task LoginPost_WithNotSuccessfulLoginAttempt_ShouldReturnLoginView()
        {
            var model = new LoginModel();
            signInManagerMock.Setup(m => m.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()))
                             .Returns(Task.FromResult(SignInStatus.Failure));

            ActionResult result = await target.Login(model, "");

            signInManagerMock.Verify(m => m.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()), Times.Once);
            result.Should().NotBeNull();
            result.GetType().ToString().Should().Be(typeof(ViewResult).ToString());
        }

        [Test]
        public void LoginPost_WithNullModel_ShouldThrowException()
        {
            Assert.Throws<AggregateException>(() => target.Login(null, "").Wait());
        }

        [Test]
        public async Task LoginPost_WithSuccessfulLoginAttemptAndExternalUrl_ShouldRedirectToHome()
        {
            var model = new LoginModel();

            ActionResult result = await target.Login(model, "http://reddit.com/");

            signInManagerMock.Verify(m => m.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()), Times.Once);
            result.Should().NotBeNull();
            result.GetType().Should().Be(typeof(RedirectToRouteResult));
        }

        [Test]
        public async Task LoginPost_WithSuccessfulLoginAttemptAndLocalUrl_ShouldRedirectToUrl()
        {
            var model = new LoginModel();

            ActionResult result = await target.Login(model, "/login");

            signInManagerMock.Verify(m => m.PasswordSignInAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<bool>()), Times.Once);
            result.Should().NotBeNull();
            result.GetType().ToString().Should().Be(typeof(RedirectResult).ToString());
        }

        [Test]
        public void LogOff_ShouldSignOutAndRedirect()
        {
            ActionResult result = target.LogOff();

            authManagerMock.Verify(m => m.SignOut(), Times.Once());
            result.Should().NotBeNull();
            result.GetType().ToString().Should().Be(typeof(RedirectToRouteResult).ToString());
        }

        [Test]
        public void Register_ShouldHaveNonActionAttribute()
        {
            target.ShouldHave(t => t.Register(), typeof(NonActionAttribute));
        }

        [Test]
        public void Register_ShouldReturnView()
        {
            ActionResult result = target.Register();

            result.Should().NotBeNull();
            result.GetType().ToString().Should().Be(typeof(ViewResult).ToString());
        }

        [Test]
        public async Task RegisterPost_WithInvalidModel_ShouldReturnRegisterView()
        {
            var model = new RegisterModel();
            target.ModelState.AddModelError("", "");

            ActionResult result = await target.Register(model);

            userManagerMock.Verify(m => m.CreateAsync(It.IsAny<AppUser>(), It.IsAny<string>()), Times.Never);
            userManagerMock.Verify(m => m.AddToRoleAsync(It.IsAny<int>(), It.IsAny<string>()), Times.Never);
            signInManagerMock.Verify(m => m.SignInAsync(It.IsAny<AppUser>(), It.IsAny<bool>(), It.IsAny<bool>()), Times.Never);
            result.Should().NotBeNull();
            result.GetType().ToString().Should().Be(typeof(ViewResult).ToString());
        }

        [Test]
        public void RegisterPost_WithNullModel_ShouldThrowException()
        {
            Assert.Throws<AggregateException>(() => target.Register(null).Wait());
        }

        [Test]
        public async Task RegisterPost_WithValidModelAndNotSuccessfulResult_ShouldReturnRegisterView()
        {
            userManagerMock.Setup(m => m.CreateAsync(It.IsAny<AppUser>(), It.IsAny<string>())).Returns(Task.FromResult(IdentityResult.Failed()));
            var model = new RegisterModel();

            ActionResult result = await target.Register(model);

            userManagerMock.Verify(m => m.CreateAsync(It.IsAny<AppUser>(), It.IsAny<string>()), Times.Once);
            userManagerMock.Verify(m => m.AddToRoleAsync(It.IsAny<int>(), It.IsAny<string>()), Times.Never);
            signInManagerMock.Verify(m => m.SignInAsync(It.IsAny<AppUser>(), It.IsAny<bool>(), It.IsAny<bool>()), Times.Never);
            result.Should().NotBeNull();
            result.GetType().ToString().Should().Be(typeof(ViewResult).ToString());
        }

        [Test]
        public async Task RegisterPost_WithValidModelAndSuccessfulResult_ShouldCreateAddToRoleSignInAndRedirect()
        {
            var model = new RegisterModel();

            ActionResult result = await target.Register(model);

            userManagerMock.Verify(m => m.CreateAsync(It.IsAny<AppUser>(), It.IsAny<string>()), Times.Once);
            userManagerMock.Verify(m => m.AddToRoleAsync(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
            signInManagerMock.Verify(m => m.SignInAsync(It.IsAny<AppUser>(), It.IsAny<bool>(), It.IsAny<bool>()), Times.Once);
            result.Should().NotBeNull();
            result.GetType().ToString().Should().Be(typeof(RedirectToRouteResult).ToString());
        }

        #endregion
    }
}