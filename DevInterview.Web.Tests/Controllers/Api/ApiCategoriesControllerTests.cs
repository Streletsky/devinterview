﻿using System.Linq;
using DevInterview.Bal.Managers.Business;
using DevInterview.Bal.Managers.Caching.Interfaces;
using DevInterview.Bal.Tests;
using DevInterview.Bal.Tests.Mocks;
using DevInterview.Common.Exceptions.Presentation;
using DevInterview.Common.Utilities.Helpers;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Web.Areas.ControlPanel.Controllers.WebApi;
using DevInterview.Web.Models;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace DevInterview.Web.Tests.Controllers.Api
{
    [TestFixture]
    public class ApiCategoriesControllerTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            ModelBindingManager.Init();

            catRepositoryMock = new CategoryRepositoryMock(FakeDataProvider.PopulateCategoryStore());
            cacheManagerMock = new Mock<ICacheManager>();
            urlizerMock = new Mock<IUrlizer>();

            catManagerMock = new Mock<CategoryManager>(catRepositoryMock.Instance.Object, cacheManagerMock.Object);
            catManagerMock.Setup(m => m.Delete(It.IsAny<int>()));
            catManagerMock.Setup(m => m.Get(It.IsAny<int>())).Returns<int>(id => catRepositoryMock.Store.Single(c => c.Id == id));
            catManagerMock.Setup(m => m.GetAll())
                          .Returns(new SearchResult<Category>
                          {
                              Data = catRepositoryMock.Store,
                              Records = catRepositoryMock.Store.Count,
                              TotalRecords = catRepositoryMock.Store.Count
                          });
            catManagerMock.Setup(m => m.Create(It.IsAny<Category>()))
                          .Returns<Category>(cat =>
                          {
                              cat.Id = catRepositoryMock.Store.Max(c => c.Id) + 1;
                              return cat;
                          })
                          .Callback<Category>(cat => catRepositoryMock.Store.Add(cat));

            urlizerMock.Setup(m => m.GetFriendlyUrl(It.IsAny<string>())).Returns<string>(str => str);

            target = new CategoriesController(catManagerMock.Object, urlizerMock.Object);
        }

        #endregion


        #region All other members

        private Mock<ICacheManager> cacheManagerMock;

        private Mock<CategoryManager> catManagerMock;

        private Mock<IUrlizer> urlizerMock;

        private CategoryRepositoryMock catRepositoryMock;

        private CategoriesController target;

        #endregion


        #region Test Methods

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public void Delete_WithInvalidId_ShouldThrowException(int id)
        {
            Assert.Throws<PresentationException>(() => target.Delete(id));
        }

        [Test]
        public void Delete_WithValidId_ShouldDeleteCategoryFromStorage()
        {
            const int id = 1;

            target.Delete(id);

            catManagerMock.Verify(m => m.Delete(id), Times.Once);
        }

        [Test]
        public void Get_WithoutResultsInStore_ShouldReturnEmptyResult()
        {
            catManagerMock.Setup(m => m.GetAll()).Returns(new SearchResult<Category>());

            SearchResult<CategoryModel> result = target.Get();

            catManagerMock.Verify(m => m.GetAll(), Times.Once());
            result.Should().NotBeNull();
            result.Data.Should().BeNull();
            result.Records.Should().Be(0);
            result.TotalRecords.Should().Be(0);
        }

        [Test]
        public void Get_WithResultsInStore_ShouldMapToWebModel()
        {
            SearchResult<CategoryModel> result = target.Get();
            CategoryModel model = result.Data.First();
            Category category = catRepositoryMock.Store.First();

            catManagerMock.Verify(m => m.GetAll(), Times.Once());
            model.Id.Should().Be(category.Id);
            model.Description.Should().Be(category.Description);
            model.UrlSlug.Should().Be(category.UrlSlug);
            model.Title.Should().Be(category.Title);
        }

        [Test]
        public void Get_WithResultsInStore_ShouldReturnResult()
        {
            SearchResult<CategoryModel> result = target.Get();

            catManagerMock.Verify(m => m.GetAll(), Times.Once());
            result.Should().NotBeNull();
            result.Data.Should().NotBeNull();
            result.Records.Should().Be(catRepositoryMock.Store.Count);
            result.TotalRecords.Should().Be(catRepositoryMock.Store.Count);
        }

        [Test]
        public void Post_WithEmptyUrlSlug_ShouldGenerateUrlSlug()
        {
            var cat = new CategoryModel
            {
                Description = "desc",
                Title = "ASP.NET MVC"
            };

            CategoryModel result = target.Post(cat);
            Category catInStorage = catRepositoryMock.Store.Single(u => u.Id == result.Id);

            catInStorage.UrlSlug.Should().NotBeNullOrEmpty();
            urlizerMock.Verify(m => m.GetFriendlyUrl(It.Is<string>(s => s == cat.Title)), Times.Once);
        }

        [Test]
        public void Post_WithNotEmptyUrlSlug_ShouldNotGenerateUrlSlug()
        {
            const string url = "aspnet";
            var cat = new CategoryModel
            {
                Description = "desc",
                Title = "ASP.NET MVC",
                UrlSlug = url
            };

            CategoryModel result = target.Post(cat);
            Category catInStorage = catRepositoryMock.Store.Single(u => u.Id == result.Id);

            catInStorage.UrlSlug.Should().Be(url);
            urlizerMock.Verify(m => m.GetFriendlyUrl(It.Is<string>(s => s == cat.Title)), Times.Never);
        }

        [Test]
        public void Post_WithNullModel_ShouldThrowException()
        {
            Assert.Throws<PresentationException>(() => target.Post(null));
        }

        [Test]
        public void Post_WithValidModel_ShouldCreateCategoryInStorage()
        {
            var cat = new CategoryModel
            {
                Description = "desc",
                Title = "title",
                UrlSlug = "/fdg"
            };

            CategoryModel result = target.Post(cat);
            Category catInStorage = catRepositoryMock.Store.Single(u => u.Id == result.Id);

            catManagerMock.Verify(m => m.Create(It.IsAny<Category>()), Times.Once());
            result.Should().NotBeNull();
            result.Should().BeSameAs(cat);
            catInStorage.Should().NotBeNull();
        }

        [Test]
        public void Post_WithValidModel_ShouldMapToWebModel()
        {
            var cat = new CategoryModel
            {
                Description = "desc",
                Title = "title",
                UrlSlug = "/fdg"
            };

            CategoryModel result = target.Post(cat);
            Category catInStorage = catRepositoryMock.Store.Single(u => u.Id == result.Id);

            catInStorage.Id.Should().Be(result.Id);
            catInStorage.Description.Should().Be(result.Description);
            catInStorage.Title.Should().Be(result.Title);
        }

        [Test]
        public void Put_WithCategoryFoundInStorage_ShouldMapToWebModel()
        {
            var cat = new CategoryModel
            {
                Id = 1,
                Description = "desc",
                Title = "title",
                UrlSlug = "/fdg"
            };

            CategoryModel result = target.Put(cat);
            Category catInStorage = catRepositoryMock.Store.Single(u => u.Id == result.Id);

            catInStorage.Id.Should().Be(result.Id);
            catInStorage.Description.Should().Be(result.Description);
            catInStorage.Title.Should().Be(result.Title);
        }

        [Test]
        public void Put_WithCategoryFoundInStorage_ShouldUpdateCategory()
        {
            var cat = new CategoryModel
            {
                Id = 1,
                Description = "desc",
                Title = "title",
                UrlSlug = "/fdg"
            };

            CategoryModel result = target.Put(cat);
            Category catInStorage = catRepositoryMock.Store.Single(u => u.Id == result.Id);

            catManagerMock.Verify(m => m.Update(It.IsAny<Category>()), Times.Once());
            catManagerMock.Verify(m => m.Get(cat.Id), Times.Once());
            result.Should().NotBeNull();
            result.Should().BeSameAs(cat);
            catInStorage.Should().NotBeNull();
        }

        [Test]
        public void Put_WithCategoryNotFoundInStorage_ShouldThrowException()
        {
            catManagerMock.Setup(m => m.Get(It.IsAny<int>())).Returns((Category)null);

            Assert.Throws<PresentationException>(() => target.Put(new CategoryModel
            {
                Id = 100
            }));
        }

        [Test]
        public void Put_WithEmptyUrlSlug_ShouldGenerateUrlSlug()
        {
            var cat = new CategoryModel
            {
                Id = 1,
                Description = "desc",
                Title = "ASP.NET MVC"
            };

            CategoryModel result = target.Put(cat);
            Category catInStorage = catRepositoryMock.Store.Single(u => u.Id == result.Id);

            catInStorage.UrlSlug.Should().NotBeNullOrEmpty();
            urlizerMock.Verify(m => m.GetFriendlyUrl(It.Is<string>(s => s == cat.Title)), Times.Once);
        }

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public void Put_WithInvalidModelId_ShouldThrowException(int id)
        {
            Assert.Throws<PresentationException>(() => target.Put(new CategoryModel
            {
                Id = id
            }));
        }

        [Test]
        public void Put_WithNotEmptyUrlSlug_ShouldNotGenerateUrlSlug()
        {
            const string url = "aspnet";
            var cat = new CategoryModel
            {
                Id = 1,
                Description = "desc",
                Title = "ASP.NET MVC",
                UrlSlug = url
            };

            CategoryModel result = target.Put(cat);
            Category catInStorage = catRepositoryMock.Store.Single(u => u.Id == result.Id);

            catInStorage.UrlSlug.Should().Be(url);
            urlizerMock.Verify(m => m.GetFriendlyUrl(It.Is<string>(s => s == cat.Title)), Times.Never);
        }

        [Test]
        public void Put_WithNullModel_ShouldThrowException()
        {
            Assert.Throws<PresentationException>(() => target.Put(null));
        }

        #endregion
    }
}