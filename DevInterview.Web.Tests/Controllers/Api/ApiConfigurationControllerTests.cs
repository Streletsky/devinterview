﻿using System.Collections.Generic;
using DevInterview.Common.Configuration.Interfaces;
using DevInterview.Common.Exceptions.Presentation;
using DevInterview.Web.Areas.ControlPanel.Controllers.WebApi;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace DevInterview.Web.Tests.Controllers.Api
{
    [TestFixture]
    public class ApiConfigurationControllerTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            ModelBindingManager.Init();

            webConfigManagerMock = new Mock<IWebConfigManager>();
            webConfigManagerMock.Setup(m => m.GetAllAppSettings()).Returns(new List<KeyValuePair<string, string>>());
            webConfigManagerMock.Setup(m => m.SaveAppSettingEntry(It.IsAny<string>(), It.IsAny<string>()));

            target = new ConfigurationController(webConfigManagerMock.Object);
        }

        #endregion


        #region All other members

        private Mock<IWebConfigManager> webConfigManagerMock;

        private ConfigurationController target;

        #endregion


        #region Test Methods

        [Test]
        public void Get_ShouldInvokeManager()
        {
            target.Get();

            webConfigManagerMock.Verify(m => m.GetAllAppSettings(), Times.Once);
        }

        [Test]
        public void Get_WithData_ShouldReturnList()
        {
            List<KeyValuePair<string, string>> result = target.Get();

            result.Should().NotBeNull();
        }

        [Test]
        public void Get_WithotData_ShouldReturnNull()
        {
            webConfigManagerMock.Setup(m => m.GetAllAppSettings()).Returns((List<KeyValuePair<string, string>>)null);

            List<KeyValuePair<string, string>> result = target.Get();

            result.Should().BeNull();
        }

        [Test]
        public void Save_WithCfgParam_ShouldInvokeManager()
        {
            var cfg = new KeyValuePair<string, string>("key", "value");

            target.Post(cfg);

            webConfigManagerMock.Verify(m => m.SaveAppSettingEntry(It.Is<string>(k => k == cfg.Key), It.Is<string>(v => v == cfg.Value)), Times.Once);
        }

        [Test]
        [TestCase(null)]
        public void Save_WithoutCfgParam_ShouldThrowException(KeyValuePair<string, string> cfg)
        {
            Assert.Throws<PresentationException>(() => target.Post(cfg));
        }

        #endregion
    }
}