﻿using System.Collections.Generic;
using DevInterview.Bal.Managers.Logging.Interfaces;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Web.Areas.ControlPanel.Controllers.WebApi;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace DevInterview.Web.Tests.Controllers.Api
{
    [TestFixture]
    public class ApiLogsControllerTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            ModelBindingManager.Init();

            logFilesManagerMock = new Mock<ILogFilesManager>();
            logFilesManagerMock.Setup(m => m.GetAll()).Returns(new List<LogFile>());
            logFilesManagerMock.Setup(m => m.Delete(It.IsAny<string>()));

            target = new LogsController(logFilesManagerMock.Object);
        }

        #endregion


        #region All other members

        private Mock<ILogFilesManager> logFilesManagerMock;

        private LogsController target;

        #endregion


        #region Test Methods

        [Test]
        [TestCase(null)]
        [TestCase("")]
        [TestCase("file.js")]
        public void Delete_WithAnyValue_ShouldInvokeManager(string fileName)
        {
            target.Delete(fileName);

            logFilesManagerMock.Verify(m => m.Delete(It.Is<string>(s => s == fileName)), Times.Once);
        }

        [Test]
        public void Get_WithFilesInSystem_ShouldReturnFilesList()
        {
            IEnumerable<LogFile> result = target.Get();

            result.Should().NotBeNull();
            logFilesManagerMock.Verify(m => m.GetAll(), Times.Once);
        }

        [Test]
        public void Get_WithoutFilesInSystem_ShouldReturnNull()
        {
            logFilesManagerMock.Setup(m => m.GetAll()).Returns((List<LogFile>)null);

            IEnumerable<LogFile> result = target.Get();

            result.Should().BeNull();
            logFilesManagerMock.Verify(m => m.GetAll(), Times.Once);
        }

        #endregion
    }
}