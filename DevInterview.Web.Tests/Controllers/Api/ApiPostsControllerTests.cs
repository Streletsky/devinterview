﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevInterview.Bal.Managers.Business;
using DevInterview.Bal.Managers.Caching.Interfaces;
using DevInterview.Bal.SearchEngine.Base;
using DevInterview.Bal.Tests;
using DevInterview.Bal.Tests.Mocks;
using DevInterview.Common.Exceptions.Presentation;
using DevInterview.Common.Utilities.Helpers;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Web.Areas.ControlPanel.Controllers.WebApi;
using DevInterview.Web.Models;
using FluentAssertions;
using Lucene.Net.Documents;
using Moq;
using NUnit.Framework;

namespace DevInterview.Web.Tests.Controllers.Api
{
    [TestFixture]
    internal class ApiPostsControllerTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            ModelBindingManager.Init();

            postModel = new PostModel
            {
                Id = 1,
                Created = DateTime.Now,
                Title = "title",
                UrlSlug = "url",
                BodyEnd = "end",
                BodyStart = "start",
                Category = new CategoryModel(),
                IsDeleted = false,
                IsVisible = true,
                LastUpdated = DateTime.Now,
                Rating = new RatingModel(),
                Tags = new List<TagModel>(),
                Views = 100
            };

            postRepositoryMock = new PostRepositoryMock(FakeDataProvider.PopulatePostStore());
            cacheManagerMock = new Mock<ICacheManager>();

            searchEngineMock = new Mock<ILuceneSearchEngine>();
            searchEngineMock.Setup(m => m.Search(It.IsAny<string>(), It.IsAny<int>())).Returns(new List<Document>());
            searchEngineMock.Setup(m => m.Search(It.IsAny<Post>(), It.IsAny<int>())).Returns(new List<Document>());

            urlizerMock = new Mock<IUrlizer>();

            postManagerMock = new Mock<PostManager>(postRepositoryMock.Instance.Object, cacheManagerMock.Object, searchEngineMock.Object);
            postManagerMock.Setup(m => m.Delete(It.IsAny<int>()));
            postManagerMock.Setup(m => m.Get(It.IsAny<int>())).Returns<int>(id => postRepositoryMock.Store.Single(c => c.Id == id));
            postManagerMock.Setup(m => m.Search(It.IsAny<SearchParameters>()))
                           .Returns(new SearchResult<Post>
                           {
                               Data = postRepositoryMock.Store,
                               Records = postRepositoryMock.Store.Count,
                               TotalRecords = postRepositoryMock.Store.Count
                           });
            postManagerMock.Setup(m => m.Create(It.IsAny<Post>()))
                           .Returns<Post>(cat =>
                           {
                               cat.Id = postRepositoryMock.Store.Max(c => c.Id) + 1;
                               return cat;
                           })
                           .Callback<Post>(cat => postRepositoryMock.Store.Add(cat));

            urlizerMock.Setup(m => m.GetFriendlyUrl(It.IsAny<string>())).Returns<string>(str => str);

            target = new PostsController(postManagerMock.Object, urlizerMock.Object, searchEngineMock.Object);
        }

        #endregion


        #region All other members

        private Mock<ICacheManager> cacheManagerMock;

        private Mock<ILuceneSearchEngine> searchEngineMock;

        private Mock<PostManager> postManagerMock;

        private Mock<IUrlizer> urlizerMock;

        private PostModel postModel;

        private PostRepositoryMock postRepositoryMock;

        private PostsController target;

        #endregion


        #region Test Methods

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public void Delete_WithInvalidId_ShouldThrowException(int id)
        {
            Assert.Throws<PresentationException>(() => target.Delete(id));
        }

        [Test]
        public void Delete_WithValidId_ShouldDeletePostFromStorage()
        {
            const int id = 1;

            target.Delete(id);

            postManagerMock.Verify(m => m.Delete(id), Times.Once);
        }

        [Test]
        public void Get_WithoutResultsInStore_ShouldReturnEmptyResult()
        {
            postManagerMock.Setup(m => m.Search(It.IsAny<SearchParameters>())).Returns(new SearchResult<Post>());

            SearchResult<PostModel> result = target.Get(new SearchParameters());

            postManagerMock.Verify(m => m.Search(It.IsAny<SearchParameters>()), Times.Once());
            result.Should().NotBeNull();
            result.Data.Should().BeNull();
            result.Records.Should().Be(0);
            result.TotalRecords.Should().Be(0);
        }

        [Test]
        public void Get_WithResultsInStore_ShouldMapToWebModel()
        {
            SearchResult<PostModel> result = target.Get(new SearchParameters());
            PostModel model = result.Data.First();
            Post post = postRepositoryMock.Store.First();

            model.Id.Should().Be(post.Id);
            model.BodyEnd.Should().Be(post.BodyEnd);
            model.BodyStart.Should().Be(post.BodyStart);
            model.Category.Should().NotBeNull();
            model.Created.Should().Be(post.Created);
            model.IsDeleted.Should().Be(post.IsDeleted);
            model.IsVisible.Should().Be(post.IsVisible);
            model.LastUpdated.Should().Be(post.LastUpdated);
            model.Rating.Should().NotBeNull();
            model.Tags.Should().NotBeNull();
            model.Title.Should().Be(post.Title);
            model.UrlSlug.Should().Be(post.UrlSlug);
            model.Views.Should().Be(post.Views);
        }

        [Test]
        public void Get_WithResultsInStore_ShouldReturnResult()
        {
            SearchResult<PostModel> result = target.Get(new SearchParameters());

            postManagerMock.Verify(m => m.Search(It.IsAny<SearchParameters>()), Times.Once());
            result.Should().NotBeNull();
            result.Data.Should().NotBeNull();
            result.Records.Should().Be(postRepositoryMock.Store.Count);
            result.TotalRecords.Should().Be(postRepositoryMock.Store.Count);
        }

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public void GetById_WithInvalidId_ShouldThrowException(int id)
        {
            Assert.Throws<PresentationException>(() => target.Get(id));
        }

        [Test]
        public void GetById_WithPostFoundInStorage_ShouldMapToWebModel()
        {
            const int id = 1;

            PostModel result = target.Get(id);

            result.Id.Should().Be(postModel.Id);
            result.BodyEnd.Should().Be(postModel.BodyEnd);
            result.BodyStart.Should().Be(postModel.BodyStart);
            result.Category.Should().NotBeNull();
            result.Created.Should().Be(postModel.Created);
            result.IsDeleted.Should().Be(postModel.IsDeleted);
            result.IsVisible.Should().Be(postModel.IsVisible);
            result.LastUpdated.Should().Be(postModel.LastUpdated);
            result.Rating.Should().NotBeNull();
            result.Tags.Should().NotBeNull();
            result.Title.Should().Be(postModel.Title);
            result.UrlSlug.Should().Be(postModel.UrlSlug);
            result.Views.Should().Be(postModel.Views);
        }

        [Test]
        public void GetById_WithPostFoundInStorage_ShouldReturnPost()
        {
            const int id = 1;

            PostModel result = target.Get(id);

            postManagerMock.Verify(m => m.Get(id), Times.Once);
            result.Should().NotBeNull();
        }

        [Test]
        public void GetById_WithPostNotFoundInStorage_ShouldThrowException()
        {
            postManagerMock.Setup(m => m.Get(It.IsAny<int>())).Returns((Post)null);

            Assert.Throws<PresentationException>(() => target.Get(1));
        }

        [Test]
        public void Post_WithNullModel_ShouldThrowException()
        {
            Assert.Throws<PresentationException>(() => target.Post(null));
        }

        [Test]
        public void Post_WithValidModel_ShouldCreatePostInStorage()
        {
            PostModel result = target.Post(postModel);
            Post catInStorage = postRepositoryMock.Store.Single(u => u.Id == result.Id);

            postManagerMock.Verify(m => m.Create(It.IsAny<Post>()), Times.Once());
            result.Should().NotBeNull();
            result.Should().BeSameAs(postModel);
            catInStorage.Should().NotBeNull();
        }

        [Test]
        public void Post_WithValidModel_ShouldGenerateUrlSlug()
        {
            postModel.UrlSlug = null;

            PostModel post = target.Post(postModel);
            Post postInStorage = postRepositoryMock.Store.Single(u => u.Id == post.Id);

            postInStorage.UrlSlug.Should().NotBeNullOrEmpty();
            urlizerMock.Verify(m => m.GetFriendlyUrl(It.Is<string>(s => s == postModel.Title)), Times.Once);
        }

        [Test]
        public void Post_WithValidModel_ShouldGenerateUrlSlugForAllTags()
        {
            var times = 1; // post UrlSlug
            postModel.Tags = new List<TagModel>
            {
                new TagModel
                {
                    Id = 1,
                    Title = "title"
                },
                new TagModel
                {
                    Id = 2,
                    Title = "asp net"
                }
            };
            times += postModel.Tags.Count;

            PostModel post = target.Post(postModel);

            urlizerMock.Verify(m => m.GetFriendlyUrl(It.IsAny<string>()), Times.Exactly(times));
        }

        [Test]
        public void Post_WithValidModel_ShouldMapToWebModel()
        {
            PostModel post = target.Post(postModel);
            Post postInStorage = postRepositoryMock.Store.Single(u => u.Id == post.Id);

            post.Id.Should().Be(postInStorage.Id);
            post.BodyEnd.Should().Be(postInStorage.BodyEnd);
            post.BodyStart.Should().Be(postInStorage.BodyStart);
            post.Category.Should().NotBeNull();
            post.Created.Should().Be(postInStorage.Created);
            post.IsDeleted.Should().Be(postInStorage.IsDeleted);
            post.IsVisible.Should().Be(postInStorage.IsVisible);
            post.LastUpdated.Should().Be(postInStorage.LastUpdated);
            post.Rating.Should().NotBeNull();
            post.Tags.Should().NotBeNull();
            post.Title.Should().Be(postInStorage.Title);
            post.Views.Should().Be(postInStorage.Views);
        }

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public void Put_WithInvalidModelId_ShouldThrowException(int id)
        {
            Assert.Throws<PresentationException>(() => target.Put(new PostModel
            {
                Id = id
            }));
        }

        [Test]
        public void Put_WithNullModel_ShouldThrowException()
        {
            Assert.Throws<PresentationException>(() => target.Put(null));
        }

        [Test]
        public void Put_WithPostFoundInStorage_ShouldGenerateUrlSlug()
        {
            postModel.UrlSlug = null;

            PostModel post = target.Put(postModel);
            Post postInStorage = postRepositoryMock.Store.Single(u => u.Id == post.Id);

            postInStorage.UrlSlug.Should().NotBeNullOrEmpty();
            urlizerMock.Verify(m => m.GetFriendlyUrl(It.Is<string>(s => s == postModel.Title)), Times.Once);
        }

        [Test]
        public void Put_WithPostFoundInStorage_ShouldGenerateUrlSlugForAllTags()
        {
            var times = 1; // post UrlSlug
            postModel.Tags = new List<TagModel>
            {
                new TagModel
                {
                    Id = 1,
                    Title = "title"
                },
                new TagModel
                {
                    Id = 2,
                    Title = "asp net"
                }
            };
            times += postModel.Tags.Count;

            PostModel post = target.Put(postModel);

            urlizerMock.Verify(m => m.GetFriendlyUrl(It.IsAny<string>()), Times.Exactly(times));
        }

        [Test]
        public void Put_WithPostFoundInStorage_ShouldMapToWebModel()
        {
            PostModel post = postModel;

            PostModel result = target.Put(post);
            Post postInStorage = postRepositoryMock.Store.Single(u => u.Id == result.Id);

            post.Id.Should().Be(postInStorage.Id);
            post.BodyEnd.Should().Be(postInStorage.BodyEnd);
            post.BodyStart.Should().Be(postInStorage.BodyStart);
            post.Category.Should().NotBeNull();
            post.Created.Should().Be(postInStorage.Created);
            post.IsDeleted.Should().Be(postInStorage.IsDeleted);
            post.IsVisible.Should().Be(postInStorage.IsVisible);
            post.LastUpdated.Should().Be(postInStorage.LastUpdated);
            post.Rating.Should().NotBeNull();
            post.Tags.Should().NotBeNull();
            post.Title.Should().Be(postInStorage.Title);
            post.Views.Should().Be(postInStorage.Views);
        }

        [Test]
        public void Put_WithPostFoundInStorage_ShouldUpdatePost()
        {
            PostModel result = target.Put(postModel);
            Post postInStorage = postRepositoryMock.Store.Single(u => u.Id == result.Id);

            postManagerMock.Verify(m => m.Update(It.IsAny<Post>()), Times.Once());
            postManagerMock.Verify(m => m.Get(postModel.Id), Times.Once());
            result.Should().NotBeNull();
            result.Should().BeSameAs(postModel);
            postInStorage.Should().NotBeNull();
        }

        [Test]
        public void Put_WithPostNotFoundInStorage_ShouldThrowException()
        {
            postManagerMock.Setup(m => m.Get(It.IsAny<int>())).Returns((Post)null);

            Assert.Throws<PresentationException>(() => target.Put(new PostModel
            {
                Id = 100
            }));
        }

        #endregion
    }
}