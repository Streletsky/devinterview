﻿using System.Collections.Generic;
using System.Linq;
using DevInterview.Bal.Managers.Identity;
using DevInterview.Bal.Tests;
using DevInterview.Bal.Tests.Mocks;
using DevInterview.Dal.Entities.Identity;
using DevInterview.Web.Areas.ControlPanel.Controllers.WebApi;
using DevInterview.Web.Models;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace DevInterview.Web.Tests.Controllers.Api
{
    [TestFixture]
    public class ApiRolesControllerTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            ModelBindingManager.Init();

            roleStoreMock = new RoleRepositoryMock(FakeDataProvider.PopulateRoleStore());
            roleManagerMock = new Mock<AppRoleManager>(roleStoreMock.Instance.Object);
            roleManagerMock.SetupGet(r => r.Roles).Returns(new EnumerableQuery<AppRole>(roleStoreMock.Store));

            target = new RolesController(roleManagerMock.Object);
        }

        #endregion


        #region All other members

        private Mock<AppRoleManager> roleManagerMock;

        private RoleRepositoryMock roleStoreMock;

        private RolesController target;

        #endregion


        #region Test Methods

        [Test]
        public void Get_WithoutRolesInStorage_ShouldReturnEmptyResult()
        {
            roleManagerMock.SetupGet(r => r.Roles).Returns(new EnumerableQuery<AppRole>(new List<AppRole>()));
            IEnumerable<AppRoleModel> result = target.Get();

            roleManagerMock.VerifyGet(r => r.Roles, Times.Once);
            result.Should().NotBeNull();
            result.Count().Should().Be(0);
        }

        [Test]
        public void Get_WithRolesInStorage_ShouldMapToWebModel()
        {
            IEnumerable<AppRoleModel> result = target.Get();
            AppRoleModel model = result.First();
            AppRole role = roleStoreMock.Store.First();

            roleManagerMock.VerifyGet(r => r.Roles, Times.Once);
            model.Id.Should().Be(role.Id);
            model.Name.Should().Be(role.Name);
        }

        [Test]
        public void Get_WithRolesInStorage_ShouldReturnRoles()
        {
            IEnumerable<AppRoleModel> result = target.Get();

            roleManagerMock.VerifyGet(r => r.Roles, Times.Once);
            result.Should().NotBeNull();
            result.Count().Should().Be(roleStoreMock.Store.Count());
        }

        #endregion
    }
}