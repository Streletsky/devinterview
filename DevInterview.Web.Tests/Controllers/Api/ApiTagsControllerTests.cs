﻿using System.Linq;
using DevInterview.Bal.Managers.Business;
using DevInterview.Bal.Managers.Caching.Interfaces;
using DevInterview.Bal.Tests;
using DevInterview.Bal.Tests.Mocks;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Web.Areas.ControlPanel.Controllers.WebApi;
using DevInterview.Web.Models;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace DevInterview.Web.Tests.Controllers.Api
{
    [TestFixture]
    public class ApiTagsControllerTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            ModelBindingManager.Init();

            tagRepositoryMock = new TagRepositoryMock(FakeDataProvider.PopulateTagStore());

            cacheManagerMock = new Mock<ICacheManager>();

            tagManagerMock = new Mock<TagManager>(tagRepositoryMock.Instance.Object, cacheManagerMock.Object);
            tagManagerMock.Setup(m => m.Search(It.IsAny<SearchParameters>()))
                          .Returns(new SearchResult<Tag>
                          {
                              Data = tagRepositoryMock.Store,
                              Records = tagRepositoryMock.Store.Count,
                              TotalRecords = tagRepositoryMock.Store.Count
                          });

            target = new TagsController(tagManagerMock.Object);
        }

        #endregion


        #region All other members

        private Mock<ICacheManager> cacheManagerMock;

        private Mock<TagManager> tagManagerMock;

        private TagRepositoryMock tagRepositoryMock;

        private TagsController target;

        #endregion


        #region Test Methods

        [Test]
        public void Search_WithoutResultsInStore_ShouldReturnEmptyResult()
        {
            tagManagerMock.Setup(m => m.Search(It.IsAny<SearchParameters>())).Returns(new SearchResult<Tag>());

            SearchResult<TagModel> result = target.Search(new SearchParameters());

            tagManagerMock.Verify(m => m.Search(It.IsAny<SearchParameters>()), Times.Once());
            result.Should().NotBeNull();
            result.Data.Should().BeNull();
            result.Records.Should().Be(0);
            result.TotalRecords.Should().Be(0);
        }

        [Test]
        public void Search_WithResultsInStore_ShouldMapToWebModel()
        {
            SearchResult<TagModel> result = target.Search(new SearchParameters());

            TagModel model = result.Data.First();
            Tag tag = tagRepositoryMock.Store.First();

            model.Id.Should().Be(tag.Id);
            model.Title.Should().Be(tag.Title);
            model.UrlSlug.Should().Be(tag.UrlSlug);
        }

        [Test]
        public void Search_WithResultsInStore_ShouldReturnResult()
        {
            SearchResult<TagModel> result = target.Search(new SearchParameters());

            tagManagerMock.Verify(m => m.Search(It.IsAny<SearchParameters>()), Times.Once());
            result.Should().NotBeNull();
            result.Data.Should().NotBeNull();
            result.Records.Should().Be(tagRepositoryMock.Store.Count);
            result.TotalRecords.Should().Be(tagRepositoryMock.Store.Count);
        }

        #endregion
    }
}