﻿using System;
using System.Linq;
using System.Threading.Tasks;
using DevInterview.Bal.Managers.Identity;
using DevInterview.Bal.Tests;
using DevInterview.Bal.Tests.Mocks;
using DevInterview.Dal.Entities.Identity;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Web.Areas.ControlPanel.Controllers.WebApi;
using DevInterview.Web.Models;
using FluentAssertions;
using Microsoft.AspNet.Identity;
using Moq;
using NUnit.Framework;

namespace DevInterview.Web.Tests.Controllers.Api
{
    [TestFixture]
    public class ApiUsersControllerTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            ModelBindingManager.Init();

            roleStoreMock = new RoleRepositoryMock(FakeDataProvider.PopulateRoleStore());
            roleManagerMock = new Mock<AppRoleManager>(roleStoreMock.Instance.Object);
            roleManagerMock.SetupGet(r => r.Roles).Returns(new EnumerableQuery<AppRole>(roleStoreMock.Store));
            roleManagerMock.Setup(m => m.FindByIdAsync(It.IsAny<int>())).Returns<int>(id => Task.FromResult(roleStoreMock.Store.First(u => u.Id == id)));

            userStoreMock = new UserRepositoryMock(FakeDataProvider.PopulateUserStore());
            userManagerMock = new Mock<AppUserManager>(userStoreMock.Instance.Object);
            userManagerMock.Setup(m => m.GetAllAsync())
                           .Returns(Task.FromResult(new SearchResult<AppUser>
                           {
                               Data = userStoreMock.Store,
                               Records = userStoreMock.Store.Count(),
                               TotalRecords = userStoreMock.Store.Count()
                           }));
            userManagerMock.Setup(m => m.FindByIdAsync(It.IsAny<int>())).Returns<int>(id => Task.FromResult(userStoreMock.Store.First(u => u.Id == id)));
            userManagerMock.Setup(m => m.UpdateAsync(It.IsAny<AppUser>()))
                           .Returns(Task.FromResult(IdentityResult.Success))
                           .Callback<AppUser>(u =>
                           {
                               AppUser result = userStoreMock.Store.Single(user => user.Id == u.Id);
                               result = u;
                           });
            userManagerMock.Setup(m => m.AddToRoleAsync(It.IsAny<int>(), It.IsAny<string>())).Returns(Task.FromResult(IdentityResult.Success));
            userManagerMock.Setup(m => m.DeleteAsync(It.IsAny<AppUser>())).Returns(Task.FromResult(IdentityResult.Success));

            target = new UsersController(userManagerMock.Object, roleManagerMock.Object);
        }

        #endregion


        #region All other members

        private Mock<AppRoleManager> roleManagerMock;

        private RoleRepositoryMock roleStoreMock;

        private UsersController target;

        private Mock<AppUserManager> userManagerMock;

        private UserRepositoryMock userStoreMock;

        #endregion


        #region Test Methods

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public void Delete_WithInvalidId_ShouldThrowException(int id)
        {
            Assert.Throws<AggregateException>(() => target.Delete(id).Wait());
        }

        [Test]
        public async Task Delete_WithUserFoundInStorageAndSuccessfulResult_ShouldDeleteUser()
        {
            userManagerMock.Setup(m => m.DeleteAsync(It.IsAny<AppUser>()))
                           .Returns(Task.FromResult(IdentityResult.Success))
                           .Callback<AppUser>(u => userStoreMock.Store.RemoveAll(user => user.Id == u.Id));

            const int id = 1;
            await target.Delete(id);
            AppUser userInStorage = userStoreMock.Store.FirstOrDefault(u => u.Id == id);

            userManagerMock.Verify(m => m.DeleteAsync(It.IsAny<AppUser>()), Times.Once());
            userManagerMock.Verify(m => m.FindByIdAsync(id), Times.Once());
            userInStorage.Should().BeNull();
        }

        [Test]
        public void Delete_WithUserInStorageAndNotSuccessfulResult_ShouldThrowException()
        {
            userManagerMock.Setup(m => m.DeleteAsync(It.IsAny<AppUser>())).Returns(Task.FromResult(new IdentityResult("Error occured.")));

            Assert.Throws<AggregateException>(() => target.Delete(1).Wait());
        }

        [Test]
        public void Delete_WithUserNotFoundInstorage_ShouldThrowException()
        {
            userManagerMock.Setup(m => m.FindByIdAsync(It.IsAny<int>())).Returns(Task.FromResult((AppUser)null));

            Assert.Throws<AggregateException>(() => target.Delete(100).Wait());
        }

        [Test]
        public async Task Get_WithoutUsersInStorage_ShouldReturnEmptyResult()
        {
            userManagerMock.Setup(m => m.GetAllAsync()).Returns(Task.FromResult((SearchResult<AppUser>)null));

            SearchResult<AppUserModel> result = await target.Get();

            userManagerMock.Verify(m => m.GetAllAsync(), Times.Once);
            result.Should().NotBeNull();
            result.Data.Should().BeNull();
            result.Records.Should().Be(0);
            result.TotalRecords.Should().Be(0);
        }

        [Test]
        public async Task Get_WithUsersInStorage_ShouldMapToWebModel()
        {
            SearchResult<AppUserModel> result = await target.Get();
            AppUserModel model = result.Data.First();
            AppUser user = userStoreMock.Store.First();

            userManagerMock.Verify(m => m.GetAllAsync(), Times.Once);
            model.Id.Should().Be(user.Id);
            model.Created.Should().Be(user.Created);
            model.IsActive.Should().Be(user.IsActive);
            model.IsDeleted.Should().Be(user.IsDeleted);
            model.UserName.Should().Be(user.UserName);
            model.Email.Should().Be(user.Email);
            model.RoleId.Should().Be(0);
        }

        [Test]
        public async Task Get_WithUsersInStorage_ShouldReturnUsers()
        {
            SearchResult<AppUserModel> result = await target.Get();
            int usersCount = userStoreMock.Store.Count();

            userManagerMock.Verify(m => m.GetAllAsync(), Times.Once);
            result.Should().NotBeNull();
            result.Data.Should().NotBeNull();
            result.Data.Count().Should().Be(usersCount);
            result.Records.Should().Be(usersCount);
            result.TotalRecords.Should().Be(usersCount);
        }

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public void Put_WithInvalidModelId_ShouldThrowException(int id)
        {
            Assert.Throws<AggregateException>(() => target.Put(new AppUserModel
                                                          {
                                                              Id = id
                                                          })
                                                          .Wait());
        }

        [Test]
        public void Put_WithNullModel_ShouldThrowException()
        {
            Assert.Throws<AggregateException>(() => target.Put(null).Wait());
        }

        [Test]
        public async Task Put_WithoutRoleId_ShouldNotUpdateRole()
        {
            var user = new AppUserModel
            {
                Id = 1,
                RoleId = 0
            };

            await target.Put(user);

            userManagerMock.Verify(m => m.AddToRoleAsync(It.IsAny<int>(), It.IsAny<string>()), Times.Never());
            userManagerMock.Verify(m => m.FindByIdAsync(user.Id), Times.Once());
            userManagerMock.Verify(m => m.UpdateAsync(It.IsAny<AppUser>()), Times.Once());
        }

        [Test]
        public async Task Put_WithRoleId_ShouldUpdateRole()
        {
            var user = new AppUserModel
            {
                Id = 1,
                RoleId = 1
            };

            await target.Put(user);

            userManagerMock.Verify(m => m.AddToRoleAsync(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
            userManagerMock.Verify(m => m.FindByIdAsync(user.Id), Times.Once());
            userManagerMock.Verify(m => m.UpdateAsync(It.IsAny<AppUser>()), Times.Once());
        }

        [Test]
        public async Task Put_WithUserFoundInStorageAndSuccessfulResult_ShouldMapToWebModel()
        {
            var user = new AppUserModel
            {
                Id = 1,
                RoleId = 1,
                UserName = "NEW",
                IsActive = true,
                IsDeleted = true,
                Email = "new@gmail.com",
                Created = DateTime.Now
            };

            AppUserModel result = await target.Put(user);
            AppUser userInStorage = userStoreMock.Store.Single(u => u.Id == result.Id);

            userInStorage.Created.Should().Be(result.Created);
            userInStorage.Email.Should().Be(result.Email);
            userInStorage.UserName.Should().Be(result.UserName);
            userInStorage.IsActive.Should().Be(result.IsActive);
            userInStorage.IsDeleted.Should().Be(result.IsDeleted);
        }

        [Test]
        public async Task Put_WithUserFoundInStorageAndSuccessfulResult_ShouldUpdateUser()
        {
            var user = new AppUserModel
            {
                Id = 1,
                RoleId = 1,
                UserName = "NEW",
                IsActive = true,
                IsDeleted = true,
                Email = "new@gmail.com",
                Created = DateTime.Now
            };

            AppUserModel result = await target.Put(user);
            AppUser userInStorage = userStoreMock.Store.Single(u => u.Id == result.Id);

            userManagerMock.Verify(m => m.UpdateAsync(It.IsAny<AppUser>()), Times.Once());
            userManagerMock.Verify(m => m.FindByIdAsync(user.Id), Times.Once());
            result.Should().NotBeNull();
            result.Should().BeSameAs(user);
            userInStorage.Should().NotBeNull();
        }

        [Test]
        public void Put_WithUserInStorageAndNotSuccessfulResult_ShouldThrowException()
        {
            userManagerMock.Setup(m => m.UpdateAsync(It.IsAny<AppUser>())).Returns(Task.FromResult(new IdentityResult("Error occured.")));

            Assert.Throws<AggregateException>(() => target.Put(new AppUserModel
                                                          {
                                                              Id = 1,
                                                              RoleId = 1
                                                          })
                                                          .Wait());
        }

        [Test]
        public void Put_WithUserNotFoundInStorage_ShouldThrowException()
        {
            userManagerMock.Setup(m => m.FindByIdAsync(It.IsAny<int>())).Returns(Task.FromResult((AppUser)null));

            Assert.Throws<AggregateException>(() => target.Put(new AppUserModel
                                                          {
                                                              Id = 100
                                                          })
                                                          .Wait());
        }

        #endregion
    }
}