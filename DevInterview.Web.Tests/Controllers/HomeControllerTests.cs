﻿using System.Web.Mvc;
using Cfg;
using DevInterview.Common.Configuration;
using DevInterview.Web.Controllers;
using FluentAssertions;
using NUnit.Framework;

namespace DevInterview.Web.Tests.Controllers
{
    [TestFixture]
    public class HomeControllerTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            ModelBindingManager.Init();
            AppSettings.Init<AppCfgReader>();

            target = new HomeController();
        }

        #endregion


        #region All other members

        private HomeController target;

        #endregion


        #region Test Methods

        [Test]
        public void About_ShouldReturnView()
        {
            ActionResult result = target.About();

            result.Should().NotBeNull();
        }

        #endregion
    }
}