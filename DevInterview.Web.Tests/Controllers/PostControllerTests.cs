﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using DevInterview.Bal.Managers.Business.Interfaces;
using DevInterview.Bal.Tests;
using DevInterview.Common.Configuration;
using DevInterview.Common.Configuration.Interfaces;
using DevInterview.Common.Utilities.Extensions.UnitTesting;
using DevInterview.Common.Utilities.Helpers;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.Identity;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Web.Controllers;
using DevInterview.Web.Models;
using FluentAssertions;
using Moq;
using MvcRouteTester;
using NUnit.Framework;

namespace DevInterview.Web.Tests.Controllers
{
    [TestFixture]
    public class PostControllerTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            ModelBindingManager.Init();

            postManagerMock = new Mock<IPostManager>();
            post = new Post
            {
                Id = 1,
                Created = DateTime.Now,
                Title = "titale",
                UrlSlug = "url",
                BodyEnd = "end",
                BodyStart = "start",
                Category = new Category(),
                CreatedBy = new AppUser(),
                IsDeleted = false,
                IsVisible = true,
                LastUpdated = DateTime.Now,
                LastUpdatedBy = new AppUser(),
                Rating = new Rating(),
                Tags = new List<Tag>(),
                Views = 100
            };

            postStore = FakeDataProvider.PopulatePostStore();
            categoryStore = FakeDataProvider.PopulateCategoryStore();
            tagStore = FakeDataProvider.PopulateTagStore();

            postManagerMock.Setup(m => m.Get(It.IsAny<int>())).Returns(post);
            postManagerMock.Setup(m => m.Search(It.IsAny<SearchParameters>()))
                           .Returns(new SearchResult<Post>
                           {
                               Data = postStore,
                               Records = postStore.Count(),
                               TotalRecords = postStore.Count()
                           });
            postManagerMock.Setup(m => m.GetMostViewedPosts())
                           .Returns(new SearchResult<Post>
                           {
                               Data = postStore,
                               Records = postStore.Count(),
                               TotalRecords = postStore.Count()
                           });
            postManagerMock.Setup(m => m.GetTopRatedPosts())
                           .Returns(new SearchResult<Post>
                           {
                               Data = postStore,
                               Records = postStore.Count(),
                               TotalRecords = postStore.Count()
                           });
            postManagerMock.Setup(m => m.GetPostsByCategory(It.IsAny<int>(), It.IsAny<int>()))
                           .Returns(new SearchResult<Post>
                           {
                               Data = postStore,
                               Records = postStore.Count(),
                               TotalRecords = postStore.Count()
                           });
            postManagerMock.Setup(m => m.GetPostsByTag(It.IsAny<int>(), It.IsAny<int>()))
                           .Returns(new SearchResult<Post>
                           {
                               Data = postStore,
                               Records = postStore.Count(),
                               TotalRecords = postStore.Count()
                           });

            categoryManagerMock = new Mock<ICategoryManager>();
            categoryManagerMock.Setup(m => m.Search(It.IsAny<SearchParameters>()))
                               .Returns(new SearchResult<Category>
                               {
                                   Data = categoryStore.Take(1),
                                   Records = 1,
                                   TotalRecords = categoryStore.Count()
                               });

            botDetectorMock = new Mock<IBotDetector>();
            botDetectorMock.Setup(m => m.CheckUserAgentForBot(It.IsAny<string>())).Returns(false);

            tagManagerMock = new Mock<ITagManager>();
            tagManagerMock.Setup(m => m.Search(It.IsAny<SearchParameters>()))
                          .Returns(new SearchResult<Tag>
                          {
                              Data = tagStore.Take(1),
                              Records = 1,
                              TotalRecords = tagStore.Count()
                          });

            constantsProviderMock = new Mock<IConstantsProvider>();
            constantsProviderMock.SetupGet(m => m.CookieName).Returns("CookieName");
            constantsProviderMock.SetupGet(m => m.CookieViewedPostsKey).Returns("CookieViewedPostsKey");
            constantsProviderMock.SetupGet(m => m.CookieVotedPostsKey).Returns("CookieVotedPostsKey");

            var routes = new RouteCollection();
            routes.MapAttributeRoutesInAssembly(typeof(PostController));
            RouteConfig.RegisterRoutes(routes);

            target = new PostController(postManagerMock.Object, categoryManagerMock.Object, tagManagerMock.Object, constantsProviderMock.Object, botDetectorMock.Object);
            target.SetFakeControllerContext(routes);
        }

        #endregion


        #region All other members

        private Post post;

        private IEnumerable<Post> postStore;

        private IEnumerable<Category> categoryStore;

        private IEnumerable<Tag> tagStore;

        private Mock<IPostManager> postManagerMock;

        private Mock<ICategoryManager> categoryManagerMock;

        private Mock<ITagManager> tagManagerMock;

        private Mock<IConstantsProvider> constantsProviderMock;

        private Mock<IBotDetector> botDetectorMock;

        private PostController target;

        #endregion


        #region Test Methods

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void GetByCategory_WithInvalidCategory_ShouldThrowException(string category)
        {
            Assert.Throws<HttpException>(() => target.GetByCategory(category));
        }

        [Test]
        public void GetByCategory_WithValidCategory_ShouldSearchCategory()
        {
            const string cat = "dotnet";

            target.GetByCategory(cat);

            categoryManagerMock.Verify(m => m.Search(It.Is<SearchParameters>(s => s.Count == 1 && s.Page == 1 && s.SearchBy == "UrlSlug" && s.SearchQuery == cat)), Times.Once);
        }

        [Test]
        public void GetByCategory_WithValidCategoryAndDefaultPage_ShouldSearchPostWithFirstPage()
        {
            const string cat = "dotnet";
            Category result = categoryStore.Take(1).FirstOrDefault();

            target.GetByCategory(cat);

            postManagerMock.Verify(m => m.GetPostsByCategory(It.Is<int>(id => id == result.Id), It.Is<int>(p => p == 1)), Times.Once);
        }

        [Test]
        public void GetByCategory_WithValidCategoryAndMultipleCategoriesInStorage_ShouldThrowException()
        {
            categoryManagerMock.Setup(m => m.Search(It.IsAny<SearchParameters>()))
                               .Returns(new SearchResult<Category>
                               {
                                   Data = categoryStore,
                                   Records = categoryStore.Count(),
                                   TotalRecords = categoryStore.Count()
                               });

            const string cat = "dotnet";

            Assert.Throws<HttpException>(() => target.GetByCategory(cat));
        }

        [Test]
        public void GetByCategory_WithValidCategoryAndPage_ShouldSearchPostWithPage()
        {
            const string cat = "dotnet";
            const int page = 2;
            Category result = categoryStore.Take(1).FirstOrDefault();

            target.GetByCategory(cat, page);

            postManagerMock.Verify(m => m.GetPostsByCategory(It.Is<int>(id => id == result.Id), It.Is<int>(p => p == page)), Times.Once);
        }

        [Test]
        public void GetByCategory_WithValidCategoryAndZeroCategoriesInStorage_ShouldThrowException()
        {
            categoryManagerMock.Setup(m => m.Search(It.IsAny<SearchParameters>()))
                               .Returns(new SearchResult<Category>
                               {
                                   Data = categoryStore,
                                   Records = 0,
                                   TotalRecords = 0
                               });

            const string cat = "dotnet";

            Assert.Throws<HttpException>(() => target.GetByCategory(cat));
        }

        [Test]
        public void GetByCategory_WithValidParams_ShouldMapToWebModel()
        {
            const string cat = "dotnet";

            var result = target.GetByCategory(cat) as ViewResult;
            var model = result.Model as SearchResult<PostModel>;

            model.Data.Should().NotBeNull();
            model.Data.Count().Should().Be(postStore.Count());
            model.Records.Should().Be(postStore.Count());
            model.TotalRecords.Should().Be(postStore.Count());
        }

        [Test]
        public void GetByCategory_WithValidParams_ShouldReturnViewWithModel()
        {
            const string cat = "dotnet";

            var result = target.GetByCategory(cat) as ViewResult;

            result.Should().NotBeNull();

            var model = result.Model as SearchResult<PostModel>;
            model.Should().NotBeNull();
        }

        [Test]
        public void GetByCategory_WithValidParams_ShouldSetPageTitleAndDescription()
        {
            const string cat = "dotnet";

            var result = target.GetByCategory(cat) as ViewResult;
            var title = result.ViewBag.PageTitle as string;
            var description = result.ViewBag.PageDescription as string;

            title.Should().NotBeNull();
            description.Should().NotBeNull();
        }

        [Test]
        [TestCase(0)]
        [TestCase(-1)]
        public void GetById_WithInvalidId_ShouldThrowException(int id)
        {
            Assert.Throws<HttpException>(() => target.GetById(id));
        }

        [Test]
        public void GetById_WithPostFoundInStorage_ShouldMapToWebModel()
        {
            const int id = 1;

            var result = target.GetById(id) as ViewResult;

            var model = result.Model as PostModel;
            model.Id.Should().Be(post.Id);
            model.BodyEnd.Should().Be(post.BodyEnd);
            model.BodyStart.Should().Be(post.BodyStart);
            model.Category.Should().NotBeNull();
            model.Created.Should().Be(post.Created);
            model.IsDeleted.Should().Be(post.IsDeleted);
            model.IsVisible.Should().Be(post.IsVisible);
            model.LastUpdated.Should().Be(post.LastUpdated);
            model.Rating.Should().NotBeNull();
            model.Tags.Should().NotBeNull();
            model.Title.Should().Be(post.Title);
            model.UrlSlug.Should().Be(post.UrlSlug);
            model.Views.Should().Be(post.Views - 1);
        }

        [Test]
        public void GetById_WithPostFoundInStorage_ShouldReturnPost()
        {
            const int id = 1;

            var result = target.GetById(id) as ViewResult;

            postManagerMock.Verify(m => m.Get(id), Times.Once);
            result.Should().NotBeNull();

            var model = result.Model as PostModel;
            model.Should().NotBeNull();
        }

        [Test]
        public void GetById_WithPostFoundInStorage_ShouldSetIsFullPostToTrue()
        {
            const int id = 1;

            var result = target.GetById(id) as ViewResult;

            var model = result.Model as PostModel;
            model.IsFullPost.Should().BeTrue();
        }

        [Test]
        public void GetById_WithPostNotFoundInStorage_ShouldThrowException()
        {
            postManagerMock.Setup(m => m.Get(It.IsAny<int>())).Returns((Post)null);

            Assert.Throws<HttpException>(() => target.GetById(1));
        }

        [Test]
        [TestCase(null)]
        [TestCase("")]
        public void GetByTag_WithInvalidTag_ShouldThrowException(string tag)
        {
            Assert.Throws<HttpException>(() => target.GetByTag(tag));
        }

        [Test]
        public void GetByTag_WithValidParams_ShouldMapToWebModel()
        {
            const string tag = "dotnet";

            var result = target.GetByTag(tag) as ViewResult;
            var model = result.Model as SearchResult<PostModel>;

            model.Data.Should().NotBeNull();
            model.Data.Count().Should().Be(postStore.Count());
            model.Records.Should().Be(postStore.Count());
            model.TotalRecords.Should().Be(postStore.Count());
        }

        [Test]
        public void GetByTag_WithValidParams_ShouldReturnViewWithModel()
        {
            const string cat = "dotnet";

            var result = target.GetByTag(cat) as ViewResult;

            result.Should().NotBeNull();

            var model = result.Model as SearchResult<PostModel>;
            model.Should().NotBeNull();
        }

        [Test]
        public void GetByTag_WithValidParams_ShouldSetPageTitle()
        {
            const string tag = "dotnet";

            var result = target.GetByTag(tag) as ViewResult;
            var title = result.ViewBag.PageTitle as string;

            title.Should().NotBeNull();
        }

        [Test]
        public void GetByTag_WithValidTag_ShouldSearchTag()
        {
            const string tag = "dotnet";

            target.GetByTag(tag);

            tagManagerMock.Verify(m => m.Search(It.Is<SearchParameters>(s => s.Count == 1 && s.Page == 1 && s.SearchBy == "UrlSlug" && s.SearchQuery == tag)), Times.Once);
        }

        [Test]
        public void GetByTag_WithValidTagAndDefaultPage_ShouldSearchPostWithFirstPage()
        {
            const string tag = "dotnet";
            Tag result = tagStore.Take(1).FirstOrDefault();

            target.GetByTag(tag);

            postManagerMock.Verify(m => m.GetPostsByTag(It.Is<int>(id => id == result.Id), It.Is<int>(p => p == 1)), Times.Once);
        }

        [Test]
        public void GetByTag_WithValidTagAndMultipleCategoriesInStorage_ShouldThrowException()
        {
            tagManagerMock.Setup(m => m.Search(It.IsAny<SearchParameters>()))
                          .Returns(new SearchResult<Tag>
                          {
                              Data = tagStore,
                              Records = tagStore.Count(),
                              TotalRecords = tagStore.Count()
                          });

            const string tag = "dotnet";

            Assert.Throws<HttpException>(() => target.GetByTag(tag));
        }

        [Test]
        public void GetByTag_WithValidTagAndPage_ShouldSearchPostWithPage()
        {
            const string tag = "dotnet";
            const int page = 2;
            Tag result = tagStore.Take(1).FirstOrDefault();

            target.GetByTag(tag, page);

            postManagerMock.Verify(m => m.GetPostsByTag(It.Is<int>(id => id == result.Id), It.Is<int>(p => p == page)), Times.Once);
        }

        [Test]
        public void GetByTag_WithValidTagAndZeroCategoriesInStorage_ShouldThrowException()
        {
            tagManagerMock.Setup(m => m.Search(It.IsAny<SearchParameters>()))
                          .Returns(new SearchResult<Tag>
                          {
                              Data = new List<Tag>(),
                              Records = 0,
                              TotalRecords = 0
                          });

            const string tag = "dotnet";

            Assert.Throws<HttpException>(() => target.GetByTag(tag));
        }

        [Test]
        public void Index_WithoutPage_ShouldReturn10LatestPosts()
        {
            var result = target.Index() as ViewResult;

            postManagerMock.Verify(m => m.Search(It.Is<SearchParameters>(p => p.Count == AppCfgReader.PageSize &&
                                                                              p.Page == 1 &&
                                                                              p.OrderBy == "Created" &&
                                                                              p.OrderDirection == "desc")),
                                   Times.Once);
            result.Should().NotBeNull();
            result.GetType().ToString().Should().Be(typeof(ViewResult).ToString());

            var model = result.Model as SearchResult<PostModel>;
            model.Should().NotBeNull();
        }

        [Test]
        public void Index_WithoutPageAndData_ShouldThrowException()
        {
            postManagerMock.Setup(m => m.Search(It.IsAny<SearchParameters>()))
                           .Returns(new SearchResult<Post>
                           {
                               Data = new List<Post>(),
                               Records = 0,
                               TotalRecords = 0
                           });

            Assert.Throws<HttpException>(() => target.Index());
        }

        [Test]
        public void Index_WithoutPageAndWithData_ShouldMapToWebModel()
        {
            var result = target.Index() as ViewResult;

            postManagerMock.Verify(m => m.Search(It.Is<SearchParameters>(p => p.Count == AppCfgReader.PageSize &&
                                                                              p.Page == 1 &&
                                                                              p.OrderBy == "Created" &&
                                                                              p.OrderDirection == "desc")),
                                   Times.Once);
            var model = result.Model as SearchResult<PostModel>;
            model.Data.Should().NotBeNull();
            model.Data.Count().Should().Be(postStore.Count());
            model.Records.Should().Be(postStore.Count());
            model.TotalRecords.Should().Be(postStore.Count());
        }

        [Test]
        public void Index_WithPageAndData_ShouldMapToWebModel()
        {
            const int page = 2;

            var result = target.Index(page) as ViewResult;

            postManagerMock.Verify(m => m.Search(It.Is<SearchParameters>(p => p.Count == AppCfgReader.PageSize &&
                                                                              p.Page == page &&
                                                                              p.OrderBy == "Created" &&
                                                                              p.OrderDirection == "desc")),
                                   Times.Once);
            var model = result.Model as SearchResult<PostModel>;
            model.Data.Should().NotBeNull();
            model.Data.Count().Should().Be(postStore.Count());
            model.Records.Should().Be(postStore.Count());
            model.TotalRecords.Should().Be(postStore.Count());
        }

        [Test]
        public void Index_WithPageAndWithoutData_ShouldThrowException()
        {
            postManagerMock.Setup(m => m.Search(It.IsAny<SearchParameters>()))
                           .Returns(new SearchResult<Post>
                           {
                               Data = new List<Post>(),
                               Records = 0,
                               TotalRecords = 0
                           });

            const int page = 2;

            Assert.Throws<HttpException>(() => target.Index(page));
        }

        [Test]
        public void MostViewed_ShouldReturnResultAndMapToWebModel()
        {
            var result = target.MostViewed() as ViewResult;

            postManagerMock.Verify(m => m.GetMostViewedPosts(), Times.Once);
            result.Should().NotBeNull();
            result.GetType().ToString().Should().Be(typeof(ViewResult).ToString());

            var model = result.Model as SearchResult<PostModel>;
            model.Data.Should().NotBeNull();
            model.Data.Count().Should().Be(postStore.Count());
            model.Records.Should().Be(postStore.Count());
            model.TotalRecords.Should().Be(postStore.Count());
        }

        [Test]
        public void TopRated_ShouldReturnResultAndMapToWebModel()
        {
            var result = target.TopRated() as ViewResult;

            postManagerMock.Verify(m => m.GetTopRatedPosts(), Times.Once);
            result.Should().NotBeNull();
            result.GetType().ToString().Should().Be(typeof(ViewResult).ToString());

            var model = result.Model as SearchResult<PostModel>;
            model.Data.Should().NotBeNull();
            model.Data.Count().Should().Be(postStore.Count());
            model.Records.Should().Be(postStore.Count());
            model.TotalRecords.Should().Be(postStore.Count());
        }

        #endregion
    }
}