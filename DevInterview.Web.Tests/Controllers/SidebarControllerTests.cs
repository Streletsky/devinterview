﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using DevInterview.Bal.Managers.Business.Interfaces;
using DevInterview.Bal.Providers.Interfaces;
using DevInterview.Bal.Tests;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Web.Controllers;
using DevInterview.Web.Models;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace DevInterview.Web.Tests.Controllers
{
    [TestFixture]
    public class SidebarControllerTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            ModelBindingManager.Init();

            postManagerMock = new Mock<IPostManager>();
            tagManagerMock = new Mock<ITagManager>();
            catManagerMock = new Mock<ICategoryManager>();
            statisticsProviderMock = new Mock<IStatisticsProvider>();

            catManagerMock.Setup(m => m.GetAll())
                          .Returns(new SearchResult<Category>
                          {
                              Data = categories,
                              TotalRecords = categories.Count,
                              Records = categories.Count
                          });

            tagManagerMock.Setup(m => m.GetAll())
                          .Returns(new SearchResult<Tag>
                          {
                              Data = tags,
                              TotalRecords = tags.Count,
                              Records = tags.Count
                          });

            postManagerMock.Setup(m => m.GetPostsCountByCategory(It.IsAny<int>())).Returns(posts.Count);
            postManagerMock.Setup(m => m.GetTaggedPostsCountByTag(It.IsAny<int>())).Returns(tags.Count);

            target = new SidebarController(catManagerMock.Object, tagManagerMock.Object, postManagerMock.Object, statisticsProviderMock.Object);
        }

        #endregion


        #region All other members

        private readonly List<Category> categories = FakeDataProvider.PopulateCategoryStore();

        private readonly List<Post> posts = FakeDataProvider.PopulatePostStore();

        private readonly List<Tag> tags = FakeDataProvider.PopulateTagStore();

        private Mock<ICategoryManager> catManagerMock;

        private Mock<IPostManager> postManagerMock;

        private Mock<ITagManager> tagManagerMock;

        private SidebarController target;

        private Mock<IStatisticsProvider> statisticsProviderMock;

        #endregion


        #region Test Methods

        [Test]
        public void About_ShouldReturnModel()
        {
            var result = target.About() as ViewResult;

            statisticsProviderMock.Verify(m => m.CategoriesCount(), Times.Once);
            statisticsProviderMock.Verify(m => m.TagsCount(), Times.Once);
            statisticsProviderMock.Verify(m => m.PostsCount(), Times.Once);
            statisticsProviderMock.Verify(m => m.VotesCount(), Times.Once);
            statisticsProviderMock.Verify(m => m.DaysSinceStart(), Times.Once);
            result.Should().NotBeNull();
        }

        [Test]
        public void Categories_ShouldReturnPostsCountWithModel()
        {
            var result = target.Categories() as ViewResult;

            postManagerMock.Verify(m => m.GetPostsCountByCategory(It.IsAny<int>()), Times.Exactly(categories.Count));
            var model = result.Model as List<CategoryModel>;
            CategoryModel catModel = model.FirstOrDefault();
            catModel.PostsCount.Should().Be(posts.Count);
        }

        [Test]
        public void Categories_WithCategoriesInStorage_ShouldMapToWebModel()
        {
            Category cat = categories.First(c => c.Id == 1);

            var result = target.Categories() as ViewResult;

            var model = result.Model as List<CategoryModel>;
            CategoryModel catModel = model.First(c => c.Id == 1);
            catModel.Should().NotBeNull();
            catModel.Id.Should().Be(cat.Id);
            catModel.Title.Should().Be(cat.Title);
            catModel.Description.Should().Be(cat.Description);
            catModel.UrlSlug.Should().Be(cat.UrlSlug);
        }

        [Test]
        public void Categories_WithCategoriesInStorage_ShouldReturnResult()
        {
            var result = target.Categories() as ViewResult;

            catManagerMock.Verify(m => m.GetAll(), Times.Once);
            result.Should().NotBeNull();

            var model = result.Model as List<CategoryModel>;
            model.Should().NotBeNull();
            model.Count.Should().Be(categories.Count);
        }

        [Test]
        public void Categories_WithoutCategoriesInStorage_ShouldReturnEmptyResult()
        {
            catManagerMock.Setup(m => m.GetAll()).Returns(new SearchResult<Category>());

            var result = target.Categories() as ViewResult;

            catManagerMock.Verify(m => m.GetAll(), Times.Once);
            result.Should().NotBeNull();

            var model = result.Model as List<CategoryModel>;
            model.Should().NotBeNull();
            model.Count.Should().Be(0);
        }

        [Test]
        public void Tags_ShouldReturnPostsCountWithModel()
        {
            var result = target.Tags() as ViewResult;

            postManagerMock.Verify(m => m.GetTaggedPostsCountByTag(It.IsAny<int>()), Times.Exactly(tags.Count));
            var model = result.Model as List<TagModel>;
            TagModel tagModel = model.FirstOrDefault();
            tagModel.PostsCount.Should().Be(posts.Count);
        }

        [Test]
        public void Tags_WithoutTagsInStorage_ShouldReturnEmptyResult()
        {
            tagManagerMock.Setup(m => m.GetAll()).Returns(new SearchResult<Tag>());

            var result = target.Tags() as ViewResult;

            tagManagerMock.Verify(m => m.GetAll(), Times.Once);
            result.Should().NotBeNull();

            var model = result.Model as List<TagModel>;
            model.Should().NotBeNull();
            model.Count.Should().Be(0);
        }

        [Test]
        public void Tags_WithTagsInStorage_ShouldMapToWebModel()
        {
            Tag tag = tags.FirstOrDefault();

            var result = target.Tags() as ViewResult;

            var model = result.Model as List<TagModel>;
            TagModel tagModel = model.FirstOrDefault();
            tagModel.Should().NotBeNull();
            tagModel.Id.Should().Be(tag.Id);
            tagModel.Title.Should().Be(tag.Title);
            tagModel.UrlSlug.Should().Be(tag.UrlSlug);
        }

        [Test]
        public void Tags_WithTagsInStorage_ShouldReturnResult()
        {
            var result = target.Tags() as ViewResult;

            tagManagerMock.Verify(m => m.GetAll(), Times.Once);
            result.Should().NotBeNull();

            var model = result.Model as List<TagModel>;
            model.Should().NotBeNull();
            model.Count.Should().Be(tags.Count);
        }

        #endregion
    }
}