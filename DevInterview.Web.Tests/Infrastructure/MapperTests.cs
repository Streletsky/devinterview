﻿using AutoMapper;
using NUnit.Framework;

namespace DevInterview.Web.Tests.Infrastructure
{
    [TestFixture]
    public class MapperTests
    {
        #region Test Methods

        [Test]
        public void Assert_ShouldNotThrowExceptions()
        {
            ModelBindingManager.Init();
            Mapper.AssertConfigurationIsValid();
        }

        #endregion
    }
}