﻿using System.Web.Routing;
using DevInterview.Web.Controllers;
using MvcRouteTester;
using NUnit.Framework;

namespace DevInterview.Web.Tests.Infrastructure
{
    [TestFixture]
    public class RoutesTests
    {
        #region Setup/Teardown Methods

        [SetUp]
        public void Setup()
        {
            target = new RouteCollection();
            target.MapAttributeRoutesInAssembly(typeof(HomeController));
            RouteConfig.RegisterRoutes(target);
        }

        #endregion


        #region All other members

        private RouteCollection target;

        #endregion


        #region Test Methods

        [Test]
        public void Contact_ShouldMapUrlToTarget()
        {
            RouteAssert.HasRoute(target, "/About");
            target.ShouldMap("/About").To<HomeController>(x => x.About());
        }

        [Test]
        public void Default_ShouldMapUrlToTarget()
        {
            RouteAssert.HasRoute(target, "/");
            target.ShouldMap("/").To<PostController>(x => x.Index(null));
        }

        [Test]
        public void Error_ShouldMapUrlToTarget()
        {
            RouteAssert.HasRoute(target, "/Error");
            target.ShouldMap("/Error").To<ErrorController>(x => x.GenericError());
        }

        [Test]
        public void GetByCategory_WithoutPage_ShouldMapUrlToTarget()
        {
            RouteAssert.HasRoute(target, "/c/asp-net");
            target.ShouldMap("/c/asp-net").To<PostController>(x => x.GetByCategory("asp-net", null));
        }

        [Test]
        public void GetByCategory_WithPage_ShouldMapUrlToTarget()
        {
            RouteAssert.HasRoute(target, "/c/asp-net/2");
            target.ShouldMap("/c/asp-net/2").To<PostController>(x => x.GetByCategory("asp-net", 2));
        }

        [Test]
        public void MainPages_WithPage_ShouldMapUrlToTarget()
        {
            RouteAssert.HasRoute(target, "/p/2");
            target.ShouldMap("/p/2").To<PostController>(x => x.Index(2));
        }

        [Test]
        public void MostViewed_ShouldMapUrlToTarget()
        {
            RouteAssert.HasRoute(target, "/MostViewed");
            target.ShouldMap("/MostViewed").To<PostController>(x => x.MostViewed());
        }

        [Test]
        public void Post_ShouldMapUrlToTarget()
        {
            RouteAssert.HasRoute(target, "/p/asp-net/url-2");
            target.ShouldMap("/p/asp-net/url-2").To<PostController>(x => x.GetById(2));
        }

        [Test]
        public void TopRated_ShouldMapUrlToTarget()
        {
            RouteAssert.HasRoute(target, "/TopRated");
            target.ShouldMap("/TopRated").To<PostController>(x => x.TopRated());
        }

        #endregion
    }
}