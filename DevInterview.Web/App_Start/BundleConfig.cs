﻿using System.Web.Optimization;

namespace DevInterview.Web
{
    public class BundleConfig
    {
        #region Public Methods and Operators

        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include("~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/browser-selector").Include("~/Scripts/css-browser-selector.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include("~/Scripts/underscore.js",
                                                                      "~/Scripts/underscore.string.js",
                                                                      "~/Scripts/angular.js",
                                                                      "~/Scripts/angular-resource.js",
                                                                      "~/Scripts/angular-route.js"));

            bundles.Add(new ScriptBundle("~/bundles/ckeditor").Include("~/Scripts/ckeditor/ckeditor.js", "~/Scripts/ng-ckeditor.js"));

            bundles.Add(new ScriptBundle("~/bundles/ngtablejs").Include("~/Scripts/ng-table.js"));

            bundles.Add(new ScriptBundle("~/bundles/ngtagsjs").Include("~/Scripts/ng-tags-input.js"));

            bundles.Add(new ScriptBundle("~/bundles/unit-tests").Include("~/Scripts/jasmine/jasmine.js",
                                                                         "~/Scripts/jasmine/jasmine-html.js",
                                                                         "~/Scripts/jasmine/console.js",
                                                                         "~/Scripts/jasmine/boot.js",
                                                                         "~/Scripts/angular-mocks.js",
                                                                         "~/Scripts/app/tests/testsSuite.js"));

            bundles.Add(new ScriptBundle("~/bundles/controlpanel").IncludeDirectory("~/Scripts/controlpanel/", "*.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").IncludeDirectory("~/Scripts/app/", "*.js"));

            bundles.Add(new StyleBundle("~/bundles/css").Include("~/Content/bootstrap.css", "~/Content/Master.css"));

            bundles.Add(new StyleBundle("~/bundles/ngtable").Include("~/Content/ng-table.css"));

            bundles.Add(new StyleBundle("~/bundles/ngtagsinput").Include("~/Content/ng-tags-input.css", "~/Content/ng-tags-input.bootstrap.css"));

            bundles.Add(new StyleBundle("~/bundles/jasmine").Include("~/Content/jasmine/jasmine.css"));
        }

        #endregion
    }
}