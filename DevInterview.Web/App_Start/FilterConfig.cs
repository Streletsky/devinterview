﻿using System.Web.Mvc;
using System.Web.Routing;
using DevInterview.Web.Infrastructure.Filters;

namespace DevInterview.Web
{
    public class FilterConfig
    {
        #region Public Methods and Operators

        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new RedirectToCanonicalUrlAttribute(RouteTable.Routes.AppendTrailingSlash, RouteTable.Routes.LowercaseUrls));
        }

        #endregion
    }
}