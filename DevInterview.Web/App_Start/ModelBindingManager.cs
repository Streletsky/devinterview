﻿using AutoMapper;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.Identity;
using DevInterview.Web.Models;

namespace DevInterview.Web
{
    public static class ModelBindingManager
    {
        #region Public Methods and Operators

        public static void Init()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Post, PostModel>().ForMember(p => p.IsFullPost, opt => opt.Ignore());
                cfg.CreateMap<PostModel, Post>().ForMember(p => p.CreatedBy, opt => opt.Ignore()).ForMember(p => p.LastUpdatedBy, opt => opt.Ignore());
                cfg.CreateMap<Category, CategoryModel>().ForMember(c => c.PostsCount, opt => opt.Ignore());
                cfg.CreateMap<CategoryModel, Category>();
                cfg.CreateMap<Rating, RatingModel>();
                cfg.CreateMap<RatingModel, Rating>();
                cfg.CreateMap<Tag, TagModel>().ForMember(p => p.PostsCount, opt => opt.Ignore());
                cfg.CreateMap<TagModel, Tag>().ForMember(p => p.Posts, opt => opt.Ignore());
                cfg.CreateMap<AppUser, AppUserModel>().ForMember(p => p.RoleId, opt => opt.Ignore());
                cfg.CreateMap<AppUserModel, AppUser>()
                   .ForMember(p => p.AccessFailedCount, opt => opt.Ignore())
                   .ForMember(p => p.Claims, opt => opt.Ignore())
                   .ForMember(p => p.EmailConfirmed, opt => opt.Ignore())
                   .ForMember(p => p.LockoutEnabled, opt => opt.Ignore())
                   .ForMember(p => p.LockoutEndDateUtc, opt => opt.Ignore())
                   .ForMember(p => p.Logins, opt => opt.Ignore())
                   .ForMember(p => p.PasswordHash, opt => opt.Ignore())
                   .ForMember(p => p.PhoneNumber, opt => opt.Ignore())
                   .ForMember(p => p.PhoneNumberConfirmed, opt => opt.Ignore())
                   .ForMember(p => p.Roles, opt => opt.Ignore())
                   .ForMember(p => p.SecurityStamp, opt => opt.Ignore())
                   .ForMember(p => p.TwoFactorEnabled, opt => opt.Ignore());
                cfg.CreateMap<AppRole, AppRoleModel>();
                cfg.CreateMap<AppRoleModel, AppRole>().ForMember(p => p.Users, opt => opt.Ignore());
            });
        }

        #endregion
    }
}