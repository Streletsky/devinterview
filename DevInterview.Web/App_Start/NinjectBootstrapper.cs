using System;
using System.Data.Entity;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using DevInterview.Bal.Managers.Business;
using DevInterview.Bal.Managers.Business.Interfaces;
using DevInterview.Bal.Managers.Caching;
using DevInterview.Bal.Managers.Caching.Interfaces;
using DevInterview.Bal.Managers.Identity;
using DevInterview.Bal.Managers.Logging;
using DevInterview.Bal.Managers.Logging.Interfaces;
using DevInterview.Bal.Providers;
using DevInterview.Bal.Providers.Interfaces;
using DevInterview.Bal.SearchEngine;
using DevInterview.Bal.SearchEngine.Base;
using DevInterview.Common.Configuration;
using DevInterview.Common.Configuration.Interfaces;
using DevInterview.Common.Logging;
using DevInterview.Common.Logging.Interfaces;
using DevInterview.Common.Utilities.Helpers;
using DevInterview.Dal.OrmInfrastructure;
using DevInterview.Dal.Repositories.Business;
using DevInterview.Dal.Repositories.Business.Interfaces;
using DevInterview.Dal.Repositories.Identity;
using DevInterview.Dal.Repositories.Identity.Interfaces;
using DevInterview.Web;
using DevInterview.Web.Infrastructure.SitemapGenerator;
using DevInterview.Web.Infrastructure.SitemapGenerator.Interfaces;
using Microsoft.Owin.Security;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Mvc;
using WebActivatorEx;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectBootstrapper), "Start")]
[assembly: ApplicationShutdownMethod(typeof(NinjectBootstrapper), "Stop")]

namespace DevInterview.Web
{
    public static class NinjectBootstrapper
    {
        #region Static Fields

        private static readonly Bootstrapper Bootstrapper = new Bootstrapper();

        #endregion


        #region Public Methods and Operators

        /// <summary>
        ///     Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            Bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        ///     Stops the application.
        /// </summary>
        public static void Stop()
        {
            Bootstrapper.ShutDown();
        }

        #endregion


        #region Methods

        /// <summary>
        ///     Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
                GlobalConfiguration.Configuration.DependencyResolver = new Ninject.Web.WebApi.NinjectDependencyResolver(kernel);

                return kernel;
            }
            catch (Exception ex)
            {
                NLogSingleton.Instance.Error(ex);
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        ///     Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            NLogSingleton.Instance.Info("Registering services with Ninject");

            kernel.Bind<DbContext>().To<AppContext>();

            kernel.Bind<ICacheManager>().To<MemoryCacheManager>();
            kernel.Bind<ICategoryManager>().To<CategoryManager>();
            kernel.Bind<IPostManager>().To<PostManager>();
            kernel.Bind<ITagManager>().To<TagManager>();
            kernel.Bind<IWebConfigManager>().To<WebConfigManager>();
            kernel.Bind<AppUserManager>().ToSelf();
            kernel.Bind<AppRoleManager>().ToSelf();
            kernel.Bind<AppSignInManager>().ToSelf();
            kernel.Bind<IAuthenticationManager>().ToMethod(c => HttpContext.Current.GetOwinContext().Authentication).InRequestScope();
            kernel.Bind<IUrlizer>().To<Urlizer>();
            kernel.Bind<ILuceneSearchEngine>().To<LuceneSearchEngine>().InSingletonScope();
            kernel.Bind<IStatisticsProvider>().To<StatisticsProvider>();
            kernel.Bind<ISitemapGenerator>().To<SitemapGenerator>();
            kernel.Bind<IConstantsProvider>().To<WebConfigConstantsProvider>();
            kernel.Bind<ILoggingHelper>().To<NLogLoggingHelper>();
            kernel.Bind<ILogFilesManager>().To<LogFilesManager>();
            kernel.Bind<IBotDetector>().To<BotDetector>();

            kernel.Bind<ICategoryRepository>().To<CategoryRepository>();
            kernel.Bind<IPostRepository>().To<PostRepository>();
            kernel.Bind<ITagRepository>().To<TagRepository>();
            kernel.Bind<IUserRepositoryAsync>().To<UserRepositoryAsync>();
            kernel.Bind<IRoleRepositoryAsync>().To<RoleRepositoryAsync>();
        }

        #endregion
    }
}