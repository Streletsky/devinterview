﻿using System.Web.Mvc;
using System.Web.Routing;

namespace DevInterview.Web
{
    public class RouteConfig
    {
        #region Public Methods and Operators

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("GenericError",
                            "Error",
                            new
                            {
                                controller = "Error",
                                action = "GenericError"
                            });

            routes.MapRoute("Sitemap",
                            "sitemap.xml",
                            new
                            {
                                controller = "SitemapXml",
                                action = "Index"
                            });

            routes.MapRoute("Rss",
                            "RssFeed",
                            new
                            {
                                controller = "RssFeed",
                                action = "Index"
                            });

            routes.MapRoute("MostViewed",
                            "MostViewed",
                            new
                            {
                                controller = "Post",
                                action = "MostViewed"
                            });

            routes.MapRoute("TopRated",
                            "TopRated",
                            new
                            {
                                controller = "Post",
                                action = "TopRated"
                            });

            routes.MapRoute("About",
                            "About",
                            new
                            {
                                controller = "Home",
                                action = "About"
                            });

            routes.MapRoute("Search",
                            "Search",
                            new
                            {
                                controller = "Post",
                                action = "Search"
                            });

            routes.MapRoute("SearchPages",
                            "Search/{page}",
                            new
                            {
                                controller = "Post",
                                action = "Search"
                            },
                            new
                            {
                                page = @"\d+"
                            });

            routes.MapRoute("Post",
                            "p/{category}/{url}-{id}",
                            new
                            {
                                controller = "Post",
                                action = "GetById"
                            },
                            new
                            {
                                id = @"\d+"
                            });

            routes.MapRoute("Tag",
                            "t/{tag}",
                            new
                            {
                                controller = "Post",
                                action = "GetByTag"
                            });

            routes.MapRoute("TagPages",
                            "t/{tag}/{page}",
                            new
                            {
                                controller = "Post",
                                action = "GetByTag"
                            },
                            new
                            {
                                page = @"\d+"
                            });

            routes.MapRoute("Category",
                            "c/{category}",
                            new
                            {
                                controller = "Post",
                                action = "GetByCategory"
                            });

            routes.MapRoute("CategoryPages",
                            "c/{category}/{page}",
                            new
                            {
                                controller = "Post",
                                action = "GetByCategory"
                            },
                            new
                            {
                                page = @"\d+"
                            });

            routes.MapRoute("MainPages",
                            "p/{page}",
                            new
                            {
                                controller = "Post",
                                action = "Index"
                            },
                            new
                            {
                                page = @"\d+"
                            });

            routes.MapRoute("Default",
                            "{controller}/{action}/{id}",
                            new
                            {
                                controller = "Post",
                                action = "Index",
                                id = UrlParameter.Optional
                            });

            routes.MapRoute("404",
                            "{*url}",
                            new
                            {
                                controller = "Error",
                                action = "Error404"
                            });
        }

        #endregion
    }
}