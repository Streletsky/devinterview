﻿using System;
using DevInterview.Bal.Managers.Identity;
using DevInterview.Dal.Entities.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace DevInterview.Web
{
    public partial class Startup
    {
        #region Public Methods and Operators

        public void ConfigureAuth(IAppBuilder app)
        {
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login"),
                Provider =
                    new CookieAuthenticationProvider
                    {
                        OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<AppUserManager, AppUser, int>(
                                                                                                                     TimeSpan.FromMinutes(30),
                                                                                                                     (manager, user) => manager.CreateIdentityAsync(user,
                                                                                                                                                                    DefaultAuthenticationTypes
                                                                                                                                                                        .ApplicationCookie),
                                                                                                                     id => id.GetUserId<int>()
                                                                                                                    )
                    }
            });
        }

        #endregion
    }
}