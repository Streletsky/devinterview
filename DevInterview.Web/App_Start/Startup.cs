﻿using DevInterview.Web;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace DevInterview.Web
{
    public partial class Startup
    {
        #region Public Methods and Operators

        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }

        #endregion
    }
}