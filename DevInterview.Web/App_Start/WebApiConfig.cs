﻿using System.Web.Http;
using DevInterview.Web.Infrastructure.Filters;

namespace DevInterview.Web
{
    public static class WebApiConfig
    {
        #region Public Methods and Operators

        public static void Register(HttpConfiguration config)
        {
            config.Routes.MapHttpRoute("DefaultApi",
                                       "api/{controller}/{id}",
                                       new
                                       {
                                           id = RouteParameter.Optional
                                       });

            config.MapHttpAttributeRoutes();

            config.Filters.Add(new WebApiErrorHandlerFilterAttribute());
        }

        #endregion
    }
}