﻿using System.Web.Mvc;

namespace DevInterview.Web.Areas.ControlPanel
{
    public class ControlPanelAreaRegistration : AreaRegistration
    {
        #region Public Properties

        public override string AreaName => "ControlPanel";

        #endregion


        #region Public Methods and Operators

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute("SimilarPosts",
                             "ControlPanel/Admin/SimilarPosts",
                             new
                             {
                                 controller = "Admin",
                                 action = "SimilarPosts",
                                 query = UrlParameter.Optional
                             });

            context.MapRoute("RefreshIndex",
                             "ControlPanel/Admin/RefreshIndex",
                             new
                             {
                                 controller = "Admin",
                                 action = "RefreshIndex",
                                 id = UrlParameter.Optional
                             });

            context.MapRoute("AngularCatchAllRoute",
                             "ControlPanel/Admin/{*.}",
                             new
                             {
                                 controller = "Admin",
                                 action = "Index",
                                 id = UrlParameter.Optional
                             });

            context.MapRoute("ControlPanel",
                             "ControlPanel/{controller}/{action}/{id}",
                             new
                             {
                                 action = "Index",
                                 id = UrlParameter.Optional
                             });
        }

        #endregion
    }
}