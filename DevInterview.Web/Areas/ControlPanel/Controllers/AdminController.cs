﻿using System.Linq;
using System.Web.Mvc;
using DevInterview.Bal.Managers.Business.Interfaces;
using DevInterview.Bal.SearchEngine.Base;
using DevInterview.Common.Configuration;
using DevInterview.Common.Utilities.Extensions;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Web.Controllers.Base;

namespace DevInterview.Web.Areas.ControlPanel.Controllers
{
    [Authorize]
    public class AdminController : AppBaseController
    {
        #region Fields

        private readonly IPostManager postManager;

        private readonly ILuceneSearchEngine searchEngine;

        #endregion


        #region Constructors and Destructors

        public AdminController(IPostManager postManager, ILuceneSearchEngine searchEngine)
        {
            this.postManager = postManager;
            this.searchEngine = searchEngine;
        }

        #endregion


        #region Public Methods and Operators

        public ViewResult Index() => View();

        public void RefreshIndex()
        {
            SearchResult<Post> posts = postManager.GetAll();
            searchEngine.ClearIndex();
            searchEngine.AddPostToIndex(posts.Data);
        }

        public JsonResult SimilarPosts(string query)
        {
            if (query.IsNullOrEmpty())
            {
                return new JsonResult();
            }

            return new JsonResult
            {
                Data = postManager.SearchSimilar(query, AppCfgReader.SimilarPostsSize).Data.Select(p => p.Title),
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        #endregion
    }
}