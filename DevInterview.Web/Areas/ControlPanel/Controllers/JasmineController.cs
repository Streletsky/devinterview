using System.Web.Mvc;

namespace DevInterview.Web.Areas.ControlPanel.Controllers
{
    [Authorize]
    public class JasmineController : Controller
    {
        #region Public Methods and Operators

        public ViewResult Run() => View("SpecRunner");

        #endregion
    }
}