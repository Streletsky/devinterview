﻿using System;
using System.Data.Entity.Core;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using DevInterview.Bal.Managers.Business.Interfaces;
using DevInterview.Common.Exceptions;
using DevInterview.Common.Exceptions.Presentation;
using DevInterview.Common.Utilities.Extensions;
using DevInterview.Common.Utilities.Helpers;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Web.Controllers.Base;
using DevInterview.Web.Models;

namespace DevInterview.Web.Areas.ControlPanel.Controllers.WebApi
{
    public class CategoriesController : WebApiBaseController
    {
        #region Fields

        private readonly ICategoryManager categoryManager;

        private readonly IUrlizer urlizer;

        #endregion


        #region Constructors and Destructors

        public CategoriesController(ICategoryManager categoryManager, IUrlizer urlizer)
        {
            this.categoryManager = categoryManager;
            this.urlizer = urlizer;
        }

        #endregion


        #region Public Methods and Operators

        [HttpDelete]
        public void Delete(int id)
        {
            if (id <= 0)
            {
                var inner = new ArgumentException("ID of Category should be positive.", nameof(id));

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not delete Category.",
                                                    TypeOfException = inner.GetType(),
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                },
                                                inner);
            }

            categoryManager.Delete(id);
        }

        [HttpGet]
        public SearchResult<CategoryModel> Get()
        {
            SearchResult<Category> categories = categoryManager.GetAll();

            if (categories == null || categories.TotalRecords == 0 || categories.Data == null)
            {
                return new SearchResult<CategoryModel>();
            }

            var results = new SearchResult<CategoryModel>
            {
                TotalRecords = categories.TotalRecords,
                Records = categories.Records,
                Data = categories.Data.Select(Mapper.Map<CategoryModel>)
            };

            return results;
        }

        [HttpPost]
        public CategoryModel Post(CategoryModel model)
        {
            if (model == null)
            {
                var inner = new ArgumentNullException(nameof(model), "Model can not be null.");

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not create Category.",
                                                    TypeOfException = inner.GetType(),
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                },
                                                inner);
            }

            var category = new Category();
            Mapper.Map(model, category);

            if (category.UrlSlug.IsNullOrEmpty())
            {
                category.UrlSlug = urlizer.GetFriendlyUrl(category.Title);
            }

            Category result = categoryManager.Create(category);
            model.Id = result.Id;

            return model;
        }

        [HttpPut]
        public CategoryModel Put(CategoryModel model)
        {
            if (model == null || model.Id <= 0)
            {
                ArgumentException inner = model == null
                                              ? new ArgumentNullException(nameof(model), "Model can not be null.")
                                              : new ArgumentException("ID of model should be positive.", nameof(model));

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not update Category.",
                                                    TypeOfException = inner.GetType(),
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                },
                                                inner);
            }

            Category category = categoryManager.Get(model.Id);

            if (category == null)
            {
                var inner = new ObjectNotFoundException($"Category with ID {model.Id} was not found in storage.");

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not update Category.",
                                                    TypeOfException = inner.GetType(),
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                },
                                                inner);
            }

            Mapper.Map(model, category);

            if (category.UrlSlug.IsNullOrEmpty())
            {
                category.UrlSlug = urlizer.GetFriendlyUrl(category.Title);
            }

            categoryManager.Update(category);

            return model;
        }

        #endregion
    }
}