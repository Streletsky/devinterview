﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using DevInterview.Common.Configuration.Interfaces;
using DevInterview.Common.Exceptions;
using DevInterview.Common.Exceptions.Presentation;
using DevInterview.Common.Utilities.Extensions;
using DevInterview.Web.Controllers.Base;

namespace DevInterview.Web.Areas.ControlPanel.Controllers.WebApi
{
    public class ConfigurationController : WebApiBaseController
    {
        #region Fields

        private readonly IWebConfigManager webConfigManager;

        #endregion


        #region Constructors and Destructors

        public ConfigurationController(IWebConfigManager webConfigManager)
        {
            this.webConfigManager = webConfigManager;
        }

        #endregion


        #region Public Methods and Operators

        [HttpGet]
        public List<KeyValuePair<string, string>> Get() => webConfigManager.GetAllAppSettings();

        [HttpPost]
        public void Post([FromBody] KeyValuePair<string, string> cfg)
        {
            if (cfg.Key.IsNullOrEmpty())
            {
                var inner = new ArgumentException("cfg Key should not be null", nameof(cfg));

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not save web.config.",
                                                    TypeOfException = inner.GetType(),
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                },
                                                inner);
            }

            webConfigManager.SaveAppSettingEntry(cfg.Key, cfg.Value);
        }

        #endregion
    }
}