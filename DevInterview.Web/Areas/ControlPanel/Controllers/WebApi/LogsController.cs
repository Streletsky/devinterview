﻿using System.Collections.Generic;
using System.Web.Http;
using DevInterview.Bal.Managers.Logging.Interfaces;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Web.Controllers.Base;

namespace DevInterview.Web.Areas.ControlPanel.Controllers.WebApi
{
    public class LogsController : WebApiBaseController
    {
        #region Fields

        private readonly ILogFilesManager logFileManager;

        #endregion


        #region Constructors and Destructors

        public LogsController(ILogFilesManager logFileManager)
        {
            this.logFileManager = logFileManager;
        }

        #endregion


        #region Public Methods and Operators

        [HttpPost]
        public void Delete([FromUri] string fileName)
        {
            logFileManager.Delete(fileName);
        }

        [HttpGet]
        public IEnumerable<LogFile> Get() => logFileManager.GetAll();

        #endregion
    }
}