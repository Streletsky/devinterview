﻿using System;
using System.Data.Entity.Core;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using DevInterview.Bal.Managers.Business.Interfaces;
using DevInterview.Bal.SearchEngine.Base;
using DevInterview.Common.Exceptions;
using DevInterview.Common.Exceptions.Presentation;
using DevInterview.Common.Utilities.Helpers;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Web.Controllers.Base;
using DevInterview.Web.Models;

namespace DevInterview.Web.Areas.ControlPanel.Controllers.WebApi
{
    public class PostsController : WebApiBaseController
    {
        #region Fields

        private readonly IPostManager postManager;

        private readonly ILuceneSearchEngine searchEngine;

        private readonly IUrlizer urlizer;

        #endregion


        #region Constructors and Destructors

        public PostsController(IPostManager postManager, IUrlizer urlizer, ILuceneSearchEngine searchEngine)
        {
            this.postManager = postManager;
            this.urlizer = urlizer;
            this.searchEngine = searchEngine;
        }

        #endregion


        #region Public Methods and Operators

        [HttpDelete]
        public void Delete(int id)
        {
            if (id <= 0)
            {
                var inner = new ArgumentException("ID of Post should be positive.", nameof(id));

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not delete Post.",
                                                    TypeOfException = inner.GetType(),
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                },
                                                inner);
            }

            postManager.Delete(id);
        }

        [HttpGet]
        public PostModel Get(int id)
        {
            if (id <= 0)
            {
                var inner = new ArgumentException("ID of Post should be positive.", "model");

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not get Post by ID.",
                                                    TypeOfException = inner.GetType(),
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                },
                                                inner);
            }

            Post post = postManager.Get(id);

            if (post == null)
            {
                var inner = new ObjectNotFoundException($"Post with ID {id} was not found.");

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not get Post from storage.",
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException,
                                                    TypeOfException = inner.GetType()
                                                },
                                                inner);
            }

            var model = Mapper.Map<PostModel>(post);

            return model;
        }

        [HttpGet]
        public SearchResult<PostModel> Get([FromUri] SearchParameters parameters)
        {
            SearchResult<Post> posts = postManager.Search(parameters);

            if (posts == null || posts.TotalRecords == 0 || posts.Data == null)
            {
                return new SearchResult<PostModel>();
            }

            var results = new SearchResult<PostModel>
            {
                TotalRecords = posts.TotalRecords,
                Records = posts.Records,
                Data = posts.Data.Select(Mapper.Map<PostModel>).OrderByDescending(p => p.Created)
            };

            return results;
        }

        [HttpPost]
        public PostModel Post(PostModel model)
        {
            if (model == null)
            {
                var inner = new ArgumentNullException(nameof(model), "Model can not be null.");

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not create Post.",
                                                    TypeOfException = inner.GetType(),
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                },
                                                inner);
            }

            var post = new Post();
            Mapper.Map(model, post);
            post.UrlSlug = urlizer.GetFriendlyUrl(post.Title);

            foreach (Tag tag in post.Tags)
            {
                tag.UrlSlug = urlizer.GetFriendlyUrl(tag.Title);
            }

            Post result = postManager.Create(post);
            model.Id = result.Id;

            return model;
        }

        [HttpPut]
        public PostModel Put(PostModel model)
        {
            if (model == null || model.Id <= 0)
            {
                ArgumentException inner = model == null
                                              ? new ArgumentNullException(nameof(model), "Model can not be null.")
                                              : new ArgumentException("ID of model should be positive.", nameof(model));

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not update Post.",
                                                    TypeOfException = inner.GetType(),
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                },
                                                inner);
            }

            Post post = postManager.Get(model.Id);

            if (post == null)
            {
                var inner = new ObjectNotFoundException($"Post with ID {model.Id} was not found in storage.");

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not update Post.",
                                                    TypeOfException = inner.GetType(),
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                },
                                                inner);
            }

            Mapper.Map(model, post);
            post.UrlSlug = urlizer.GetFriendlyUrl(post.Title);

            foreach (Tag tag in post.Tags)
            {
                tag.UrlSlug = urlizer.GetFriendlyUrl(tag.Title);
            }

            post = postManager.Update(post);

            searchEngine.AddPostToIndex(post);

            return model;
        }

        #endregion
    }
}