﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using DevInterview.Bal.Managers.Identity;
using DevInterview.Web.Controllers.Base;
using DevInterview.Web.Models;

namespace DevInterview.Web.Areas.ControlPanel.Controllers.WebApi
{
    public class RolesController : WebApiBaseController
    {
        #region Fields

        private readonly AppRoleManager rolesManager;

        #endregion


        #region Constructors and Destructors

        public RolesController(AppRoleManager rolesManager)
        {
            this.rolesManager = rolesManager;
        }

        #endregion


        #region Public Methods and Operators

        [HttpGet]
        public IEnumerable<AppRoleModel> Get() => rolesManager.Roles.Select(Mapper.Map<AppRoleModel>);

        #endregion
    }
}