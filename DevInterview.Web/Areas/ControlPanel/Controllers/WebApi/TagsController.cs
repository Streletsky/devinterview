﻿using System.Linq;
using System.Web.Http;
using AutoMapper;
using DevInterview.Bal.Managers.Business.Interfaces;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Web.Controllers.Base;
using DevInterview.Web.Models;

namespace DevInterview.Web.Areas.ControlPanel.Controllers.WebApi
{
    public class TagsController : WebApiBaseController
    {
        #region Fields

        private readonly ITagManager tagManager;

        #endregion


        #region Constructors and Destructors

        public TagsController(ITagManager tagManager)
        {
            this.tagManager = tagManager;
        }

        #endregion


        #region Public Methods and Operators

        [HttpGet]
        public SearchResult<TagModel> Search([FromUri] SearchParameters parameters)
        {
            SearchResult<Tag> tags = tagManager.Search(parameters);

            if (tags == null || tags.TotalRecords == 0 || tags.Data == null)
            {
                return new SearchResult<TagModel>();
            }

            var results = new SearchResult<TagModel>
            {
                TotalRecords = tags.TotalRecords,
                Records = tags.Records,
                Data = tags.Data.Select(Mapper.Map<TagModel>)
            };

            return results;
        }

        #endregion
    }
}