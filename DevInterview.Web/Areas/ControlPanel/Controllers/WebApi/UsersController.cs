﻿using System;
using System.Data.Entity.Core;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using DevInterview.Bal.Managers.Identity;
using DevInterview.Common.Exceptions;
using DevInterview.Common.Exceptions.Dal;
using DevInterview.Common.Exceptions.Presentation;
using DevInterview.Dal.Entities.Identity;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Web.Controllers.Base;
using DevInterview.Web.Models;
using Microsoft.AspNet.Identity;

namespace DevInterview.Web.Areas.ControlPanel.Controllers.WebApi
{
    public class UsersController : WebApiBaseController
    {
        #region Fields

        private readonly AppRoleManager roleManager;

        private readonly AppUserManager userManager;

        #endregion


        #region Constructors and Destructors

        public UsersController(AppUserManager userManager, AppRoleManager roleManager)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        #endregion


        #region Public Methods and Operators

        [HttpDelete]
        public async Task Delete(int id)
        {
            if (id <= 0)
            {
                var inner = new ArgumentException("ID of AppUser should be positive.", nameof(id));

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not delete AppUser.",
                                                    TypeOfException = inner.GetType(),
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                },
                                                inner);
            }

            AppUser user = await userManager.FindByIdAsync(id);

            if (user == null)
            {
                var inner = new ObjectNotFoundException($"AppUser with ID {id} was not found in storage.");

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not delete AppUser.",
                                                    TypeOfException = inner.GetType(),
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                },
                                                inner);
            }

            IdentityResult updateResult = await userManager.DeleteAsync(user);
            if (!updateResult.Succeeded)
            {
                var inner = new DataAccessException(string.Join("\r\n", updateResult.Errors));

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not delete AppUser.",
                                                    TypeOfException = inner.GetType(),
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                },
                                                inner);
            }
        }

        [HttpGet]
        public async Task<SearchResult<AppUserModel>> Get()
        {
            SearchResult<AppUser> users = await userManager.GetAllAsync();
            if (users == null)
            {
                return new SearchResult<AppUserModel>();
            }

            var results = new SearchResult<AppUserModel>
            {
                TotalRecords = users.TotalRecords,
                Records = users.Records,
                Data = users.Data.Select(Mapper.Map<AppUserModel>).ToList()
            };

            foreach (AppUser user in users.Data)
            {
                if (user.Roles.Any())
                {
                    AppUserModel result = results.Data.First(r => r.Id == user.Id);
                    result.RoleId = user.Roles.First().RoleId;
                }
            }

            return results;
        }

        [HttpPut]
        public async Task<AppUserModel> Put(AppUserModel model)
        {
            if (model == null || model.Id <= 0)
            {
                ArgumentException inner = model == null
                                              ? new ArgumentNullException(nameof(model), "Model can not be null.")
                                              : new ArgumentException("ID of model should be positive.", nameof(model));

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not update AppUser.",
                                                    TypeOfException = inner.GetType(),
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                },
                                                inner);
            }

            AppUser user = await userManager.FindByIdAsync(model.Id);

            if (user == null)
            {
                var inner = new ObjectNotFoundException($"AppUser with ID {model.Id} was not found in storage.");

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not update AppUser.",
                                                    TypeOfException = inner.GetType(),
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                },
                                                inner);
            }

            Mapper.Map(model, user);

            if (model.RoleId > 0)
            {
                AppRole role = await roleManager.FindByIdAsync(model.RoleId);
                if (user.Roles.All(r => r.RoleId != role.Id))
                {
                    user.Roles.Clear();

                    await userManager.AddToRoleAsync(user.Id, role.Name);
                }
            }

            IdentityResult updateResult = await userManager.UpdateAsync(user);
            if (!updateResult.Succeeded)
            {
                var inner = new DataAccessException(string.Join("\r\n", updateResult.Errors));

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not update AppUser.",
                                                    TypeOfException = inner.GetType(),
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                },
                                                inner);
            }

            return model;
        }

        #endregion
    }
}