﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using DevInterview.Bal.Managers.Identity;
using DevInterview.Common.Enums;
using DevInterview.Common.Exceptions;
using DevInterview.Common.Exceptions.Presentation;
using DevInterview.Dal.Entities.Identity;
using DevInterview.Web.Controllers.Base;
using DevInterview.Web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace DevInterview.Web.Controllers
{
    [Authorize]
    public class AccountController : AppBaseController
    {
        #region Fields

        private readonly AppUserManager appUserManager;

        private readonly AppSignInManager signInManager;

        #endregion


        #region Constructors and Destructors

        public AccountController(AppUserManager appUserManager, AppSignInManager signInManager)
        {
            this.appUserManager = appUserManager;
            this.signInManager = signInManager;
        }

        #endregion


        #region Public Methods and Operators

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginModel model, string returnUrl)
        {
            if (model == null)
            {
                var inner = new ArgumentNullException(nameof(model), "Model can not be null.");

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not login to app.",
                                                    TypeOfException = inner.GetType(),
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                },
                                                inner);
            }

            if (!ModelState.IsValid)
            {
                return View(model);
            }

            SignInStatus result = await signInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);

            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return View(model);
            }
        }

        public ActionResult LogOff()
        {
            signInManager.AuthenticationManager.SignOut();

            return RedirectToAction("Index", "Post");
        }

        [NonAction]
        public ActionResult Register() => View();

        [HttpPost]
        [NonAction]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterModel model)
        {
            if (model == null)
            {
                var inner = new ArgumentNullException(nameof(model), "Model can not be null.");

                throw new PresentationException(
                                                new ExceptionMessage
                                                {
                                                    FailedOperationDescription = "Can not login to app.",
                                                    TypeOfException = inner.GetType(),
                                                    PossibleReason = ExceptionHelper.ReferenceToInnerException
                                                },
                                                inner);
            }

            if (ModelState.IsValid)
            {
                var user = new AppUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    IsActive = true,
                    Created = DateTime.Now
                };

                IdentityResult result = await appUserManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await appUserManager.AddToRoleAsync(user.Id, Roles.Common.ToString());

                    await signInManager.SignInAsync(user, false, false);

                    return RedirectToAction("Index", "Post");
                }

                AddErrors(result);
            }

            return View(model);
        }

        #endregion


        #region Methods

        private void AddErrors(IdentityResult result)
        {
            foreach (string error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return RedirectToAction("Index", "Post");
        }

        #endregion
    }
}