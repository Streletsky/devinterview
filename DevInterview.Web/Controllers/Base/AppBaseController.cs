﻿using System;
using System.Web;
using System.Web.Mvc;
using DevInterview.Common.Logging;

namespace DevInterview.Web.Controllers.Base
{
    public abstract class AppBaseController : Controller
    {
        #region Methods

        /// <summary>
        ///     Called when an unhandled exception occurs in the action.
        /// </summary>
        /// <param name="filterContext">Information about the current request and action.</param>
        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled)
            {
                return;
            }

            Exception ex = filterContext.Exception;
            var serverEx = ex as HttpException;
            NLogSingleton.Instance.Error(ex, "Exception at controller level.");

            if (ex.Message == "Page Not Found" || serverEx != null && serverEx.ErrorCode == 404)
            {
                return;
            }

            filterContext.ExceptionHandled = true;
            Server.ClearError();
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.StatusCode = 500;
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;

            // If the request is AJAX return JSON, else - View.
            if (filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                filterContext.Result = new JsonResult
                {
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                    Data = new
                    {
                        error = true,
                        message = filterContext.Exception.Message
                    }
                };
            }
            else
            {
                var controllerName = (string)filterContext.RouteData.Values["controller"];
                var actionName = (string)filterContext.RouteData.Values["action"];
                var model = new HandleErrorInfo(filterContext.Exception, controllerName, actionName);

                filterContext.Result = new ViewResult
                {
                    ViewName = "GenericError",
                    ViewData = new ViewDataDictionary(model),
                    TempData = filterContext.Controller.TempData
                };
            }

            NLogSingleton.Instance.Info("Exception at controller level handled successfully.");

            base.OnException(filterContext);
        }

        #endregion
    }
}