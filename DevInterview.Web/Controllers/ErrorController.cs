﻿using System.Web.Mvc;
using DevInterview.Common.Configuration;
using DevInterview.Common.Logging;

namespace DevInterview.Web.Controllers
{
    public class ErrorController : Controller
    {
        #region Public Methods and Operators

        public ActionResult Error404()
        {
            ViewBag.MetaTitle = $"Error 404 — {AppCfgReader.DomainName}";
            ViewBag.MetaDescription = $"Error 404 — {AppCfgReader.DomainName}";
            Response.StatusCode = 404;
            Response.TrySkipIisCustomErrors = true;

            NLogSingleton.Instance.Info("Exception 404 handled successfully at controller level.");

            return View();
        }

        public ActionResult GenericError()
        {
            ViewBag.MetaTitle = $"Server error — {AppCfgReader.DomainName}";
            ViewBag.MetaDescription = $"Server error — {AppCfgReader.DomainName}";
            Response.StatusCode = 500;

            return View();
        }

        #endregion
    }
}