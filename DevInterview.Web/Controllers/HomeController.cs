﻿using System.Web.Mvc;
using DevInterview.Web.Controllers.Base;

namespace DevInterview.Web.Controllers
{
    public class HomeController : AppBaseController
    {
        #region Public Methods and Operators

        public ActionResult About() => View();

        #endregion
    }
}