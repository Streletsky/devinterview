using System.Web.Mvc;

namespace DevInterview.Web.Controllers
{
	public class JasmineController : Controller
	{
		#region Public Methods and Operators

		public ViewResult Run()
		{
			return View("SpecRunner");
		}

		#endregion
	}
}