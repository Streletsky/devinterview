﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using DevInterview.Bal.Managers.Business.Interfaces;
using DevInterview.Common.Configuration.Interfaces;
using DevInterview.Common.Utilities.Extensions;
using DevInterview.Common.Utilities.Helpers;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Web.Controllers.Base;
using DevInterview.Web.Infrastructure.RazorExtensions;
using DevInterview.Web.Models;

namespace DevInterview.Web.Controllers
{
    // TODO: encapsulate cookies logic
    public class PostController : AppBaseController
    {
        #region Fields

        private readonly IBotDetector botDetector;

        private readonly ICategoryManager categoryManager;

        private readonly IConstantsProvider constantsProvider;

        private readonly IPostManager postManager;

        private readonly ITagManager tagManager;

        #endregion


        #region Constructors and Destructors

        public PostController(IPostManager postManager, ICategoryManager categoryManager, ITagManager tagManager, IConstantsProvider constantsProvider, IBotDetector botDetector)
        {
            this.postManager = postManager;
            this.categoryManager = categoryManager;
            this.tagManager = tagManager;
            this.constantsProvider = constantsProvider;
            this.botDetector = botDetector;
        }

        #endregion


        #region Public Methods and Operators

        public ActionResult GetByCategory(string category, int? page = null)
        {
            if (string.IsNullOrEmpty(category))
            {
                throw new HttpException(404, "Page Not Found");
            }

            SearchResult<Category> cats = categoryManager.Search(new SearchParameters
            {
                Count = 1,
                Page = 1,
                SearchBy = "UrlSlug",
                SearchQuery = category
            });

            if (cats.Records > 1)
            {
                throw new HttpException(404, "Page Not Found");
            }

            if (cats.Records == 0)
            {
                throw new HttpException(404, "Page Not Found");
            }

            Category cat = cats.Data.First();

            SearchResult<Post> result = postManager.GetPostsByCategory(cat.Id, page ?? 1);

            SearchResult<PostModel> posts = MapSearchResultToModel(result);

            var pagesRouteName = "CategoryPages";
            string url = page == null || page == 1
                             ? Url.AbsoluteRouteUrl("Category",
                                                    new
                                                    {
                                                        category = cat.UrlSlug
                                                    })
                             : Url.AbsoluteRouteUrl(pagesRouteName,
                                                    new
                                                    {
                                                        category = cat.UrlSlug,
                                                        page
                                                    });

            ViewBag.PageTitle = $"{cat.Title} questions and answers";
            ViewBag.PageDescription = cat.Description;
            ViewBag.MetaTitle = $"{cat.Title} questions and answers";
            ViewBag.MetaDescription = cat.Description;
            ViewBag.CanonicalUrl = url;
            ViewBag.Page = page ?? 1;
            ViewBag.TotalPages = Math.Ceiling((double)result.TotalRecords / constantsProvider.PageSize);
            ViewBag.PagerRouteName = pagesRouteName;

            return View("Index", posts);
        }

        public ActionResult GetById(int id)
        {
            if (id <= 0)
            {
                throw new HttpException(404, "Page Not Found");
            }

            Post post = postManager.Get(id);

            if (post == null)
            {
                throw new HttpException(404, "Page Not Found");
            }

            var model = Mapper.Map<PostModel>(post);
            model.IsFullPost = true;

            bool isCrawler = botDetector.CheckUserAgentForBot(Request.UserAgent);
            if (!isCrawler)
            {
                HttpCookie cookie = Request.Cookies.Get(constantsProvider.CookieName);
                if (cookie == null)
                {
                    cookie = new HttpCookie(constantsProvider.CookieName);
                    cookie.Values.Set(constantsProvider.CookieViewedPostsKey, id.ToString());
                    cookie.Expires = DateTime.Now.AddDays(constantsProvider.CookieLifeTime);
                    Response.Cookies.Add(cookie);

                    post.Views += 1;
                    postManager.Update(post);
                }
                else
                {
                    string viewedPosts = cookie.Values.Get(constantsProvider.CookieViewedPostsKey);
                    List<string> viewedPostsList = viewedPosts.IsNullOrEmpty()
                                                       ? new List<string>()
                                                       : viewedPosts.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                    if (!viewedPostsList.Contains(id.ToString()))
                    {
                        viewedPostsList.Add(id.ToString());
                        cookie.Values.Set(constantsProvider.CookieViewedPostsKey, string.Join(",", viewedPostsList));
                        cookie.Expires = DateTime.Now.AddDays(constantsProvider.CookieLifeTime);
                        Response.Cookies.Add(cookie);

                        post.Views += 1;
                        postManager.Update(post);
                    }
                }
            }

            ViewBag.MetaTitle = post.Title;
            ViewBag.MetaDescription = post.Description;
            ViewBag.CanonicalUrl = Url.AbsoluteRouteUrl("Post",
                                                        new
                                                        {
                                                            id = post.Id,
                                                            category = post.Category.UrlSlug,
                                                            url = post.UrlSlug
                                                        });

            return View(model);
        }

        public ActionResult GetByTag(string tag, int? page = null)
        {
            if (string.IsNullOrEmpty(tag))
            {
                throw new HttpException(404, "Page Not Found");
            }

            SearchResult<Tag> tags = tagManager.Search(new SearchParameters
            {
                Count = 1,
                Page = 1,
                SearchBy = "UrlSlug",
                SearchQuery = tag
            });

            if (tags.Records > 1)
            {
                throw new HttpException(404, "Page Not Found");
            }

            if (tags.Records == 0)
            {
                throw new HttpException(404, "Page Not Found");
            }

            Tag tagFound = tags.Data.First();

            SearchResult<Post> result = postManager.GetPostsByTag(tagFound.Id, page ?? 1);

            SearchResult<PostModel> posts = MapSearchResultToModel(result);

            var pagesRouteName = "TagPages";
            string url = page == null || page == 1
                             ? Url.AbsoluteRouteUrl("Tag",
                                                    new
                                                    {
                                                        tag = tagFound.UrlSlug
                                                    })
                             : Url.AbsoluteRouteUrl(pagesRouteName,
                                                    new
                                                    {
                                                        tag = tagFound.UrlSlug,
                                                        page
                                                    });

            ViewBag.PageTitle = $"Posts tagged with \"{tagFound.Title}\" tag";
            ViewBag.MetaTitle = $"Posts tagged with \"{tagFound.Title}\" tag";
            ViewBag.MetaDescription = $"Interview questions tagged with \"{tagFound.Title}\" tag";
            ViewBag.CanonicalUrl = url;
            ViewBag.Page = page ?? 1;
            ViewBag.TotalPages = Math.Ceiling((double)result.TotalRecords / constantsProvider.PageSize);
            ViewBag.PagerRouteName = "TagPages";

            return View("Index", posts);
        }

        public ActionResult Index(int? page = null)
        {
            SearchResult<Post> result = postManager.Search(new SearchParameters
            {
                Count = constantsProvider.PageSize,
                Page = page ?? 1,
                OrderBy = "Created",
                OrderDirection = "desc"
            });

            if (result.Records == 0)
            {
                throw new HttpException(404, "Page Not Found");
            }

            SearchResult<PostModel> posts = MapSearchResultToModel(result);

            var pagesRouteName = "MainPages";
            string url = page == null || page == 1
                             ? Url.AbsoluteRouteUrl("Default")
                             : Url.AbsoluteRouteUrl(pagesRouteName,
                                                    new
                                                    {
                                                        page
                                                    });

            ViewBag.MetaTitle = $"Software developer interview Q&A — {constantsProvider.DomainName}";
            ViewBag.MetaDescription = "Check out the list of most common interview questions for software developer position, prepare yourself for getting a new job!";
            ViewBag.CanonicalUrl = url;
            ViewBag.PageTitle = "A comprehensive list of technical interview questions and answers for software developers";
            ViewBag.Page = page ?? 1;
            ViewBag.TotalPages = Math.Ceiling((double)result.TotalRecords / constantsProvider.PageSize);
            ViewBag.PagerRouteName = pagesRouteName;

            return View(posts);
        }

        public ActionResult MostViewed()
        {
            SearchResult<Post> result = postManager.GetMostViewedPosts();

            SearchResult<PostModel> posts = MapSearchResultToModel(result);

            ViewBag.MetaTitle = $"Most viewed interview questions at {constantsProvider.DomainName}";
            ViewBag.MetaDescription = $"Most viewed interview questions for software developer position at {constantsProvider.DomainName}";
            ViewBag.CanonicalUrl = Url.AbsoluteRouteUrl("MostViewed");
            ViewBag.PageTitle = "Most viewed interview questions for software developer";

            return View("Index", posts);
        }

        public ActionResult Search(PostsQueryModel query = null, int? page = null)
        {
            ViewBag.MetaTitle = "Search questions and answers";
            ViewBag.MetaDescription = $"Search software developer interview questions and answers at {constantsProvider.DomainName}";
            ViewBag.CanonicalUrl = Url.AbsoluteRouteUrl("Search");
            ViewBag.PageTitle = "Search questions and answers";

            if (query == null)
            {
                return View();
            }

            SearchResult<Post> result;
            if (query.Text.IsNullOrEmpty())
            {
                result = postManager.GetAll();
            }
            else
            {
                result = postManager.SearchSimilar(query.Text, int.MaxValue);
            }

            if (!query.OrderBy.IsNullOrEmpty())
            {
                result.Data = result.Data.OrderBy(query.OrderBy + " descending").ToList();
            }

            if (query.Category != null && query.Category.Id > 0)
            {
                result.Data = result.Data.Where(p => p.Category.Id == query.Category.Id).ToList();
            }

            if (query.Tags != null && query.Tags.Count > 0)
            {
                result.Data = result.Data.Where(p => p.Tags.Any(t => query.Tags.Any(tt => tt.Title == t.Title))).ToList();
            }

            int totalRecords = result.Data.Count();
            result.Data = result.Data.Skip(((page ?? 1) - 1) * constantsProvider.PageSize).Take(constantsProvider.PageSize);
            int records = result.Data.Count();
            result.TotalRecords = totalRecords;
            result.Records = records > constantsProvider.PageSize ? constantsProvider.PageSize : records;

            ViewBag.PageTitle = "Search questions and answers";
            ViewBag.MetaTitle = "Search questions and answers";
            ViewBag.MetaDescription = $"Search questions and answers on {constantsProvider.DomainName}";
            ViewBag.CanonicalUrl = Request.Url;
            ViewBag.Page = page ?? 1;
            ViewBag.TotalPages = Math.Ceiling((double)result.TotalRecords / constantsProvider.PageSize);
            ViewBag.Query = query;
            ViewBag.PagerRouteName = "SearchPages";

            return View(MapSearchResultToModel(result));
        }

        public ActionResult SimilarPosts(int postId)
        {
            Post post = postManager.Get(postId);
            SearchResult<Post> result = postManager.SearchSimilar(post, constantsProvider.SimilarPostsSize + 1);
            result.Data = result.Data.Where(p => p.Id != postId);

            return View(MapSearchResultToModel(result));
        }

        public ActionResult TopRated()
        {
            SearchResult<Post> result = postManager.GetTopRatedPosts();

            SearchResult<PostModel> posts = MapSearchResultToModel(result);

            ViewBag.MetaTitle = $"Top rated interview questions at {constantsProvider.DomainName}";
            ViewBag.MetaDescription = $"Top rated interview questions for software developer position at {constantsProvider.DomainName}";
            ViewBag.CanonicalUrl = Url.AbsoluteRouteUrl("TopRated");
            ViewBag.PageTitle = "Top rated interview questions for software developer";

            return View("Index", posts);
        }

        public JsonResult VotePost(int id, bool voteUp)
        {
            Post post = postManager.Get(id);
            HttpCookie cookie = Request.Cookies.Get(constantsProvider.CookieName);
            if (cookie == null)
            {
                cookie = new HttpCookie(constantsProvider.CookieName);
                cookie.Values.Set(constantsProvider.CookieVotedPostsKey, id.ToString());
                cookie.Expires = DateTime.Now.AddDays(constantsProvider.CookieLifeTime);
                Response.Cookies.Add(cookie);

                if (voteUp)
                {
                    post.Rating.VotesUp += 1;
                }
                else
                {
                    post.Rating.VotesDown += 1;
                }

                postManager.Update(post);

                return new JsonResult
                {
                    Data = "ok",
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }

            string viewedPosts = cookie.Values.Get(constantsProvider.CookieVotedPostsKey);
            List<string> viewedPostsList = viewedPosts.IsNullOrEmpty() ? new List<string>() : viewedPosts.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            if (!viewedPostsList.Contains(id.ToString()))
            {
                viewedPostsList.Add(id.ToString());
                cookie.Values.Set(constantsProvider.CookieVotedPostsKey, string.Join(",", viewedPostsList));
                cookie.Expires = DateTime.Now.AddDays(constantsProvider.CookieLifeTime);
                Response.Cookies.Add(cookie);

                if (voteUp)
                {
                    post.Rating.VotesUp += 1;
                }
                else
                {
                    post.Rating.VotesDown += 1;
                }

                postManager.Update(post);

                return new JsonResult
                {
                    Data = "ok",
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }

            return new JsonResult
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        #endregion


        #region Methods

        private SearchResult<PostModel> MapSearchResultToModel(SearchResult<Post> result) => new SearchResult<PostModel>
        {
            Data = result.Data.Select(Mapper.Map<PostModel>),
            Records = result.Records,
            TotalRecords = result.TotalRecords
        };

        #endregion
    }
}