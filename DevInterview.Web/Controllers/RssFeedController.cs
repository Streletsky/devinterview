﻿using System;
using System.Collections.Generic;
using System.ServiceModel.Syndication;
using System.Text;
using System.Xml;
using DevInterview.Bal.Managers.Business;
using DevInterview.Common.Configuration;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Web.Controllers.Base;
using DevInterview.Web.Infrastructure.RazorExtensions;

namespace DevInterview.Web.Controllers
{
    public class RssFeedController : AppBaseController
    {
        #region Fields

        private readonly PostManager postManager;

        #endregion


        #region Constructors and Destructors

        public RssFeedController(PostManager postManager)
        {
            this.postManager = postManager;
        }

        #endregion


        #region Public Methods and Operators

        public void Index()
        {
            var feed = new SyndicationFeed($"{AppCfgReader.DomainName} - prepare yourself for getting a new job",
                                           "Cache of questions and answers for a software developer position.",
                                           new Uri($"http://{AppCfgReader.DomainName}/"))
            {
                Language = "en"
            };

            var items = new List<SyndicationItem>();
            SearchResult<Post> latestPosts = postManager.GetLatestPosts();
            foreach (Post post in latestPosts.Data)
            {
                var item = new SyndicationItem
                {
                    Title = new TextSyndicationContent(post.Title),
                    Content = new TextSyndicationContent(post.BodyStart),
                    PublishDate = post.Created
                };

                item.AddPermalink(new Uri(Url.AbsoluteRouteUrl("Post",
                                                               new
                                                               {
                                                                   id = post.Id,
                                                                   category = post.Category.UrlSlug,
                                                                   url = post.UrlSlug
                                                               })));
                items.Add(item);
            }

            feed.Items = items;

            var formatter = new Rss20FeedFormatter(feed);
            XmlWriter writer = XmlWriter.Create(Response.Output);
            Response.ContentEncoding = Encoding.UTF8;
            Response.ContentType = "application/rss+xml";

            formatter.WriteTo(writer);
            writer.Flush();
        }

        #endregion
    }
}