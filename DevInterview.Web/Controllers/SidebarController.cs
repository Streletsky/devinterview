﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using DevInterview.Bal.Managers.Business.Interfaces;
using DevInterview.Bal.Providers.Interfaces;
using DevInterview.Dal.Entities.Business;
using DevInterview.Dal.Entities.NotBusiness;
using DevInterview.Web.Controllers.Base;
using DevInterview.Web.Models;

namespace DevInterview.Web.Controllers
{
    public class SidebarController : AppBaseController
    {
        #region Fields

        private readonly ICategoryManager categoryManager;

        private readonly IPostManager postManager;

        private readonly IStatisticsProvider statisticsProvider;

        private readonly ITagManager tagManager;

        #endregion


        #region Constructors and Destructors

        public SidebarController(ICategoryManager categoryManager, ITagManager tagManager, IPostManager postManager, IStatisticsProvider statisticsProvider)
        {
            this.categoryManager = categoryManager;
            this.tagManager = tagManager;
            this.postManager = postManager;
            this.statisticsProvider = statisticsProvider;
        }

        #endregion


        #region Public Methods and Operators

        public ActionResult About()
        {
            var model = new StatisticsModel
            {
                VotesCount = statisticsProvider.VotesCount(),
                CategoriesCount = statisticsProvider.CategoriesCount(),
                PostsCount = statisticsProvider.PostsCount(),
                DaysSinceStart = statisticsProvider.DaysSinceStart(),
                TagsCount = statisticsProvider.TagsCount()
            };

            return View(model);
        }

        public ActionResult Categories()
        {
            SearchResult<Category> categories = categoryManager.GetAll();

            if (categories == null || categories.TotalRecords == 0 || categories.Data == null)
            {
                return View(new List<CategoryModel>());
            }

            List<CategoryModel> models = categories.Data.Select(Mapper.Map<CategoryModel>).ToList();

            foreach (CategoryModel cat in models)
            {
                cat.PostsCount = postManager.GetPostsCountByCategory(cat.Id);
            }

            return View(models);
        }

        public ActionResult Tags()
        {
            SearchResult<Tag> tags = tagManager.GetAll();
            if (tags == null || tags.TotalRecords == 0 || tags.Data == null)
            {
                return View(new List<TagModel>());
            }

            List<TagModel> models = tags.Data.Select(Mapper.Map<TagModel>).ToList();

            foreach (TagModel tag in models)
            {
                tag.PostsCount = postManager.GetTaggedPostsCountByTag(tag.Id);
            }

            return View(models.Where(t => t.PostsCount > 0).ToList());
        }

        #endregion
    }
}