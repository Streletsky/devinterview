﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Web.Mvc;
using DevInterview.Bal.Managers.Business.Interfaces;
using DevInterview.Common.Logging;
using DevInterview.Dal.Entities.Business;
using DevInterview.Web.Controllers.Base;
using DevInterview.Web.Infrastructure.RazorExtensions;
using DevInterview.Web.Infrastructure.SitemapGenerator;
using DevInterview.Web.Infrastructure.SitemapGenerator.Interfaces;

namespace DevInterview.Web.Controllers
{
    public class SitemapXmlController : AppBaseController
    {
        #region Fields

        private readonly ICategoryManager catManager;

        private readonly IPostManager postManager;

        private readonly ISitemapGenerator sitemapGenerator;

        private readonly ITagManager tagManager;

        #endregion


        #region Constructors and Destructors

        public SitemapXmlController(ISitemapGenerator sitemapGenerator, IPostManager postManager, ITagManager tagManager, ICategoryManager catManager)
        {
            this.sitemapGenerator = sitemapGenerator;
            this.postManager = postManager;
            this.tagManager = tagManager;
            this.catManager = catManager;
        }

        #endregion


        #region Public Methods and Operators

        public ActionResult Index()
        {
            NLogSingleton.Instance.Info($"sitemap.xml requested by UA: {Request.UserAgent}, IP: {Request.UserHostAddress}");

            IOrderedEnumerable<Post> posts = postManager.GetAll().Data.OrderBy(p => p.Created);
            IEnumerable<Category> cats = catManager.GetAll().Data;
            IEnumerable<Tag> tags = tagManager.GetAll().Data;
            DateTime lastModifiesDate = posts.Max(p => p.Created);

            sitemapGenerator.AddSitemapNode(Url.AbsoluteRouteUrl("Default"), 1.0, SitemapFrequency.Daily, lastModifiesDate);
            sitemapGenerator.AddSitemapNode(Url.AbsoluteRouteUrl("MostViewed"), 0.9, SitemapFrequency.Weekly);
            sitemapGenerator.AddSitemapNode(Url.AbsoluteRouteUrl("TopRated"), 0.9, SitemapFrequency.Weekly);
            sitemapGenerator.AddSitemapNode(Url.AbsoluteRouteUrl("About"), 0.7, SitemapFrequency.Weekly);
            sitemapGenerator.AddSitemapNode(Url.AbsoluteRouteUrl("Search"), 0.7, SitemapFrequency.Weekly);

            foreach (Category cat in cats)
            {
                sitemapGenerator.AddSitemapNode(Url.AbsoluteRouteUrl("Category",
                                                                     new
                                                                     {
                                                                         category = cat.UrlSlug
                                                                     }),
                                                0.9,
                                                SitemapFrequency.Daily);
            }

            foreach (Tag tag in tags)
            {
                sitemapGenerator.AddSitemapNode(Url.AbsoluteRouteUrl("Tag",
                                                                     new
                                                                     {
                                                                         tag = tag.UrlSlug
                                                                     }),
                                                0.9,
                                                SitemapFrequency.Daily);
            }

            foreach (Post post in posts)
            {
                sitemapGenerator.AddSitemapNode(Url.AbsoluteRouteUrl("Post",
                                                                     new
                                                                     {
                                                                         id = post.Id,
                                                                         category = post.Category.UrlSlug,
                                                                         url = post.UrlSlug
                                                                     }),
                                                0.8,
                                                SitemapFrequency.Weekly,
                                                post.LastUpdated);
            }

            string xml = sitemapGenerator.BuildSitemapDocument();

            return Content(xml, MediaTypeNames.Text.Xml, Encoding.UTF8);
        }

        #endregion
    }
}