﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GenericError.aspx.cs" Inherits="DevInterview.Web.GenericError" %>

<% Context.Response.StatusCode = 500; %>

<!DOCTYPE html>

<html lang="en">
<head>
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
	<meta content="Server error &mdash; developer-interview.com" name="description">
	<meta content="all" name="noindex">
	<title>Server error &mdash; developer-interview.com</title>
	<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Ubuntu+Mono">
	<link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700">
	<link rel="stylesheet" href="/Content/bootstrap.css">
	<link rel="stylesheet" href="/Content/Master.css">
</head>
<body itemtype="http://schema.org/WebPage" itemscope="">
<meta content="Server error &mdash; developer-interview.com" itemprop="name">
<meta content="Server error &mdash; developer-interview.com" itemprop="description">
<header>
	<nav itemtype="http://schema.org/SiteNavigationElement" itemscope="" role="navigation" class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<a itemprop="url" href="/" class="navbar-brand">Dev.Interview</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li>
						<a title="Top Rated" itemprop="url" class="btn btn-link" href="/TopRated">
							<span aria-hidden="true" class="glyphicon glyphicon-king"></span>&nbsp;<span itemprop="name" class="">Top Rated</span>
						</a>
					</li>
					<li>
						<a title="Most Viewed" itemprop="url" class="btn btn-link" href="/MostViewed">
							<span aria-hidden="true" class="glyphicon glyphicon-fire"></span>&nbsp;<span itemprop="name" class="">Most Viewed</span>
						</a>
					</li>
					<li>
						<a title="RSS" itemprop="url" class="btn btn-link" href="/RssFeed">
							<span aria-hidden="true" class="glyphicon glyphicon-list-alt"></span>&nbsp;<span itemprop="name">RSS</span>
						</a>
					</li>
					<li>
						<a title="Search" itemprop="url" class="btn btn-link" href="/Search">
							<span aria-hidden="true" class="glyphicon glyphicon-search"></span>&nbsp;<span itemprop="name" class="">Search</span>
						</a>
					</li>
					<li>
						<a title="About" itemprop="url" class="btn btn-link" href="/About">
							<span aria-hidden="true" class="glyphicon glyphicon-user"></span>&nbsp;<span itemprop="name" class="">About</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
</header>
<div class="container">
	<div class="row">
		<main role="main" class="body-content widget col-lg-9">


			<div class="page-header">
				<h1 class="text-center">You see an error - something went wrong at some point</h1>
			</div>
			<div style="opacity: 0.25;" class="text-center">
				<img src="/Images/error.png">
			</div>
			<h4 style="margin-top: 25px;" class="text-center">Probably it is our fault. Sorry.</h4>
		</main>
	</div>
</div>
<footer class="footer-container">
	<div class="footer-text">&copy; 2016 developer-interview.com &mdash; prepare yourself for the next interview</div>
</footer>
<script src="/Scripts/css-browser-selector.js"></script>
</body>
</html>