﻿using System;
using System.Data.Entity;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AutoMapper;
using Cfg;
using DevInterview.Common.Configuration;
using DevInterview.Common.Logging;
using DevInterview.Dal.OrmInfrastructure;
using DevInterview.Web.Controllers;
using DevInterview.Web.Infrastructure.ModelBinders;
using DevInterview.Web.Models;

namespace DevInterview.Web
{
    public class Global : HttpApplication
    {
        #region Methods

        protected void Application_AuthenticateRequest(object sender, EventArgs e) { }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (Request.Browser.Type.ToUpper().Contains("IE"))
            {
                if (Request.Browser.MajorVersion < 9)
                {
                    Response.Redirect("/OldBrowser.html", true);
                }
            }
        }

        protected void Application_End(object sender, EventArgs e) { }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            if (Context.Response.StatusCode == 404)
            {
                NLogSingleton.Instance.Warn($"Error 404 at {Context.Request.Url} (handled by App_EndRequest)");

                Server.ClearError();
                Response.TrySkipIisCustomErrors = true;

                var rd = new RouteData();
                rd.Values["controller"] = "Error";
                rd.Values["action"] = "Error404";

                IController c = new ErrorController();
                c.Execute(new RequestContext(new HttpContextWrapper(Context), rd));
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();

            Server.ClearError();
            Response.TrySkipIisCustomErrors = true;

            var serverError = exception as HttpException;
            RouteData rd;
            if (serverError != null && serverError.GetHttpCode() == 404)
            {
                NLogSingleton.Instance.Warn($"Error 404 at {Context.Request.Url} (handled by App_Error)");

                rd = new RouteData();
                rd.Values["controller"] = "Error";
                rd.Values["action"] = "Error404";
            }
            else
            {
                NLogSingleton.Instance.Error(exception, "Exception at global level.");

                rd = new RouteData();
                rd.Values["controller"] = "Error";
                rd.Values["action"] = "GenericError";
            }

            IController c = new ErrorController();
            c.Execute(new RequestContext(new HttpContextWrapper(Context), rd));
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AppSettings.Init<AppCfgReader>();
            ModelBindingManager.Init();
            Mapper.AssertConfigurationIsValid();
            ModelBinders.Binders.Add(typeof(PostsQueryModel), new PostsQueryModelBinder());
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AppContext, MigrationConfiguration>());
        }

        protected void Session_End(object sender, EventArgs e) { }

        protected void Session_Start(object sender, EventArgs e) { }

        #endregion
    }
}