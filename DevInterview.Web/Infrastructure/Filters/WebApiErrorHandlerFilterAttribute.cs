﻿using System.Web.Http.Filters;
using DevInterview.Common.Logging;

namespace DevInterview.Web.Infrastructure.Filters
{
    public class WebApiErrorHandlerFilterAttribute : ExceptionFilterAttribute
    {
        #region Public Methods and Operators

        public override void OnException(HttpActionExecutedContext context)
        {
            NLogSingleton.Instance.Error(context.Exception, "Exception at WebApi controller level.");

            base.OnException(context);
        }

        #endregion
    }
}