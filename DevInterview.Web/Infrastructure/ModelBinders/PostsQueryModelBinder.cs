﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using DevInterview.Common.Utilities.Extensions;
using DevInterview.Web.Models;

namespace DevInterview.Web.Infrastructure.ModelBinders
{
    public class PostsQueryModelBinder : DefaultModelBinder
    {
        #region Public Methods and Operators

        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType == typeof(PostsQueryModel))
            {
                HttpRequestBase request = controllerContext.HttpContext.Request;

                string text = request.Params.Get("text");
                string categoryId = request.Params.Get("categoryid");
                string tags = request.Params.Get("tags");
                string order = request.Params.Get("orderby");

                var result = new PostsQueryModel();
                result.Text = text;
                result.OrderBy = order;
                if (!categoryId.IsNullOrEmpty())
                {
                    int catId;
                    if (int.TryParse(categoryId, out catId))
                    {
                        result.Category = new CategoryModel
                        {
                            Id = catId
                        };
                    }
                }

                result.Tags = new List<TagModel>();
                if (!tags.IsNullOrEmpty())
                {
                    foreach (string tag in tags.Split(','))
                    {
                        result.Tags.Add(new TagModel
                        {
                            Title = tag
                        });
                    }
                }

                if (result.Category != null || !result.Text.IsNullOrEmpty() || result.Tags.Count > 0)
                {
                    return result;
                }

                return null;
            }

            return base.BindModel(controllerContext, bindingContext);
        }

        #endregion
    }
}