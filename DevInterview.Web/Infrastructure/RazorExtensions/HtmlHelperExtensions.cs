﻿using System.Web.Mvc;
using System.Web.Routing;

namespace DevInterview.Web.Infrastructure.RazorExtensions
{
    public static class HtmlHelperExtensions
    {
        #region Public Methods and Operators

        public static string ActiveLink(this HtmlHelper helper, string expected, string routeDataKey)
        {
            object categoryFromrouteData = helper.ViewContext.RouteData.Values[routeDataKey];
            bool isCurrentLink = categoryFromrouteData != null && categoryFromrouteData.ToString() == expected;

            return isCurrentLink ? "active" : "";
        }

        public static string ActivePage(this HtmlHelper helper, string action, string controller)
        {
            RouteValueDictionary currentRoute = helper.ViewContext.RouteData.Values;
            bool isCurrentRoute = controller == currentRoute["controller"].ToString() && action == currentRoute["action"].ToString();

            return isCurrentRoute ? "active" : "";
        }

        #endregion
    }
}