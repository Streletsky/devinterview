﻿using System.Web.Mvc;

namespace DevInterview.Web.Infrastructure.RazorExtensions
{
    public static class UrlHelperExtension
    {
        #region Public Methods and Operators

        public static string AbsoluteRouteUrl(this UrlHelper urlHelper, string routeName, object routeValues = null)
        {
            if (urlHelper.RequestContext.HttpContext.Request.Url != null)
            {
                string scheme = urlHelper.RequestContext.HttpContext.Request.Url.Scheme;

                return urlHelper.RouteUrl(routeName, routeValues, scheme);
            }

            return urlHelper.RouteUrl(routeName, routeValues);
        }

        #endregion
    }
}