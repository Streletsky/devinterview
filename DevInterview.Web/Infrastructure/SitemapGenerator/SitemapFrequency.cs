﻿namespace DevInterview.Web.Infrastructure.SitemapGenerator
{
    public enum SitemapFrequency
    {
        Never,

        Yearly,

        Monthly,

        Weekly,

        Daily,

        Hourly,

        Always
    }
}