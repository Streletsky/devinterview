﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.Linq;
using DevInterview.Web.Infrastructure.SitemapGenerator.Interfaces;

namespace DevInterview.Web.Infrastructure.SitemapGenerator
{
    public class SitemapGenerator : ISitemapGenerator
    {
        #region Fields

        private readonly IList<SitemapNode> sitemapNodes;

        #endregion


        #region Constructors and Destructors

        public SitemapGenerator()
        {
            sitemapNodes = new List<SitemapNode>();
        }

        #endregion


        #region Public Methods and Operators

        public void AddSitemapNode(string url, double priority, SitemapFrequency freq, DateTime? lastModified = null)
        {
            sitemapNodes.Add(new SitemapNode
            {
                Url = url,
                Frequency = freq,
                Priority = priority,
                LastModified = lastModified
            });
        }

        public void AddSitemapNode(SitemapNode node)
        {
            sitemapNodes.Add(node);
        }

        public void AddSitemapNodes(IEnumerable<SitemapNode> nodes)
        {
            foreach (SitemapNode node in nodes)
            {
                AddSitemapNode(node);
            }
        }

        public string BuildSitemapDocument()
        {
            XNamespace xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            var root = new XElement(xmlns + "urlset");

            foreach (SitemapNode sitemapNode in sitemapNodes)
            {
                var urlElement = new XElement(
                                              xmlns + "url",
                                              new XElement(xmlns + "loc", Uri.EscapeUriString(sitemapNode.Url)),
                                              sitemapNode.LastModified == null
                                                  ? null
                                                  : new XElement(
                                                                 xmlns + "lastmod",
                                                                 sitemapNode.LastModified.Value.ToLocalTime().ToString("yyyy-MM-ddTHH:mm:sszzz")),
                                              sitemapNode.Frequency == null
                                                  ? null
                                                  : new XElement(
                                                                 xmlns + "changefreq",
                                                                 sitemapNode.Frequency.Value.ToString().ToLowerInvariant()),
                                              sitemapNode.Priority == null
                                                  ? null
                                                  : new XElement(
                                                                 xmlns + "priority",
                                                                 sitemapNode.Priority.Value.ToString("F1", CultureInfo.InvariantCulture)));
                root.Add(urlElement);
            }

            var document = new XDocument(root);

            return document.ToString();
        }

        #endregion
    }
}