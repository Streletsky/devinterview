﻿using System;

namespace DevInterview.Web.Infrastructure.SitemapGenerator
{
    public class SitemapNode
    {
        #region Public Properties

        public SitemapFrequency? Frequency { get; set; }

        public DateTime? LastModified { get; set; }

        public double? Priority { get; set; }

        public string Url { get; set; }

        #endregion
    }
}