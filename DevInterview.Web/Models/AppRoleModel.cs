﻿namespace DevInterview.Web.Models
{
    public class AppRoleModel
    {
        #region Public Properties

        public int Id { get; set; }

        public string Name { get; set; }

        #endregion
    }
}