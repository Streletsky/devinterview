﻿using System;

namespace DevInterview.Web.Models
{
    public class AppUserModel
    {
        #region Public Properties

        public DateTime Created { get; set; }

        public string Email { get; set; }

        public int Id { get; set; }

        public bool IsActive { get; set; }

        public bool IsDeleted { get; set; }

        public int RoleId { get; set; }

        public string UserName { get; set; }

        #endregion
    }
}