﻿namespace DevInterview.Web.Models
{
    public class CategoryModel
    {
        #region Public Properties

        public string Description { get; set; }

        public int Id { get; set; }

        public int PostsCount { get; set; }

        public string Title { get; set; }

        public string UrlSlug { get; set; }

        #endregion
    }
}