﻿using System.ComponentModel.DataAnnotations;

namespace DevInterview.Web.Models
{
    public class LoginModel
    {
        #region Public Properties

        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        #endregion
    }
}