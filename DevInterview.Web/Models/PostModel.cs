﻿using System;
using System.Collections.Generic;

namespace DevInterview.Web.Models
{
    public class PostModel
    {
        #region Public Properties

        public string BodyEnd { get; set; }

        public string BodyStart { get; set; }

        public CategoryModel Category { get; set; }

        public DateTime Created { get; set; }

        public string Description { get; set; }

        public int Id { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsFullPost { get; set; }

        public bool IsLinkingRequired { get; set; }

        public bool IsVisible { get; set; }

        public DateTime LastUpdated { get; set; }

        public RatingModel Rating { get; set; }

        public virtual ICollection<TagModel> Tags { get; set; }

        public string Title { get; set; }

        public string UrlSlug { get; set; }

        public int Views { get; set; }

        #endregion
    }
}