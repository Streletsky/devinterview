﻿using System.Collections.Generic;

namespace DevInterview.Web.Models
{
    public class PostsQueryModel
    {
        #region Public Properties

        public CategoryModel Category { get; set; }

        public string OrderBy { get; set; }

        public IList<TagModel> Tags { get; set; }

        public string Text { get; set; }

        #endregion
    }
}