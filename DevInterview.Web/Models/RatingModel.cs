﻿namespace DevInterview.Web.Models
{
    public class RatingModel
    {
        #region Public Properties

        public int Id { get; set; }

        public int VotesDifference { get; set; }

        public int VotesDown { get; set; }

        public int VotesUp { get; set; }

        #endregion
    }
}