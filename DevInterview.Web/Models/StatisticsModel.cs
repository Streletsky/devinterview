﻿using System;

namespace DevInterview.Web.Models
{
    public class StatisticsModel
    {
        #region Public Properties

        public int CategoriesCount { get; set; }

        public double DaysSinceStart { get; set; }

        public int PostsCount { get; set; }

        public int TagsCount { get; set; }

        public Tuple<int, int> VotesCount { get; set; }

        #endregion
    }
}