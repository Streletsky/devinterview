﻿namespace DevInterview.Web.Models
{
    public class TagModel
    {
        #region Public Properties

        public int Id { get; set; }

        public int PostsCount { get; set; }

        public string Title { get; set; }

        public string UrlSlug { get; set; }

        #endregion
    }
}