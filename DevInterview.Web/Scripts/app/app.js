﻿'use strict';

angular.module('mainApp',
	[
		'ngTagsInput',
		'mainApp.services',
		'mainApp.search'
	]);