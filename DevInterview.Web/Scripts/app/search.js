﻿'use strict';

angular.module('mainApp.search', [])
	.controller('SearchCtrl',
		[
			'$scope', '$q', 'tagApi', 'categoryApi', 'jsonObjectsProvider',
			function($scope, $q, tagApi, categoryApi, jsonObjectsProvider) {
				$scope.postsQuery = window.VanillaApp.PostsQuery || jsonObjectsProvider.PostsQuery();
				$scope.tagsAutocomplete = [];
				$scope.categories = [];
				$scope.orderOptions = [
					{ Value: '', Title: 'Relevance' },
					{ Value: 'Views', Title: 'Views' },
					{ Value: 'Rating.VotesDifference', Title: 'Votes' },
					{ Value: 'Created', Title: 'Creation Date' }
				];

				$scope.loadTags = function($query) {
					var deferred = $q.defer();
					var params = jsonObjectsProvider.SearchParameters();
					params.SearchQuery = $query;
					params.SearchBy = 'Title';
					params.Count = 10;
					params.IsImplicitSearch = true;

					tagApi.search(params)
						.then(function(data) {
							if (data.Records) {
								$scope.tagsAutocomplete = data.Data;
							} else {
								$scope.tagsAutocomplete = [];
							}
						});

					deferred.resolve($scope.tagsAutocomplete);

					return deferred.promise;
				};

				$scope.search = function() {
					var queryString = '/Search?text=' +
						$scope.postsQuery.Text +
						($scope.postsQuery.Category.Id != 0 && $scope.postsQuery.Category.Id != null
							? '&categoryid=' + $scope.postsQuery.Category.Id
							: '') +
						($scope.postsQuery.OrderBy ? '&orderby=' + $scope.postsQuery.OrderBy : '');

					_.each($scope.postsQuery.Tags,
						function(tag) {
							queryString += '&tags=' + tag.Title;
						});

					if (queryString !== '/Search?text=') {
						window.location.href = queryString;
					}
				};

				// Init
				(function() {
					categoryApi.getAll()
						.then(function(data) {
							$scope.categories = data.Data;
							var cat = _.find($scope.categories,
								function(c) {
									return c.Id == $scope.postsQuery.Category.Id;
								});

							if (cat) {
								$scope.postsQuery.Category = cat;
							}
						});
				})();
			}
		]);