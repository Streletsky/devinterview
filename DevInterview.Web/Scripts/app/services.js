﻿'use strict';

angular.module('mainApp.services', ['ngResource'])
	.factory('jsonObjectsProvider',
		[
			function() {
				return {
					SearchParameters: function() {
						return {
							Count: 0,
							OrderBy: null,
							OrderDirection: null,
							Page: 1,
							SearchBy: null,
							SearchQuery: null,
							IsImplicitSearch: false
						};
					},
					PostsQuery: function() {
						return {
							Text: '',
							Category: {
								Id: 0
							},
							Tags: [],
							OrderBy: 'Relevance'
						};
					}
				};
			}
		])
	.service('tagApi',
		[
			'$resource',
			function($resource) {
				var resource = $resource('/api/tags/:id',
					{},
					{
						'query': { method: 'GET', isArray: false }
					});

				this.search = function(params) {
					return resource.get(params).$promise;
				};
			}
		])
	.service('categoryApi',
		[
			'$resource',
			function($resource) {
				var resource = $resource('/api/categories/:id',
					{},
					{
						'query': { method: 'GET', isArray: false }
					});

				this.getAll = function() {
					return resource.query().$promise;
				};
			}
		]);