﻿'use strict';

angular.module('controlPanel',
		[
			'ngRoute',
			'ngTable',
			'ngTagsInput',
			'ngCkeditor',
			'ngResource',
			'controlPanel.services',
			'controlPanel.directives',
			'controlPanel.users',
			'controlPanel.categories',
			'controlPanel.configuration',
			'controlPanel.logs',
			'controlPanel.posts'
		])
	.config([
		'$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
			$routeProvider.otherwise({ redirectTo: '/Posts' });
			$locationProvider.html5Mode(true);
		}
	]);