﻿'use strict';

angular.module('controlPanel.categories', ['ngRoute'])
	.config([
		'$routeProvider', function($routeProvider) {
			$routeProvider.when('/Categories',
				{
					templateUrl: '/Scripts/controlpanel/categories.html',
					controller: 'CategoriesCtrl'
				});
		}
	])
	.controller('CategoriesCtrl',
		[
            '$scope', 'categoryApi', 'jsonObjectsProvider', 'NgTableParams',
			function($scope, categoryApi, jsonObjectsProvider, NgTableParams) {
				$scope.tableParams = new NgTableParams({
						page: 1,
						count: 50
					},
					{
						counts: [],
						total: 0,
						getData: function() {
							return categoryApi.getAll()
								.then(function(data) {
									return data.Data;
								});
						}
					});

				$scope.save = function(cat) {
					categoryApi.save(cat,
						function() {
							cat.$edit = false;
						});
				};

				$scope.delete = function(cat) {
					categoryApi.delete(cat,
						function(data) {
							$scope.tableParams.reload();
						});
				};

				$scope.create = function() {
					var category = jsonObjectsProvider.Category();
					category.$edit = true;

					$scope.tableParams.data.push(category);
				};
			}
		]);