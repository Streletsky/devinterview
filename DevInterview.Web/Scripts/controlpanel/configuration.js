﻿'use strict';

angular.module('controlPanel.configuration', ['ngRoute'])
	.config([
		'$routeProvider', function($routeProvider) {
			$routeProvider.when('/Configuration',
				{
					templateUrl: '/Scripts/controlpanel/configuration.html',
					controller: 'ConfigurationCtrl'
				});
		}
	])
	.controller('ConfigurationCtrl',
		[
            '$scope', 'configApi', 'NgTableParams', function ($scope, configApi, NgTableParams) {
                $scope.tableParams = new NgTableParams({
						page: 1,
						count: 1000
					},
					{
						counts: [],
						total: 0,
						getData: function() {
							return configApi.getAll()
								.then(function(data) {
									return data.data;
								});
						}
					});

				$scope.save = function(cfg) {
					configApi.save(cfg,
						function() {
							cfg.$edit = false;
						});
				};

				$scope.refreshIndex = function() {
					configApi.refreshIndex();
				};
			}
		]);