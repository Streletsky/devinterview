﻿'use strict';

angular.module('controlPanel.directives', [])
	.directive('activeLink',
		[
			'$location', function($location) {
				return {
					restrict: 'A',
					link: function(scope, element, attrs, controller) {
						var cssClass = attrs.activeLink;
						var path = attrs.href;
						scope.location = $location;
						scope.$watch('location.path()',
							function(newPath) {
								var oldPath = '/' + _.last(_.string.words(path, '/'));
								if (oldPath === newPath) {
									element.addClass(cssClass);
								} else {
									element.removeClass(cssClass);
								}
							});
					}
				};
			}
		])
	.filter('yesNoFlag',
		function() {
			return function(input) {
				if (input === true) {
					return 'Yes';
				} else if (input === false) {
					return 'No';
				}

				return input;
			};
		});