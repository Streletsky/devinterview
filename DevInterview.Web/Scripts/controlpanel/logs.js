﻿'use strict';

angular.module('controlPanel.logs', ['ngRoute'])
	.config([
		'$routeProvider', function($routeProvider) {
			$routeProvider.when('/Logs',
				{
					templateUrl: '/Scripts/controlpanel/logs.html',
					controller: 'LogsCtrl'
				});
		}
	])
	.controller('LogsCtrl',
		[
            '$scope', 'logApi', 'NgTableParams', function ($scope, logApi, NgTableParams) {
                $scope.tableParams = new NgTableParams({
						page: 1,
						count: 1000
					},
					{
						counts: [],
						total: 0,
						getData: function() {
							return logApi.getAll()
								.then(function(data) {
									return data;
								});
						}
					});

				$scope.delete = function(log) {
					logApi.delete(log.FileName,
						function() {
							$scope.tableParams.reload();
						});
				};
			}
		]);