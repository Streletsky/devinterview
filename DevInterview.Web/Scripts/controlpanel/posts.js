﻿'use strict';

angular.module('controlPanel.posts', [])
	.config([
		'$routeProvider', function($routeProvider) {
			$routeProvider.when('/Posts',
				{
					templateUrl: '/Scripts/controlpanel/posts.html',
					controller: 'PostsCtrl'
				});
		}
	])
	.config([
		'$routeProvider', function($routeProvider) {
			$routeProvider.when('/EditPost',
				{
					templateUrl: '/Scripts/controlpanel/editPost.html',
					controller: 'EditPostCtrl'
				});
		}
	])
	.config([
		'$routeProvider', function($routeProvider) {
			$routeProvider.when('/EditPost/:postId',
				{
					templateUrl: '/Scripts/controlpanel/editPost.html',
					controller: 'EditPostCtrl'
				});
		}
	])
	.controller('PostsCtrl',
		[
            '$scope', '$location', '$filter', 'postApi', 'jsonObjectsProvider', 'NgTableParams',
			function($scope, $location, $filter, postApi, jsonObjectsProvider, NgTableParams) {
				$scope.tableParams = new NgTableParams({
						page: 1,
						count: 20,
						sorting: { Created: 'desc' }
					},
					{
						counts: [],
						getData: function($params) {
							var searchParams = jsonObjectsProvider.SearchParameters();
							searchParams.Count = $params.count();
							searchParams.Page = $params.page();
							searchParams.OrderBy = _.string.trim($params.orderBy(), ['+', '-']);
							searchParams.OrderDirection = _.string.startsWith($params.orderBy(), '-') ? 'desc' : '';
							return postApi.search(searchParams)
								.then(function(data) {
									$params.total(data.TotalRecords);
									return $filter('orderBy')(data.Data, $params.orderBy());
								});
						}
					});

				$scope.delete = function(post) {
					postApi.delete(post,
						function(data) {
							$scope.tableParams.reload();
						});
				};

				$scope.create = function() {
					$location.path('/EditPost');
				};
			}
		])
	.controller('EditPostCtrl',
		[
			'$scope', '$location', '$routeParams', '$q', 'postApi', 'categoryApi', 'tagApi', 'jsonObjectsProvider',
			function($scope, $location, $routeParams, $q, postApi, categoryApi, tagApi, jsonObjectsProvider) {
				$scope.post = jsonObjectsProvider.Post();
				$scope.categories = [];
				$scope.tagsAutocomplete = [];
				$scope.similarPosts = [];

				$scope.save = function() {
					postApi.save($scope.post,
						function(post) {
							window.location.href = '/p/' + post.Category.UrlSlug + '/' + post.UrlSlug + '-' + post.Id;
						});
				};

				$scope.saveNow = function() {
					postApi.save($scope.post,
						function(data) {
							alert('Saved!');
							$location.path('/EditPost/' + data.Id);
						});
				};

				$scope.checkSimilar = function() {
					postApi.searchSimiar($scope.post.Title,
						function(data) {
							$scope.similarPosts = data.data;
						});
				};

				$scope.loadTags = function($query) {
					var deferred = $q.defer();
					var params = jsonObjectsProvider.SearchParameters();
					params.SearchQuery = $query;
					params.SearchBy = 'Title';
					params.Count = 10;
					params.IsImplicitSearch = true;

					tagApi.search(params)
						.then(function(data) {
							if (data.Records) {
								$scope.tagsAutocomplete = data.Data;
							} else {
								$scope.tagsAutocomplete = [];
							}
						});

					deferred.resolve($scope.tagsAutocomplete);

					return deferred.promise;
				};

				// Init
				(function() {
					categoryApi.getAll()
						.then(function(data) {
							$scope.categories = data.Data;
						});

					if ($routeParams.postId) {
						postApi.get($routeParams.postId)
							.then(function(data) {
								$scope.post = data;
							});
					}
				})();
			}
		]);