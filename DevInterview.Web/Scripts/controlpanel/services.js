﻿'use strict';

angular.module('controlPanel.services', ['ngResource'])
	.factory('jsonObjectsProvider',
		[
			function() {
				return {
					SearchParameters: function() {
						return {
							Count: 0,
							OrderBy: null,
							OrderDirection: null,
							Page: 1,
							SearchBy: null,
							SearchQuery: null,
							IsImplicitSearch: false
						};
					},
					Category: function() {
						return {
							Id: 0,
							Title: '',
							UrlSlug: '',
							Description: ''
						};
					},
					Post: function() {
						return {
							Id: 0,
							Title: null,
							UrlSlug: null,
							BodyEnd: null,
							BodyStart: null,
							Category: {},
							Created: null,
							LastUpdated: null,
							IsDeleted: false,
							IsVisible: true,
							IsLinkingRequired: false,
							Rating: {},
							Tags: [],
							Views: 0,
							Description: ''
						};
					}
				};
			}
		])
	.service('userApi',
		[
			'$resource',
			function($resource) {
				var resource = $resource('/api/users/:id',
					{},
					{
						'query': { method: 'GET', isArray: false },
						'update': { method: 'PUT' }
					});

				this.getAll = function() {
					return resource.query().$promise;
				};

				this.save = function(user, callback) {
					if (user.Id) {
						resource.update({ id: user.Id }, user, callback || angular.noop);
					} else {
						resource.save(user, callback || angular.noop);
					}
				};

				this.delete = function(user, callback) {
					resource.delete({ id: user.Id }, callback || angular.noop);
				};
			}
		])
	.service('roleApi',
		[
			'$resource',
			function($resource) {
				var resource = $resource('/api/roles/:id');

				this.getAll = function() {
					return resource.query().$promise;
				};
			}
		])
	.service('categoryApi',
		[
			'$resource',
			function($resource) {
				var resource = $resource('/api/categories/:id',
					{},
					{
						'query': { method: 'GET', isArray: false },
						'update': { method: 'PUT' }
					});

				this.getAll = function() {
					return resource.query().$promise;
				};

				this.save = function(cat, callback) {
					if (cat.Id) {
						resource.update({ id: cat.Id }, cat, callback || angular.noop);
					} else {
						resource.save(cat, callback || angular.noop);
					}
				};

				this.delete = function(cat, callback) {
					resource.delete({ id: cat.Id }, callback || angular.noop);
				};
			}
		])
	.service('postApi',
		[
			'$resource', '$http',
			function($resource, $http) {
				var resource = $resource('/api/posts/:id',
					{},
					{
						'query': { method: 'GET', isArray: false },
						'update': { method: 'PUT' }
					});

				this.get = function(id) {
					return resource.get({ id: id }).$promise;
				};

				this.search = function(params) {
					return resource.get(params).$promise;
				};

				this.save = function(post, callback) {
					if (post.Id) {
						resource.update({ id: post.Id }, post, callback || angular.noop);
					} else {
						resource.save(post, callback || angular.noop);
					}
				};

				this.delete = function(post, callback) {
					resource.delete({ id: post.Id }, callback || angular.noop);
				};

				this.searchSimiar = function(query, callback) {
					return $http.get('/controlpanel/admin/similarposts?query=' + query).then(callback);
				};
			}
		])
	.service('tagApi',
		[
			'$resource',
			function($resource) {
				var resource = $resource('/api/tags/:id',
					{},
					{
						'query': { method: 'GET', isArray: false },
						'update': { method: 'PUT' }
					});

				this.get = function(id) {
					return resource.get({ id: id }).$promise;
				};

				this.search = function(params) {
					return resource.get(params).$promise;
				};

				this.save = function(tag, callback) {
					if (tag.Id) {
						resource.update({ id: tag.Id }, tag, callback || angular.noop);
					} else {
						resource.save(tag, callback || angular.noop);
					}
				};

				this.delete = function(tag, callback) {
					resource.delete({ id: tag.Id }, callback || angular.noop);
				};
			}
		])
	.service('configApi',
		[
			'$resource', '$http',
			function($resource, $http) {
				var resource = $resource('/api/configuration/:id',
					{},
					{
						'query': { method: 'GET', isArray: false },
						'update': { method: 'POST' }
					});

				this.getAll = function() {
					return $http.get('/api/configuration');
				};

				this.save = function(cfg, callback) {
					resource.update(cfg, callback || angular.noop);
				};

				this.refreshIndex = function() {
					$http.get('/controlpanel/admin/refreshindex')
						.then(function() {
							alert('Lucene Index refreshed');
						});
				};
			}
		])
	.service('logApi',
		[
			'$resource',
			function($resource) {
				var resource = $resource('/api/logs/:id');

				this.getAll = function() {
					return resource.query().$promise;
				};

				this.delete = function(fileName, callback) {
					resource.delete({ fileName: fileName }, callback || angular.noop);
				};
			}
		]);