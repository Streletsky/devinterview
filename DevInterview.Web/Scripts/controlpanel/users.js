﻿'use strict';

angular.module('controlPanel.users', [])
	.config([
		'$routeProvider', function($routeProvider) {
			$routeProvider.when('/Users',
				{
					templateUrl: '/Scripts/controlpanel/users.html',
					controller: 'UsersCtrl'
				});
		}
	])
	.controller('UsersCtrl',
		[
            '$scope', 'userApi', 'roleApi', 'NgTableParams', function ($scope, userApi, roleApi, NgTableParams) {
				$scope.roles = [];

                $scope.tableParams = new NgTableParams({
						page: 1,
						count: 10
					},
					{
						counts: [],
						total: 0,
						getData: function() {
							return userApi.getAll()
								.then(function(data) {
									return data.Data;
								});
						}
					});

				$scope.save = function(user) {
					userApi.save(user,
						function() {
							user.$edit = false;
						});
				};

				$scope.delete = function(user) {
					userApi.delete(user,
						function(data) {
							$scope.tableParams.reload();
						});
				};

				// Init
				(function() {
					roleApi.getAll()
						.then(function(data) {
							$scope.roles = data;
						});
				})();
			}
		]);